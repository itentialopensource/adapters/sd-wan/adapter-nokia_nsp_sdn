# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the nokia_nsp_sdn System. The API that was used to build the adapter for nokia_nsp_sdn is usually available in the report directory of this adapter. The adapter utilizes the nokia_nsp_sdn API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Nokia NSP SDN adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Nokia's NSP SDN to aid in retrieval of various resources from the NSP specific to a Carrier SDN Managed Network.

With this adapter you have the ability to perform operations with Nokia NSP SDN such as:

- Policy
- Security
- Ports
- Links
- Tenants

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
