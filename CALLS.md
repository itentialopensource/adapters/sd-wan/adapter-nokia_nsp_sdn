## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Nokia NSP SDN. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Nokia NSP SDN.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Nokia Network Services Platform. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getApplicationId(id, callback)</td>
    <td style="padding:15px">Find an object by Application ID</td>
    <td style="padding:15px">{base_path}/{version}/v4/generic/application-id/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setApplicationId(appId, id, callback)</td>
    <td style="padding:15px">Set an object Application ID</td>
    <td style="padding:15px">{base_path}/{version}/v4/generic/application-id/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConsumed(uuid, callback)</td>
    <td style="padding:15px">Find all objects being consumed by an object</td>
    <td style="padding:15px">{base_path}/{version}/v4/generic/consumed/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findByExternalId(body, callback)</td>
    <td style="padding:15px">Find the unique identifier of an object given an external identifier</td>
    <td style="padding:15px">{base_path}/{version}/v4/generic/find-by-external-id?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTenants(uuid, callback)</td>
    <td style="padding:15px">Find all tenants assigned to an object</td>
    <td style="padding:15px">{base_path}/{version}/v4/generic/tenants/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">get(uuid, callback)</td>
    <td style="padding:15px">Find an object by a unique identifier</td>
    <td style="padding:15px">{base_path}/{version}/v4/generic/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addL2BackhaulEndpoint(body, serviceUuid, callback)</td>
    <td style="padding:15px">Add an endpoint to a L2 Backhaul Service</td>
    <td style="padding:15px">{base_path}/{version}/v4/ietf/ethtsvc/l2-backhaul/{pathv1}/endpoint?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateL2BackhaulEndpoint(endpointUuid, body, serviceUuid, callback)</td>
    <td style="padding:15px">Update endpoint of a L2 Backhaul service</td>
    <td style="padding:15px">{base_path}/{version}/v4/ietf/ethtsvc/l2-backhaul/{pathv1}/endpoint/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchL2BackhaulEndpoint(endpointUuid, body, serviceUuid, callback)</td>
    <td style="padding:15px">Patch endpoint of an L3 VPN service</td>
    <td style="padding:15px">{base_path}/{version}/v4/ietf/ethtsvc/l2-backhaul/{pathv1}/endpoint/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createL2backhaul(autoPath, body, callback)</td>
    <td style="padding:15px">Create L2 Backhaul Service</td>
    <td style="padding:15px">{base_path}/{version}/v4/ietf/ethtsvc/l2backhaul/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateL2backhaul(body, serviceUuid, callback)</td>
    <td style="padding:15px">Modify L2 Backhaul service</td>
    <td style="padding:15px">{base_path}/{version}/v4/ietf/ethtsvc/l2backhaul/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchL2backhaul(body, serviceUuid, callback)</td>
    <td style="padding:15px">Patch L2 Backhaul service</td>
    <td style="padding:15px">{base_path}/{version}/v4/ietf/ethtsvc/l2backhaul/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4Services(callback)</td>
    <td style="padding:15px">Query all service objects</td>
    <td style="padding:15px">{base_path}/{version}/v4/services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createClines(body, callback)</td>
    <td style="padding:15px">Create a C-Line service</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/clines?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateClines(body, serviceUuid, callback)</td>
    <td style="padding:15px">Modify a C-Line service</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/clines/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchClines(body, serviceUuid, callback)</td>
    <td style="padding:15px">Patch a C-Line service</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/clines/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateClineEndpoint(endpointUuid, serviceUuid, body, callback)</td>
    <td style="padding:15px">Update the endpoint of a C-Line service</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/clines/{pathv1}/endpoint/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchClineEndpoint(endpointUuid, serviceUuid, body, callback)</td>
    <td style="padding:15px">Patch the endpoint of a C-Line service</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/clines/{pathv1}/endpoint/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getComponents(uuid, callback)</td>
    <td style="padding:15px">Retrieve inventory of service endpoints, tunnels, and physical links for the requested service UUID</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/components/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createEaccess(body, callback)</td>
    <td style="padding:15px">Create an IP E-Access service</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/eaccess?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateEaccess(body, serviceUuid, callback)</td>
    <td style="padding:15px">Modify an IP E-Access service</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/eaccess/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchEaccess(body, serviceUuid, callback)</td>
    <td style="padding:15px">Patch an IP E-Access service</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/eaccess/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createElans(body, callback)</td>
    <td style="padding:15px">Create an E-LAN VPN service</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/elans?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateElans(body, serviceUuid, callback)</td>
    <td style="padding:15px">Modify an E-LAN service</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/elans/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchElans(body, serviceUuid, callback)</td>
    <td style="padding:15px">Patch an E-LAN service</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/elans/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addElanEndpoint(body, serviceUuid, callback)</td>
    <td style="padding:15px">Add an endpoint to an E-LAN service</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/elans/{pathv1}/endpoint?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateElanEndpoint(endpointUuid, body, serviceUuid, callback)</td>
    <td style="padding:15px">Update the endpoint of an E-LAN service</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/elans/{pathv1}/endpoint/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchElanEndpoint(endpointUuid, body, serviceUuid, callback)</td>
    <td style="padding:15px">Patch the endpoint of an E-LAN service</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/elans/{pathv1}/endpoint/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createElines(body, callback)</td>
    <td style="padding:15px">Create an IP or optical E-Line service</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/elines?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateElines(body, serviceUuid, callback)</td>
    <td style="padding:15px">Modify an IP or optical E-Line service</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/elines/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchElines(body, serviceUuid, callback)</td>
    <td style="padding:15px">Patch an IP or optical E-Line service</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/elines/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateElineEndpoint(endpointUuid, body, serviceUuid, callback)</td>
    <td style="padding:15px">Update the endpoint of an E-Line service</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/elines/{pathv1}/endpoint/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchElineEndpoint(endpointUuid, body, serviceUuid, callback)</td>
    <td style="padding:15px">Patch the endpoint of an E-Line service</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/elines/{pathv1}/endpoint/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateFixL3VpnVrfMembership(serviceUuid, callback)</td>
    <td style="padding:15px">Sites within the specified L3-VPN service that are not correctly connected by VRF membership will b</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/fix-l3-vpn-vrf-membership/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIes(body, callback)</td>
    <td style="padding:15px">Create an IP IES service</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/ies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIes(body, serviceUuid, callback)</td>
    <td style="padding:15px">Modify an IP IES service</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/ies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchIes(body, serviceUuid, callback)</td>
    <td style="padding:15px">Patch an IP IES service</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/ies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addIesEndpoint(body, serviceUuid, callback)</td>
    <td style="padding:15px">Add an endpoint to a IES service</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/ies/{pathv1}/endpoint?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIesEndpoint(endpointUuid, body, serviceUuid, callback)</td>
    <td style="padding:15px">Update the endpoint of an IES service</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/ies/{pathv1}/endpoint/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchIesEndpoint(endpointUuid, body, serviceUuid, callback)</td>
    <td style="padding:15px">Patch the endpoint of an IES service</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/ies/{pathv1}/endpoint/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addIesLoopbackEndpoint(body, serviceUuid, callback)</td>
    <td style="padding:15px">Add a loop back endpoint to an IES service</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/ies/{pathv1}/loopback-endpoint?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIesLoopbackEndpoint(endpointUuid, body, serviceUuid, callback)</td>
    <td style="padding:15px">Update loop back endpoint of an IES service</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/ies/{pathv1}/loopback-endpoint/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchIesLoopbackEndpoint(endpointUuid, body, serviceUuid, callback)</td>
    <td style="padding:15px">Patch loop back endpoint of an IES service</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/ies/{pathv1}/loopback-endpoint/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createL3DciVpns(body, callback)</td>
    <td style="padding:15px">Support for limited Data Center use cases as VXLAN Stitched connectivity type. Unsupported Proof of</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/l3-dci-vpns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchL3DciL3Endpoint(endpointUuid, body, serviceUuid, callback)</td>
    <td style="padding:15px">Patch L3 endpoint of an L3 DCI service</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/l3-dcis/{pathv1}/endpoint/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getL3VpnRtAudit(callback)</td>
    <td style="padding:15px">Runs an audit to find all L3-VPN services with invalid VRF membership. Currently the audit only con</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/l3-vpn-rt-audit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createL3Vpns(body, callback)</td>
    <td style="padding:15px">Create an L3 VPN service</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/l3-vpns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateL3Vpns(body, serviceUuid, callback)</td>
    <td style="padding:15px">Modify an L3 VPN service</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/l3-vpns/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchL3Vpns(body, serviceUuid, callback)</td>
    <td style="padding:15px">Patch an L3 VPN service</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/l3-vpns/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addL3VpnEndpoint(body, serviceUuid, callback)</td>
    <td style="padding:15px">Add an endpoint to an L3 VPN service</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/l3-vpns/{pathv1}/endpoint?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateL3VpnEndpoint(endpointUuid, body, serviceUuid, callback)</td>
    <td style="padding:15px">Update endpoint of an L3 VPN service</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/l3-vpns/{pathv1}/endpoint/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchL3VpnEndpoint(endpointUuid, body, serviceUuid, callback)</td>
    <td style="padding:15px">Patch endpoint of an L3 VPN service</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/l3-vpns/{pathv1}/endpoint/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addL3VpnLoopbackEndpoint(body, serviceUuid, callback)</td>
    <td style="padding:15px">Add a loop back endpoint to an L3 VPN service</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/l3-vpns/{pathv1}/loopback-endpoint?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateL3VpnLoopbackEndpoint(endpointUuid, body, serviceUuid, callback)</td>
    <td style="padding:15px">Update loop back endpoint of an L3 VPN service</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/l3-vpns/{pathv1}/loopback-endpoint/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchL3VpnLoopbackEndpoint(endpointUuid, body, serviceUuid, callback)</td>
    <td style="padding:15px">Patch loop back endpoint of an L3 VPN service</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/l3-vpns/{pathv1}/loopback-endpoint/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createLags(body, callback)</td>
    <td style="padding:15px">Create a LAG service</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/lags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateLags(body, serviceUuid, callback)</td>
    <td style="padding:15px">Modify an LAG service</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/lags/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNormalizedL3Vpns(body, callback)</td>
    <td style="padding:15px">Create a normalized L3 VPN service</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/normalized-l3-vpns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNormalizedL3Vpns(body, serviceUuid, callback)</td>
    <td style="padding:15px">Modify a normalized L3 VPN service</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/normalized-l3-vpns/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchNormalizedL3Vpns(body, serviceUuid, callback)</td>
    <td style="padding:15px">Patch a normalized L3 VPN service</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/normalized-l3-vpns/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addL3VpnNormalizedEndpoint(body, serviceUuid, callback)</td>
    <td style="padding:15px">Add an endpoint to a normalized L3 VPN service</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/normalized-l3-vpns/{pathv1}/endpoint?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOptical(body, callback)</td>
    <td style="padding:15px">Create or modify an optical service</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/optical?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOptical(body, serviceUuid, callback)</td>
    <td style="padding:15px">Modify an optical service</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/optical/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchOptical(body, serviceUuid, callback)</td>
    <td style="padding:15px">Patch an optical service</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/optical/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPhysicalLinksOnService(serviceUuid, callback)</td>
    <td style="padding:15px">Query physical links used by service</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/physical-links-on-service/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getResourceCount(resourceType = 'EACCESS_SERVICE', tenantUuid, callback)</td>
    <td style="padding:15px">getResourceCount</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/resource-count/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServicesOnTunnel(tunnelUuid, callback)</td>
    <td style="padding:15px">Query services using a tunnel</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/services-on-tunnel/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ServicesTenantUuid(uuid, callback)</td>
    <td style="padding:15px">getTenant</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/tenant/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTunnel(tunnelUuid, callback)</td>
    <td style="padding:15px">Find a Tunnel</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/tunnel/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTunnel(body, tunnelUuid, callback)</td>
    <td style="padding:15px">Modify a tunnel</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/tunnel/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTunnels(callback)</td>
    <td style="padding:15px">Query all Tunnels</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/tunnels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTunnelsOnService(serviceUuid, callback)</td>
    <td style="padding:15px">Query service tunnels used by service</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/tunnels-on-service/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServiceEndpoint(endpointUuid, serviceUuid, callback)</td>
    <td style="padding:15px">Delete a service endpoint from a service</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/{pathv1}/endpoint/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEndpoints(serviceUuid, callback)</td>
    <td style="padding:15px">Query all endpoints associated to a service object</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/{pathv1}/endpoints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4ServicesUuid(uuid, callback)</td>
    <td style="padding:15px">Find a service by a unique identifier</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4ServicesUuid(uuid, callback)</td>
    <td style="padding:15px">Delete a service</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLink(linkId, callback)</td>
    <td style="padding:15px">Get TE link. A link has a UUID, attributes, a source (node, termination-point) and a destination (n</td>
    <td style="padding:15px">{base_path}/{version}/v4/ietf/te/link/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchLink(linkUuid, body, callback)</td>
    <td style="padding:15px">Patch TE link by updating the configuration parameters. Supported configuration parameter(s): SRLG,</td>
    <td style="padding:15px">{base_path}/{version}/v4/ietf/te/link/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetwork(networkId, callback)</td>
    <td style="padding:15px">Get TE network. A network has a UUID, attributes, a list of nodes and a list of links.</td>
    <td style="padding:15px">{base_path}/{version}/v4/ietf/te/network/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworks(callback)</td>
    <td style="padding:15px">Get TE networks. A list of networks where each network has a UUID.</td>
    <td style="padding:15px">{base_path}/{version}/v4/ietf/te/networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNode(nodeId, callback)</td>
    <td style="padding:15px">Get TE node. A node has a UUID, attributes and a list of termination-points.</td>
    <td style="padding:15px">{base_path}/{version}/v4/ietf/te/node/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTerminationPoint(tpId, callback)</td>
    <td style="padding:15px">Get TE termination-point. A termination-point has a UUID and attributes.</td>
    <td style="padding:15px">{base_path}/{version}/v4/ietf/te/termination-point/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getL2Paths(body, callback)</td>
    <td style="padding:15px">Get L2 Path</td>
    <td style="padding:15px">{base_path}/{version}/v4/l2/backhaul/paths/l2-paths?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAmiVersionTemplates(callback)</td>
    <td style="padding:15px">Fetch the template global names for all AMIs, grouped by AMI</td>
    <td style="padding:15px">{base_path}/{version}/v4/mediation/ami-version-templates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAmisVersions(callback)</td>
    <td style="padding:15px">Fetch the AMIs and the versions.</td>
    <td style="padding:15px">{base_path}/{version}/v4/mediation/amis-versions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllAugmentationMeta(callback)</td>
    <td style="padding:15px">Query all Augmentation Meta Infos</td>
    <td style="padding:15px">{base_path}/{version}/v4/mediation/augmentation-meta?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAugmentationMeta(body, callback)</td>
    <td style="padding:15px">Create an Augmentation Meta</td>
    <td style="padding:15px">{base_path}/{version}/v4/mediation/augmentation-meta?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAugmentationMeta(body, id, callback)</td>
    <td style="padding:15px">Modify an Augmentation Meta</td>
    <td style="padding:15px">{base_path}/{version}/v4/mediation/augmentation-meta/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAugmentationMeta(id, callback)</td>
    <td style="padding:15px">Delete an Augmentation meta</td>
    <td style="padding:15px">{base_path}/{version}/v4/mediation/augmentation-meta/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMediationAugmentation(body, callback)</td>
    <td style="padding:15px">This action is to fetch the meta information of the passthrough attributes that could be provided d</td>
    <td style="padding:15px">{base_path}/{version}/v4/mediation/mediation-augmentation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">fetchLspList(body, callback)</td>
    <td style="padding:15px">Fetch RSVP or SR-TE LSP list which discovered by SDN through device management NFMP or MDM</td>
    <td style="padding:15px">{base_path}/{version}/v4/mpls/lsp-list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createLspPath(body, callback)</td>
    <td style="padding:15px">Asynchronous API to create MPLS LSP path (PCE Initiated only)</td>
    <td style="padding:15px">{base_path}/{version}/v4/mpls/lsp-path?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">fetchLspPathList(body, callback)</td>
    <td style="padding:15px">Fetch MPLS LSP path list</td>
    <td style="padding:15px">{base_path}/{version}/v4/mpls/lsp-path-list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLspPath(pathId, callback)</td>
    <td style="padding:15px">Get MPLS LSP path</td>
    <td style="padding:15px">{base_path}/{version}/v4/mpls/lsp-path/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLspPath(pathId, callback)</td>
    <td style="padding:15px">Asynchronous API to delete MPLS LSP path(PCE Initiated only)</td>
    <td style="padding:15px">{base_path}/{version}/v4/mpls/lsp-path/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchLspPath(body, pathId, callback)</td>
    <td style="padding:15px">Asynchronous API to patch PCE Initiated MPLS LSP path config and add/remove path profile override </td>
    <td style="padding:15px">{base_path}/{version}/v4/mpls/lsp-path/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLspPaths(callback)</td>
    <td style="padding:15px">Get MPLS LSP paths</td>
    <td style="padding:15px">{base_path}/{version}/v4/mpls/lsp-paths?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createLspPaths(body, callback)</td>
    <td style="padding:15px">Asynchronous API to create MPLS LSP paths(PCE Initiated only)</td>
    <td style="padding:15px">{base_path}/{version}/v4/mpls/lsp-paths?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLspPaths(body, callback)</td>
    <td style="padding:15px">Asynchronous API to delete MPLS LSP paths(PCE Initiated only). List maximum size is 500.</td>
    <td style="padding:15px">{base_path}/{version}/v4/mpls/lsp-paths?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLspPathsPaginated(limit, page, callback)</td>
    <td style="padding:15px">Get MPLS LSP paths</td>
    <td style="padding:15px">{base_path}/{version}/v4/mpls/lsp-paths-paginated/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLsp(lspId, callback)</td>
    <td style="padding:15px">Get RSVP or SR-TE LSP which discovered by SDN through device management NFMP or MDM</td>
    <td style="padding:15px">{base_path}/{version}/v4/mpls/lsp/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLsps(callback)</td>
    <td style="padding:15px">Get RSVP or SR-TE LSPs which discovered by SDN through device management NFMP or MDM</td>
    <td style="padding:15px">{base_path}/{version}/v4/mpls/lsps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOptimization(body, callback)</td>
    <td style="padding:15px">Trigger global concurrent optimization on PCEP LSP paths</td>
    <td style="padding:15px">{base_path}/{version}/v4/mpls/optimization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createResignal(body, callback)</td>
    <td style="padding:15px">Trigger resignal on a list of PCEP LSP paths</td>
    <td style="padding:15px">{base_path}/{version}/v4/mpls/resignal?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOnLink(linkId, requestType = 'MOVED', callback)</td>
    <td style="padding:15px">Get MPLS LSP paths on link</td>
    <td style="padding:15px">{base_path}/{version}/v4/mpls/lsp/paths/on-link/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">gets(callback)</td>
    <td style="padding:15px">Query all Network Elements</td>
    <td style="padding:15px">{base_path}/{version}/v4/ne?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystem(systemId, callback)</td>
    <td style="padding:15px">Query a unique identifier for a Network Element by System ID</td>
    <td style="padding:15px">{base_path}/{version}/v4/ne/system/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">update(id, body, callback)</td>
    <td style="padding:15px">Update Network Element capabilities</td>
    <td style="padding:15px">{base_path}/{version}/v4/ne/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4NeUuid(uuid, callback)</td>
    <td style="padding:15px">Find a network element by a unique identifier</td>
    <td style="padding:15px">{base_path}/{version}/v4/ne/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLatency(callback)</td>
    <td style="padding:15px">Get NSP latency parameters</td>
    <td style="padding:15px">{base_path}/{version}/v4/nsp/configuration/latency?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchLatency(body, callback)</td>
    <td style="padding:15px">Patch NSP latency parameters</td>
    <td style="padding:15px">{base_path}/{version}/v4/nsp/configuration/latency?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLatency(session, callback)</td>
    <td style="padding:15px">API used to delete twamp tests.</td>
    <td style="padding:15px">{base_path}/{version}/v4/nsp/configuration/latency/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTcaConfigPolicy(callback)</td>
    <td style="padding:15px">Get TCA Policy configuration. Policies configuration include list of policies and collection enable</td>
    <td style="padding:15px">{base_path}/{version}/v4/nsp/configuration/tca-config-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchTcaConfigPolicy(body, callback)</td>
    <td style="padding:15px">Patch a TCA configuration policy by updating the parameters</td>
    <td style="padding:15px">{base_path}/{version}/v4/nsp/configuration/tca-config-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTrafficDataCollection(callback)</td>
    <td style="padding:15px">Get NSP traffic collection parameters.</td>
    <td style="padding:15px">{base_path}/{version}/v4/nsp/configuration/traffic-data-collection?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchTrafficDataCollection(body, callback)</td>
    <td style="padding:15px">Patch NSP traffic-collection parameters.</td>
    <td style="padding:15px">{base_path}/{version}/v4/nsp/configuration/traffic-data-collection?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLinkTp(tpId, callback)</td>
    <td style="padding:15px">Get link connected to the given termination point. The link may be a source or destination on the t</td>
    <td style="padding:15px">{base_path}/{version}/v4/nsp/net/l3/link/tp/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4NspNetL3LinkLinkId(linkId, callback)</td>
    <td style="padding:15px">Get link. A link has a UUID, attributes, a source (node, termination-point) and a destination (node</td>
    <td style="padding:15px">{base_path}/{version}/v4/nsp/net/l3/link/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchV4NspNetL3LinkLinkUuid(body, linkUuid, callback)</td>
    <td style="padding:15px">Patch a link by updating the configuration parameters. Configuration parameters supported are srlg-</td>
    <td style="padding:15px">{base_path}/{version}/v4/nsp/net/l3/link/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4NspNetL3NetworkNetworkId(networkId, callback)</td>
    <td style="padding:15px">Get NSP network. A network has a UUID, attributes, a list of nodes and a list of links</td>
    <td style="padding:15px">{base_path}/{version}/v4/nsp/net/l3/network/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4NspNetL3Networks(callback)</td>
    <td style="padding:15px">Get NSP networks. A list of networks where each network has a UUID</td>
    <td style="padding:15px">{base_path}/{version}/v4/nsp/net/l3/networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4NspNetL3NodeNodeId(nodeId, callback)</td>
    <td style="padding:15px">Get NSP node. A node has a UUID, attributes and a list of termination-points</td>
    <td style="padding:15px">{base_path}/{version}/v4/nsp/net/l3/node/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodesRouter(routerId, callback)</td>
    <td style="padding:15px">Get NSP nodes by IGP Router ID. A node has a UUID, attributes and a list of termination-points</td>
    <td style="padding:15px">{base_path}/{version}/v4/nsp/net/l3/nodes/router/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodesSite(siteIp, callback)</td>
    <td style="padding:15px">Get NSP nodes by Site IP. A node has a UUID, attributes and a list of termination-points</td>
    <td style="padding:15px">{base_path}/{version}/v4/nsp/net/l3/nodes/site/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4NspNetL3TerminationPointTpId(tpId, callback)</td>
    <td style="padding:15px">Get NSP termination-point. A termination-point has a UUID and attributes</td>
    <td style="padding:15px">{base_path}/{version}/v4/nsp/net/l3/termination-point/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTerminationPointReverse(tpId, callback)</td>
    <td style="padding:15px">Get NSP reverse termination-point for a given UUID. If the UUID corresponds to a termination point </td>
    <td style="padding:15px">{base_path}/{version}/v4/nsp/net/l3/termination-point/{pathv1}/reverse?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTerminationPointsNodeip(ipAddress, nodeId, callback)</td>
    <td style="padding:15px">Get NSP termination-point for a given node ID and IP Address</td>
    <td style="padding:15px">{base_path}/{version}/v4/nsp/net/l3/termination-points/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createFlows(body, callback)</td>
    <td style="padding:15px">Create flows</td>
    <td style="padding:15px">{base_path}/{version}/v4/openflow/flows?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFlows(body, callback)</td>
    <td style="padding:15px">Delete flows</td>
    <td style="padding:15px">{base_path}/{version}/v4/openflow/flows?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchFlows(body, callback)</td>
    <td style="padding:15px">Update flow instruction</td>
    <td style="padding:15px">{base_path}/{version}/v4/openflow/flows?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFlowsearch(body, callback)</td>
    <td style="padding:15px">Query flows based on search criteria</td>
    <td style="padding:15px">{base_path}/{version}/v4/openflow/flows/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFlowsearchById(body, callback)</td>
    <td style="padding:15px">Query flows based on unique identifier</td>
    <td style="padding:15px">{base_path}/{version}/v4/openflow/flows/search-by-id?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPorts(datapathId, callback)</td>
    <td style="padding:15px">Query all ports of an openflow switch</td>
    <td style="padding:15px">{base_path}/{version}/v4/openflow/ports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSwitches(callback)</td>
    <td style="padding:15px">Query all openflow switches</td>
    <td style="padding:15px">{base_path}/{version}/v4/openflow/switches?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSwitchesInRouter(neId, callback)</td>
    <td style="padding:15px">Query all openflow switches in a router</td>
    <td style="padding:15px">{base_path}/{version}/v4/openflow/switches/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTables(datapathId, callback)</td>
    <td style="padding:15px">Query all tables in an openflow switch</td>
    <td style="padding:15px">{base_path}/{version}/v4/openflow/tables/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4Physicallinks(callback)</td>
    <td style="padding:15px">Query all physical links</td>
    <td style="padding:15px">{base_path}/{version}/v4/physicallinks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">delete(linkId, callback)</td>
    <td style="padding:15px">Delete an undiscovered physical link that was defined in NSP</td>
    <td style="padding:15px">{base_path}/{version}/v4/physicallinks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">create(destId, srcId, callback)</td>
    <td style="padding:15px">Define an undicovered physical link in NSP</td>
    <td style="padding:15px">{base_path}/{version}/v4/physicallinks/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4PhysicallinksUuid(uuid, callback)</td>
    <td style="padding:15px">Find a physical link by a unique identifier</td>
    <td style="padding:15px">{base_path}/{version}/v4/physicallinks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllIpOpticalCorrelationPolicy(callback)</td>
    <td style="padding:15px">Query all IP Optical correlation policies</td>
    <td style="padding:15px">{base_path}/{version}/v4/policy/ip-optical-correlation-policiy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIpOpticalCorrelationPolicy(body, callback)</td>
    <td style="padding:15px">Create an IP Optical correlation policy</td>
    <td style="padding:15px">{base_path}/{version}/v4/policy/ip-optical-correlation-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpOpticalCorrelationPolicy(policyId, callback)</td>
    <td style="padding:15px">Query an IP Optical correlation policy</td>
    <td style="padding:15px">{base_path}/{version}/v4/policy/ip-optical-correlation-policy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIpOpticalCorrelationPolicy(body, policyId, callback)</td>
    <td style="padding:15px">Update an IP Optical correlation policy</td>
    <td style="padding:15px">{base_path}/{version}/v4/policy/ip-optical-correlation-policy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIpOpticalCorrelationPolicy(policyId, callback)</td>
    <td style="padding:15px">Delete an IP Optical correlation policy</td>
    <td style="padding:15px">{base_path}/{version}/v4/policy/ip-optical-correlation-policy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllRdRtRanges(callback)</td>
    <td style="padding:15px">Query all L3VPN RT/RD policies</td>
    <td style="padding:15px">{base_path}/{version}/v4/policy/rd-rt-ranges?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRdRtRanges(body, callback)</td>
    <td style="padding:15px">Create a L3VPN RT/RD policy</td>
    <td style="padding:15px">{base_path}/{version}/v4/policy/rd-rt-ranges?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRdRtRanges(body, policyId, callback)</td>
    <td style="padding:15px">Update a L3VPN RT/RD policy</td>
    <td style="padding:15px">{base_path}/{version}/v4/policy/rd-rt-ranges/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRdRtRanges(policyId, callback)</td>
    <td style="padding:15px">Delete an existing L3VPN RT/RD policy.</td>
    <td style="padding:15px">{base_path}/{version}/v4/policy/rd-rt-ranges/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllRouterPortProtectionGroupPolicy(callback)</td>
    <td style="padding:15px">Query all router port protection group policies</td>
    <td style="padding:15px">{base_path}/{version}/v4/policy/router-port-protection-group-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRouterPortProtectionGroupPolicy(body, callback)</td>
    <td style="padding:15px">Create a router port protection group policy</td>
    <td style="padding:15px">{base_path}/{version}/v4/policy/router-port-protection-group-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRouterPortProtectionGroupPolicy(policyId, callback)</td>
    <td style="padding:15px">Query a router port protection group policy</td>
    <td style="padding:15px">{base_path}/{version}/v4/policy/router-port-protection-group-policy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRouterPortProtectionGroupPolicy(policyId, body, callback)</td>
    <td style="padding:15px">Update a router port protection group policy</td>
    <td style="padding:15px">{base_path}/{version}/v4/policy/router-port-protection-group-policy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRouterPortProtectionGroupPolicy(policyId, callback)</td>
    <td style="padding:15px">Delete a router port protection group policy</td>
    <td style="padding:15px">{base_path}/{version}/v4/policy/router-port-protection-group-policy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSteeringParameter(body, callback)</td>
    <td style="padding:15px">Create a Steering Parameter</td>
    <td style="padding:15px">{base_path}/{version}/v4/policy/steering-parameter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSteeringParameter(steeringParameterName, callback)</td>
    <td style="padding:15px">Delete a Steering Parameter</td>
    <td style="padding:15px">{base_path}/{version}/v4/policy/steering-parameter/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSteeringParameters(callback)</td>
    <td style="padding:15px">Query all Steering Paramters</td>
    <td style="padding:15px">{base_path}/{version}/v4/policy/steering-parameters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllTunnelSelections(callback)</td>
    <td style="padding:15px">Query all tunnel selection policies.</td>
    <td style="padding:15px">{base_path}/{version}/v4/policy/tunnel-selections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTunnelSelections(body, callback)</td>
    <td style="padding:15px">Create a tunnel selection policy.</td>
    <td style="padding:15px">{base_path}/{version}/v4/policy/tunnel-selections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTunnelSelections(policyId, callback)</td>
    <td style="padding:15px">Query a tunnel selections policy by its unique identifier.</td>
    <td style="padding:15px">{base_path}/{version}/v4/policy/tunnel-selections/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTunnelSelections(policyId, body, callback)</td>
    <td style="padding:15px">Update a tunnel selection policy.</td>
    <td style="padding:15px">{base_path}/{version}/v4/policy/tunnel-selections/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTunnelSelections(policyId, callback)</td>
    <td style="padding:15px">Delete an existing tunnel selections policy.</td>
    <td style="padding:15px">{base_path}/{version}/v4/policy/tunnel-selections/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4Ports(callback)</td>
    <td style="padding:15px">Find all ports in the network</td>
    <td style="padding:15px">{base_path}/{version}/v4/ports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNeAllByTenant(neId, tenantUuid, callback)</td>
    <td style="padding:15px">getNeAllByTenant</td>
    <td style="padding:15px">{base_path}/{version}/v4/ports/ne-all-by-tenant/{pathv1}/tenant/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNeAll(neId, callback)</td>
    <td style="padding:15px">Query all ports of the provided network element</td>
    <td style="padding:15px">{base_path}/{version}/v4/ports/ne-all/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNeByTenant(neId, serviceType = 'ODU', tenantUuid, callback)</td>
    <td style="padding:15px">getNeByTenant</td>
    <td style="padding:15px">{base_path}/{version}/v4/ports/ne-by-tenant/{pathv1}/servicetype/{pathv2}/tenant/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNe(neUuid, serviceType = 'ODU', callback)</td>
    <td style="padding:15px">Query all ports of the provided network element of a service type</td>
    <td style="padding:15px">{base_path}/{version}/v4/ports/ne/{pathv1}/servicetype/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceCountOnPort(portUuid, callback)</td>
    <td style="padding:15px">Find number of services on a specified port</td>
    <td style="padding:15px">{base_path}/{version}/v4/ports/service-count-on-port/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServicesOnPort(portUuid, callback)</td>
    <td style="padding:15px">Query all services using a port</td>
    <td style="padding:15px">{base_path}/{version}/v4/ports/services-on-port/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServicetype(serviceType = 'ODU', callback)</td>
    <td style="padding:15px">Find all ports in the network that support a given service type</td>
    <td style="padding:15px">{base_path}/{version}/v4/ports/servicetype/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServicetypeByTenant(serviceType = 'ODU', tenantUuid, callback)</td>
    <td style="padding:15px">getServicetypeByTenant</td>
    <td style="padding:15px">{base_path}/{version}/v4/ports/servicetype/{pathv1}/tenant/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTenant(tenantUuid, callback)</td>
    <td style="padding:15px">getTenant</td>
    <td style="padding:15px">{base_path}/{version}/v4/ports/tenant/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4PortsId(id, body, callback)</td>
    <td style="padding:15px">Update the port</td>
    <td style="padding:15px">{base_path}/{version}/v4/ports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4PortsPortUuid(portUuid, callback)</td>
    <td style="padding:15px">Find a port by unique identifier</td>
    <td style="padding:15px">{base_path}/{version}/v4/ports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthentication(callback)</td>
    <td style="padding:15px">Retrieve information about the currently-authenticated user from the perspective of the system</td>
    <td style="padding:15px">{base_path}/{version}/v4/security/authentication?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createConstraintTest(serviceUuid, callback)</td>
    <td style="padding:15px">BETA - Run service constraint test</td>
    <td style="padding:15px">{base_path}/{version}/v4/servicedebug/constraint-test/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDciRecompute(serviceUuid, callback)</td>
    <td style="padding:15px">Triggers re-computation for a L2 or L3 DCI Service. The DCI Service types automatically react to to</td>
    <td style="padding:15px">{base_path}/{version}/v4/servicedebug/dci-recompute/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">isMaster(callback)</td>
    <td style="padding:15px">NSP server is master</td>
    <td style="padding:15px">{base_path}/{version}/v4/system/isMaster?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setPluginConnect(pluginKey, pluginName = 'VSR_NRC', pluginVId, callback)</td>
    <td style="padding:15px">BETA: Forces NSP Plugin to form a specific connection</td>
    <td style="padding:15px">{base_path}/{version}/v4/system/plugin-connect/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resyncNms(callback)</td>
    <td style="padding:15px">Triggers a data synchronization with the connected NMS</td>
    <td style="padding:15px">{base_path}/{version}/v4/system/resync-nms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resyncObject(uuid, callback)</td>
    <td style="padding:15px">Triggers a data synchronization with the connected NMS on a specific object</td>
    <td style="padding:15px">{base_path}/{version}/v4/system/resync-object/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getState(callback)</td>
    <td style="padding:15px">Retrieves the state of the system</td>
    <td style="padding:15px">{base_path}/{version}/v4/system/state?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVersion(callback)</td>
    <td style="padding:15px">Retrieves the version of the system</td>
    <td style="padding:15px">{base_path}/{version}/v4/system/version?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllClineServices(callback)</td>
    <td style="padding:15px">Query all C-Line service creation templates</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/cline-services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createClineServices(body, callback)</td>
    <td style="padding:15px">Create a C-Line service creation template</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/cline-services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getClineServices(templateId, callback)</td>
    <td style="padding:15px">Find a C-Line service creation template by its unique identifier</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/cline-services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateClineServices(body, templateId, callback)</td>
    <td style="padding:15px">Modify a C-Line service creation template</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/cline-services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteClineServices(templateId, callback)</td>
    <td style="padding:15px">Delete a C-Line service creation template</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/cline-services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllCustomAttributes(callback)</td>
    <td style="padding:15px">Query all Custom Attributes templates</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/custom-attributes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCustomAttributes(body, callback)</td>
    <td style="padding:15px">Create a Custom Attributes template</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/custom-attributes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCustomAttributes(templateId, callback)</td>
    <td style="padding:15px">Find a Custom Attributes template by its unique identifier</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/custom-attributes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCustomAttributes(body, templateId, callback)</td>
    <td style="padding:15px">Modify a Custom Attributes template</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/custom-attributes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCustomAttributes(templateId, callback)</td>
    <td style="padding:15px">Delete a Custom Attributes template</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/custom-attributes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllElanServices(callback)</td>
    <td style="padding:15px">Query all E-LAN service creation templates</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/elan-services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createElanServices(body, callback)</td>
    <td style="padding:15px">Create an E-LAN service creation template</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/elan-services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getElanServices(templateId, callback)</td>
    <td style="padding:15px">Find an E-LAN service creation template by its unique identifier</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/elan-services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateElanServices(body, templateId, callback)</td>
    <td style="padding:15px">Modify an E-LAN service creation template</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/elan-services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteElanServices(templateId, callback)</td>
    <td style="padding:15px">Delete an E-LAN service creation template</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/elan-services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllElineServices(callback)</td>
    <td style="padding:15px">Query all E-Line service creation templates</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/eline-services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createElineServices(body, callback)</td>
    <td style="padding:15px">Create an E-Line service creation template</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/eline-services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getElineServices(templateId, callback)</td>
    <td style="padding:15px">Find an E-Line service creation template by its unique identifier</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/eline-services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateElineServices(body, templateId, callback)</td>
    <td style="padding:15px">Modify an E-Line service creation template</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/eline-services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteElineServices(templateId, callback)</td>
    <td style="padding:15px">Delete an E-Line service creation template</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/eline-services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllGenericQos(callback)</td>
    <td style="padding:15px">Query all generic QoS profiles.</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/generic-qos?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createGenericQos(body, callback)</td>
    <td style="padding:15px">Create a Generic QoS Profile</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/generic-qos?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGenericQosProfiles(neId, callback)</td>
    <td style="padding:15px">Query all generic QoS profiles of the provided network element</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/generic-qos-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGenericQos(gqpId, callback)</td>
    <td style="padding:15px">Find a generic QoS profile by its unique ID</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/generic-qos/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateGenericQos(gqpId, body, callback)</td>
    <td style="padding:15px">Update a Generic QoS Profile</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/generic-qos/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGenericQos(gqpId, callback)</td>
    <td style="padding:15px">Delete a Generic QoS Profile</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/generic-qos/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllIesServices(callback)</td>
    <td style="padding:15px">Query all IES service creation templates</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/ies-services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIesServices(body, callback)</td>
    <td style="padding:15px">Create an IES service creation template</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/ies-services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIesServices(templateId, callback)</td>
    <td style="padding:15px">Find an IES service creation template by its unique identifier</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/ies-services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIesServices(body, templateId, callback)</td>
    <td style="padding:15px">Modify an IES service creation template</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/ies-services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIesServices(templateId, callback)</td>
    <td style="padding:15px">Delete an IES service creation template</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/ies-services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllL2DciVpnServices(callback)</td>
    <td style="padding:15px">Query all L2 DCI VPN service creation templates</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/l2-dci-vpn-services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getL2DciVpnServices(templateId, callback)</td>
    <td style="padding:15px">Find an L2 DCI VPN service creation template by its unique identifier</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/l2-dci-vpn-services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateL2DciVpnServices(body, templateId, callback)</td>
    <td style="padding:15px">Modify an L2 DCI VPN service creation template</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/l2-dci-vpn-services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllL2backhaulServices(callback)</td>
    <td style="padding:15px">Query all L2 Backhaul service creation templates</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/l2backhaul-services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createL2backhaulServices(body, callback)</td>
    <td style="padding:15px">Create an L2 backhaul service creation template</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/l2backhaul-services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getL2backhaulServices(templateId, callback)</td>
    <td style="padding:15px">Find an L2 Backhaul service creation template by its unique identifier</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/l2backhaul-services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateL2backhaulServices(body, templateId, callback)</td>
    <td style="padding:15px">Modify an L2 Backhaul service creation template</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/l2backhaul-services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteL2backhaulServices(templateId, callback)</td>
    <td style="padding:15px">Delete an L2 Backhaul service creation template</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/l2backhaul-services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllL3DciVpnServices(callback)</td>
    <td style="padding:15px">Query all L3 DCI VPN service creation templates</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/l3-dci-vpn-services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createL3DciVpnServices(body, callback)</td>
    <td style="padding:15px">Create an L3 DCI VPN service creation template</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/l3-dci-vpn-services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getL3DciVpnServices(templateId, callback)</td>
    <td style="padding:15px">Find an L3 DCI VPN service creation template by its unique identifier</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/l3-dci-vpn-services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateL3DciVpnServices(body, templateId, callback)</td>
    <td style="padding:15px">Modify an L3 DCI VPN service creation template</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/l3-dci-vpn-services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteL3DciVpnServices(templateId, callback)</td>
    <td style="padding:15px">Delete an L3 DCI VPN service creation template</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/l3-dci-vpn-services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllL3VpnServices(callback)</td>
    <td style="padding:15px">Query all L3 VPN service creation templates</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/l3-vpn-services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createL3VpnServices(body, callback)</td>
    <td style="padding:15px">Create an L3 VPN service creation template</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/l3-vpn-services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getL3VpnServices(templateId, callback)</td>
    <td style="padding:15px">Find an L3 VPN service creation template by its unique identifier</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/l3-vpn-services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateL3VpnServices(body, templateId, callback)</td>
    <td style="padding:15px">Modify an L3 VPN service creation template</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/l3-vpn-services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteL3VpnServices(templateId, callback)</td>
    <td style="padding:15px">Delete an L3 VPN service creation template</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/l3-vpn-services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllLagServices(callback)</td>
    <td style="padding:15px">Query all LAG service creation templates</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/lag-services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createLagServices(body, callback)</td>
    <td style="padding:15px">Create a LAG service creation template</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/lag-services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLagServices(templateId, callback)</td>
    <td style="padding:15px">Find a LAG service creation template by its unique identifier</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/lag-services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateLagServices(body, templateId, callback)</td>
    <td style="padding:15px">Modify a LAG service creation template</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/lag-services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLagServices(templateId, callback)</td>
    <td style="padding:15px">Delete a LAG service creation template</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/lag-services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createMediationProfileMapping(body, callback)</td>
    <td style="padding:15px">Create an Mediation profile Mapping</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/mediation-profile-mapping?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMediationProfileMapping(profileId, callback)</td>
    <td style="padding:15px">Find a mediation profile mapping by its unique identifier</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/mediation-profile-mapping/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateMediationProfileMapping(body, profileId, callback)</td>
    <td style="padding:15px">Update a mediation profile mapping</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/mediation-profile-mapping/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMediationProfileMapping(profileId, callback)</td>
    <td style="padding:15px">Delete an Mediation profile mapping</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/mediation-profile-mapping/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllMediationProfileMappings(callback)</td>
    <td style="padding:15px">Query all mediation profile mappings</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/mediation-profile-mappings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNfmpTemplate(callback)</td>
    <td style="padding:15px">Query NFM-P system scripts. Some of these scripts may be used for service creation or modification.</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/nfmp-template?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllOchServices(callback)</td>
    <td style="padding:15px">Query all OCH service creation templates</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/och-services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOchServices(body, callback)</td>
    <td style="padding:15px">Create an OCH service creation template</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/och-services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOchServices(templateId, callback)</td>
    <td style="padding:15px">Find an OCH service creation template by its unique identifier</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/och-services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOchServices(body, templateId, callback)</td>
    <td style="padding:15px">Modify an OCH service creation template</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/och-services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOchServices(templateId, callback)</td>
    <td style="padding:15px">Delete an OCH service creation template</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/och-services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllOduServices(callback)</td>
    <td style="padding:15px">Query all ODU service creation templates</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/odu-services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOduServices(body, callback)</td>
    <td style="padding:15px">Create an ODU service creation template</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/odu-services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOduServices(templateId, callback)</td>
    <td style="padding:15px">Find an ODU service creation template by its unique identifier</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/odu-services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOduServices(body, templateId, callback)</td>
    <td style="padding:15px">Modify an ODU service creation template</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/odu-services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOduServices(templateId, callback)</td>
    <td style="padding:15px">Delete an ODU service creation template</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/odu-services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllOpticalConnectivityConstraint(callback)</td>
    <td style="padding:15px">Query all optical connectivity constraint templates</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/optical-connectivity-constraint?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOpticalConnectivityConstraint(body, callback)</td>
    <td style="padding:15px">Create an optical connectivity constraint creation template</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/optical-connectivity-constraint?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOpticalConnectivityConstraint(templateId, callback)</td>
    <td style="padding:15px">Find an optical connectivity constraint creation template by its unique identifier</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/optical-connectivity-constraint/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOpticalConnectivityConstraint(body, templateId, callback)</td>
    <td style="padding:15px">Modify an optical connectivity constraint creation template</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/optical-connectivity-constraint/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOpticalConnectivityConstraint(templateId, callback)</td>
    <td style="padding:15px">Delete an optical connectivity constraint creation template</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/optical-connectivity-constraint/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllOpticalConnectivityService(callback)</td>
    <td style="padding:15px">Query all optical connectivity service templates</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/optical-connectivity-service?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOpticalConnectivityService(body, callback)</td>
    <td style="padding:15px">Create an optical connectivity service creation template</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/optical-connectivity-service?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOpticalConnectivityService(templateId, callback)</td>
    <td style="padding:15px">Find an optical connectivity service creation template by its unique identifier</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/optical-connectivity-service/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOpticalConnectivityService(body, templateId, callback)</td>
    <td style="padding:15px">Modify an optical connectivity service creation template</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/optical-connectivity-service/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOpticalConnectivityService(templateId, callback)</td>
    <td style="padding:15px">Delete an optical connectivity service creation template</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/optical-connectivity-service/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllOpticalResilienceConstraint(callback)</td>
    <td style="padding:15px">Query all optical resilience constraint templates</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/optical-resilience-constraint?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOpticalResilienceConstraint(body, callback)</td>
    <td style="padding:15px">Create an optical resilience constraint creation template</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/optical-resilience-constraint?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOpticalResilienceConstraint(templateId, callback)</td>
    <td style="padding:15px">Find an optical resilience constraint creation template by its unique identifier</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/optical-resilience-constraint/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOpticalResilienceConstraint(body, templateId, callback)</td>
    <td style="padding:15px">Modify an optical resilience constraint creation template</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/optical-resilience-constraint/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOpticalResilienceConstraint(templateId, callback)</td>
    <td style="padding:15px">Delete an optical resilience constraint creation template</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/optical-resilience-constraint/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllOpticalRoutingConstraint(callback)</td>
    <td style="padding:15px">Query all optical routing constraint templates</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/optical-routing-constraint?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOpticalRoutingConstraint(body, callback)</td>
    <td style="padding:15px">Create an optical routing constraint creation template</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/optical-routing-constraint?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOpticalRoutingConstraint(templateId, callback)</td>
    <td style="padding:15px">Find an optical routing constraint creation template by its unique identifier</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/optical-routing-constraint/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOpticalRoutingConstraint(body, templateId, callback)</td>
    <td style="padding:15px">Modify an optical routing constraint creation template</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/optical-routing-constraint/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOpticalRoutingConstraint(templateId, callback)</td>
    <td style="padding:15px">Delete an optical routing constraint creation template</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/optical-routing-constraint/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllPathProfiles(callback)</td>
    <td style="padding:15px">Query all path profile templates</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/path-profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPathProfiles(body, callback)</td>
    <td style="padding:15px">Create a path profile template</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/path-profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPathProfiles(templateId, callback)</td>
    <td style="padding:15px">Find a path profile template by its unique identifier</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/path-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePathProfiles(body, templateId, callback)</td>
    <td style="padding:15px">Update a path profile template</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/path-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePathProfiles(templateId, callback)</td>
    <td style="padding:15px">Delete an existing path profile template</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/path-profiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllQos(callback)</td>
    <td style="padding:15px">Query all QoS templates</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/qos?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createQos(body, callback)</td>
    <td style="padding:15px">Create a QoS template</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/qos?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllQosPolicies(callback)</td>
    <td style="padding:15px">Query all QoS policies</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/qos-policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getQosPolicies(neId, callback)</td>
    <td style="padding:15px">Query all QoS policies of the provided network element</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/qos-policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getQos(templateId, callback)</td>
    <td style="padding:15px">Find a QoS template by its unique identifier</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/qos/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateQos(body, templateId, callback)</td>
    <td style="padding:15px">Modify a QoS template</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/qos/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteQos(templateId, callback)</td>
    <td style="padding:15px">Delete a QoS template</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/qos/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllRouterIdSystemIdMappings(callback)</td>
    <td style="padding:15px">Query all router ID system ID mapping policy</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/router-id-system-id-mapping?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRouterIdSystemIdMapping(body, callback)</td>
    <td style="padding:15px">Create a router ID system ID mapping policy</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/router-id-system-id-mapping?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRouterIdSystemIdMapping(policyId, callback)</td>
    <td style="padding:15px">Find a router-id-system-id-mapping by its unique identifier</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/router-id-system-id-mapping/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRouterIdSystemIdMapping(policyId, body, callback)</td>
    <td style="padding:15px">Modify a router-id-system-id-mapping policy</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/router-id-system-id-mapping/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRouterIdSystemIdMapping(policyId, callback)</td>
    <td style="padding:15px">Delete a router-id-mapping policy</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/router-id-system-id-mapping/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllSystemIpMplsConfig(callback)</td>
    <td style="padding:15px">Query all system IP MPLS Configuration</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/system-ip-mpls-config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSystemIpMplsConfig(body, templateId, callback)</td>
    <td style="padding:15px">Update a system ip mpls configuration</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/system-ip-mpls-config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllTunnelCreations(callback)</td>
    <td style="padding:15px">Query all tunnel creation templates. Deprecated: Use get-all-tunnel-selections instead.</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/tunnel-creations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTunnelCreations(templateId, body, callback)</td>
    <td style="padding:15px">Modify a tunnel creation template. Deprecated: update-tunnel-selections instead.</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/tunnel-creations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllWorkflowProfileMappings(callback)</td>
    <td style="padding:15px">Query all workflow profiles</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/workflow-profile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createWorkflowProfile(body, callback)</td>
    <td style="padding:15px">Create a workflow profile mapping</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/workflow-profile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWorkflowProfile(profileId, callback)</td>
    <td style="padding:15px">Find a workflow profile mapping by its unique identifier</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/workflow-profile/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateWorkflowProfile(profileId, body, callback)</td>
    <td style="padding:15px">Update a workflow profile mapping</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/workflow-profile/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteWorkflowProfile(profileId, callback)</td>
    <td style="padding:15px">Delete a workflow profile</td>
    <td style="padding:15px">{base_path}/{version}/v4/template/workflow-profile/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4Tenants(callback)</td>
    <td style="padding:15px">gets</td>
    <td style="padding:15px">{base_path}/{version}/v4/tenants?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postV4Tenants(body, callback)</td>
    <td style="padding:15px">create</td>
    <td style="padding:15px">{base_path}/{version}/v4/tenants?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCustomerAll(callback)</td>
    <td style="padding:15px">Query all Customers</td>
    <td style="padding:15px">{base_path}/{version}/v4/tenants/customer-all?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCustomer(customerId, callback)</td>
    <td style="padding:15px">Find a customer by customer-Id</td>
    <td style="padding:15px">{base_path}/{version}/v4/tenants/customer/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resync(provider = 'KEYSTONE', callback)</td>
    <td style="padding:15px">resync</td>
    <td style="padding:15px">{base_path}/{version}/v4/tenants/resync/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4TenantsTenantUuid(tenantUuid, callback)</td>
    <td style="padding:15px">get</td>
    <td style="padding:15px">{base_path}/{version}/v4/tenants/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putV4TenantsTenantUuid(body, tenantUuid, callback)</td>
    <td style="padding:15px">update</td>
    <td style="padding:15px">{base_path}/{version}/v4/tenants/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteV4TenantsTenantUuid(tenantUuid, callback)</td>
    <td style="padding:15px">delete</td>
    <td style="padding:15px">{base_path}/{version}/v4/tenants/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getResources(tenantUuid, callback)</td>
    <td style="padding:15px">getResources</td>
    <td style="padding:15px">{base_path}/{version}/v4/tenants/{pathv1}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addResources(body, tenantUuid, callback)</td>
    <td style="padding:15px">addResources</td>
    <td style="padding:15px">{base_path}/{version}/v4/tenants/{pathv1}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteResources(body, tenantUuid, callback)</td>
    <td style="padding:15px">deleteResources</td>
    <td style="padding:15px">{base_path}/{version}/v4/tenants/{pathv1}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsergroup(groupName, tenantUuid, callback)</td>
    <td style="padding:15px">getUsergroup</td>
    <td style="padding:15px">{base_path}/{version}/v4/tenants/{pathv1}/usergroup/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUsergroup(groupName, tenantUuid, callback)</td>
    <td style="padding:15px">deleteUsergroup</td>
    <td style="padding:15px">{base_path}/{version}/v4/tenants/{pathv1}/usergroup/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createUsergroup(groupName, roleType = 'OPERATOR', tenantUuid, callback)</td>
    <td style="padding:15px">createUsergroup</td>
    <td style="padding:15px">{base_path}/{version}/v4/tenants/{pathv1}/usergroup/{pathv2}/role/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4Usergroups(callback)</td>
    <td style="padding:15px">Query all Usergroups</td>
    <td style="padding:15px">{base_path}/{version}/v4/usergroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4UsergroupsGroupName(groupName, callback)</td>
    <td style="padding:15px">Find a Usergroup by a group name</td>
    <td style="padding:15px">{base_path}/{version}/v4/usergroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV4UsergroupsGroupNameTenants(groupName, callback)</td>
    <td style="padding:15px">getTenants</td>
    <td style="padding:15px">{base_path}/{version}/v4/usergroups/{pathv1}/tenants?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNrcpHistorical(callback)</td>
    <td style="padding:15px">getNrcpHistorical</td>
    <td style="padding:15px">{base_path}/{version}/v4/nsp/configuration/nrcp-historical?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchNrcpHistorical(body, callback)</td>
    <td style="padding:15px">patchNrcpHistorical</td>
    <td style="padding:15px">{base_path}/{version}/v4/nsp/configuration/nrcp-historical?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSrPolicyConfig(callback)</td>
    <td style="padding:15px">getSrPolicyConfig</td>
    <td style="padding:15px">{base_path}/{version}/v4/nsp/configuration/sr-policy-config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchSrPolicyConfig(body, callback)</td>
    <td style="padding:15px">patchSrPolicyConfig</td>
    <td style="padding:15px">{base_path}/{version}/v4/nsp/configuration/sr-policy-config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addElineEndpoint(serviceUuid, body, callback)</td>
    <td style="padding:15px">addElineEndpoint</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/elines/{pathv1}/endpoint?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addL3DciL3Endpoint(serviceUuid, body, callback)</td>
    <td style="padding:15px">addL3DciL3Endpoint</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/l3-dcis/{pathv1}/endpoint?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addL3DciL3LoopbackEndpoint(serviceUuid, body, callback)</td>
    <td style="padding:15px">addL3DciL3LoopbackEndpoint</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/l3-dcis/{pathv1}/loopback-endpoint?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchL3DciL3LoopbackEndpoint(endpointUuid, serviceUuid, body, callback)</td>
    <td style="padding:15px">patchL3DciL3LoopbackEndpoint</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/l3-dcis/{pathv1}/loopback-endpoint/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateStitchL2Ext(l2extServiceUuid, serviceEndpointUuid, serviceUuid, callback)</td>
    <td style="padding:15px">updateStitchL2Ext</td>
    <td style="padding:15px">{base_path}/{version}/v4/services/stitch-l2-ext/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTopologyReferencesByDomain(networkId, callback)</td>
    <td style="padding:15px">deleteTopologyReferencesByDomain</td>
    <td style="padding:15px">{base_path}/{version}/v4/nsp/net/l3/topology-references/domain/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTopologyReferencesByLink(linkId, callback)</td>
    <td style="padding:15px">deleteTopologyReferencesByLink</td>
    <td style="padding:15px">{base_path}/{version}/v4/nsp/net/l3/topology-references/link/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">diagnoseLsp(lspPathId, callback)</td>
    <td style="padding:15px">diagnoseLsp</td>
    <td style="padding:15px">{base_path}/{version}/v4/nsp/pathtool/diagnose-lsp/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">diagnosePath(body, callback)</td>
    <td style="padding:15px">diagnosePath</td>
    <td style="padding:15px">{base_path}/{version}/v4/nsp/pathtool/diagnose-path?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addMultipointPort(multipointLinkId, portId, callback)</td>
    <td style="padding:15px">addMultipointPort</td>
    <td style="padding:15px">{base_path}/{version}/v4/physicallinks/multipoint-port/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeMultipointPort(multipointLinkId, portId, callback)</td>
    <td style="padding:15px">removeMultipointPort</td>
    <td style="padding:15px">{base_path}/{version}/v4/physicallinks/multipoint-port/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMultipoint(linkId, callback)</td>
    <td style="padding:15px">deleteMultipoint</td>
    <td style="padding:15px">{base_path}/{version}/v4/physicallinks/multipoint/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createMultipoint(linkId, body, callback)</td>
    <td style="padding:15px">createMultipoint</td>
    <td style="padding:15px">{base_path}/{version}/v4/physicallinks/multipoint/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMultipoint(linkId, callback)</td>
    <td style="padding:15px">getMultipoint</td>
    <td style="padding:15px">{base_path}/{version}/v4/physicallinks/multipoint/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMultipoints(callback)</td>
    <td style="padding:15px">getMultipoints</td>
    <td style="padding:15px">{base_path}/{version}/v4/physicallinks/multipoints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllConnectionProfile(neId, callback)</td>
    <td style="padding:15px">getAllConnectionProfile</td>
    <td style="padding:15px">{base_path}/{version}/v4/policy/all-connection-profile/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTriggerTtzAlgorithm(callback)</td>
    <td style="padding:15px">createTriggerTtzAlgorithm</td>
    <td style="padding:15px">{base_path}/{version}/v4/servicedebug/trigger-ttz-algorithm?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSRPolicies(callback)</td>
    <td style="padding:15px">getSRPolicies</td>
    <td style="padding:15px">{base_path}/{version}/v4/srpolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSRPolicy(body, callback)</td>
    <td style="padding:15px">createSRPolicy</td>
    <td style="padding:15px">{base_path}/{version}/v4/srpolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCandidatePathResignal(id, callback)</td>
    <td style="padding:15px">createCandidatePathResignal</td>
    <td style="padding:15px">{base_path}/{version}/v4/srpolicies/candidate-path-resignal/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCandidatePathAdmin(adminState = 'MAINTENANCE', candidatePathId, callback)</td>
    <td style="padding:15px">patchCandidatePathAdmin</td>
    <td style="padding:15px">{base_path}/{version}/v4/srpolicies/candidate-path/{pathv1}/admin/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCandidatePaths(id, body, callback)</td>
    <td style="padding:15px">updateCandidatePaths</td>
    <td style="padding:15px">{base_path}/{version}/v4/srpolicies/candidate-paths/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSRPoliciesOnLink(linkId, requestType = 'MOVED', callback)</td>
    <td style="padding:15px">getSRPoliciesOnLink</td>
    <td style="padding:15px">{base_path}/{version}/v4/srpolicies/on-link/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePolicies(callback)</td>
    <td style="padding:15px">deletePolicies</td>
    <td style="padding:15px">{base_path}/{version}/v4/srpolicies/policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePolicies(id, body, callback)</td>
    <td style="padding:15px">updatePolicies</td>
    <td style="padding:15px">{base_path}/{version}/v4/srpolicies/policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">fetchPolicyList(body, callback)</td>
    <td style="padding:15px">fetchPolicyList</td>
    <td style="padding:15px">{base_path}/{version}/v4/srpolicies/policy-list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePolicy(id, callback)</td>
    <td style="padding:15px">deletePolicy</td>
    <td style="padding:15px">{base_path}/{version}/v4/srpolicies/policy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPolicyAdmin(adminState = 'MAINTENANCE', srPolicyId, callback)</td>
    <td style="padding:15px">patchPolicyAdmin</td>
    <td style="padding:15px">{base_path}/{version}/v4/srpolicies/policy/{pathv1}/admin/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSRPoliciesResignal(body, callback)</td>
    <td style="padding:15px">createSRPoliciesResignal</td>
    <td style="padding:15px">{base_path}/{version}/v4/srpolicies/resignal?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findSdnPath(body, callback)</td>
    <td style="padding:15px">findPath</td>
    <td style="padding:15px">{base_path}/{version}/v4/nsp/pathtool/find-path?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAssocGroupConfig(callback)</td>
    <td style="padding:15px">getAssocGroupConfig</td>
    <td style="padding:15px">{base_path}/{version}/v4/nsp/configuration/assoc-group-config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAssocGroupConfig(body, callback)</td>
    <td style="padding:15px">patchAssocGroupConfig</td>
    <td style="padding:15px">{base_path}/{version}/v4/nsp/configuration/assoc-group-config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBsidGenerationConfig(callback)</td>
    <td style="padding:15px">getBsidGenerationConfig</td>
    <td style="padding:15px">{base_path}/{version}/v4/nsp/configuration/bsid-generation-config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchBsidGenerationConfig(body, callback)</td>
    <td style="padding:15px">patchBsidGenerationConfig</td>
    <td style="padding:15px">{base_path}/{version}/v4/nsp/configuration/bsid-generation-config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceNameIpService(callback)</td>
    <td style="padding:15px">getDeviceNameIpService</td>
    <td style="padding:15px">{base_path}/{version}/v4/nsp/configuration/device-name-ip-service?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDeviceNameIpService(body, callback)</td>
    <td style="padding:15px">patchDeviceNameIpService</td>
    <td style="padding:15px">{base_path}/{version}/v4/nsp/configuration/device-name-ip-service?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLspBsidGenerationConfig(callback)</td>
    <td style="padding:15px">getLspBsidGenerationConfig</td>
    <td style="padding:15px">{base_path}/{version}/v4/nsp/configuration/lsp-bsid-generation-config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchLspBsidGenerationConfig(body, callback)</td>
    <td style="padding:15px">patchLspBsidGenerationConfig</td>
    <td style="padding:15px">{base_path}/{version}/v4/nsp/configuration/lsp-bsid-generation-config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAssocGroups(callback)</td>
    <td style="padding:15px">getAssocGroups</td>
    <td style="padding:15px">{base_path}/{version}/v4/mpls/assoc-groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAssocGroupsByType(associationGroupType, callback)</td>
    <td style="padding:15px">getAssocGroupsByType</td>
    <td style="padding:15px">{base_path}/{version}/v4/mpls/assoc-groups-by-type/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">lspPathProfileOverride(body, callback)</td>
    <td style="padding:15px">lspPathProfileOverride</td>
    <td style="padding:15px">{base_path}/{version}/v4/mpls/lsp-path-profile-override?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">fetchLspPathsByTunnelId(body, callback)</td>
    <td style="padding:15px">fetchLspPathsByTunnelId</td>
    <td style="padding:15px">{base_path}/{version}/v4/mpls/lsp-paths-by-tunnel-id?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOnAssocGroup(associationGroupId, associationGroupSource, associationGroupType, callback)</td>
    <td style="padding:15px">getOnAssocGroup</td>
    <td style="padding:15px">{base_path}/{version}/v4/mpls/lsp/paths/on-assoc-group/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOnBsid(bsidId, callback)</td>
    <td style="padding:15px">getOnBsid</td>
    <td style="padding:15px">{base_path}/{version}/v4/mpls/lsp/paths/on-bsid/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProfileGroup(extendedId, profileId, callback)</td>
    <td style="padding:15px">getProfileGroup</td>
    <td style="padding:15px">{base_path}/{version}/v4/mpls/lsp/paths/profile-group/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">fetchLinks(body, callback)</td>
    <td style="padding:15px">fetchLinks</td>
    <td style="padding:15px">{base_path}/{version}/v4/nsp/net/l3/links?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findPath(body, callback)</td>
    <td style="padding:15px">findPath</td>
    <td style="padding:15px">{base_path}/{version}/v4/nsp/pathtool/find-path?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getByKey(color, endpoint, headend, callback)</td>
    <td style="padding:15px">getByKey</td>
    <td style="padding:15px">{base_path}/{version}/v4/srpolicies/by-key/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">fetchCandidatePathList(body, callback)</td>
    <td style="padding:15px">fetchCandidatePathList</td>
    <td style="padding:15px">{base_path}/{version}/v4/srpolicies/candidate-path-list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConsumer(id, callback)</td>
    <td style="padding:15px">getConsumer</td>
    <td style="padding:15px">{base_path}/{version}/v4/srpolicies/consumer/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createResignalAll(callback)</td>
    <td style="padding:15px">createResignalAll</td>
    <td style="padding:15px">{base_path}/{version}/v4/srpolicies/resignal-all?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createResignalSRPolicies(body, callback)</td>
    <td style="padding:15px">createResignal</td>
    <td style="padding:15px">{base_path}/{version}/v4/srpolicies/resignal?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSRPolicyByID(id, callback)</td>
    <td style="padding:15px">gets</td>
    <td style="padding:15px">{base_path}/{version}/v4/srpolicies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
