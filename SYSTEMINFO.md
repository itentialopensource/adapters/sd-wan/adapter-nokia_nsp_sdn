# Nokia NSP Carrier SDN Inventory

Vendor: Nokia Network Services Platform
Homepage: https://www.nokia.com

Product: Nokia Network Services Platform
Product Page: https://www.nokia.com/networks/ip-networks/network-services-platform/

## Introduction
We classify Nokia NSP SDN into the SD-WAN/SASE domain as Nokia NSP SDN provides a Software Defined Wide Area Network (SD-WAN) solution. 

## Why Integrate
The Nokia NSP SDN adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Nokia's NSP SDN to aid in retrieval of various resources from the NSP specific to a Carrier SDN Managed Network.

With this adapter you have the ability to perform operations with Nokia NSP SDN such as:

- Policy
- Security
- Ports
- Links
- Tenants

## Additional Product Documentation
The [API documents for Nokia NSP SDN](https://network.developer.nokia.com/learn/23_11/network-operations/service-activation-and-configuration/carrier-sdn-inventory-apis/)