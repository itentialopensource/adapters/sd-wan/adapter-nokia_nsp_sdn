
## 0.6.5 [10-15-2024]

* Changes made at 2024.10.14_20:21PM

See merge request itentialopensource/adapters/adapter-nokia_nsp_sdn!17

---

## 0.6.4 [09-13-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-nokia_nsp_sdn!15

---

## 0.6.3 [08-14-2024]

* Changes made at 2024.08.14_18:32PM

See merge request itentialopensource/adapters/adapter-nokia_nsp_sdn!14

---

## 0.6.2 [08-06-2024]

* Changes made at 2024.08.06_19:44PM

See merge request itentialopensource/adapters/adapter-nokia_nsp_sdn!13

---

## 0.6.1 [05-30-2024]

* Added calls from most recent version of API

See merge request itentialopensource/adapters/adapter-nokia_nsp_sdn!12

---

## 0.6.0 [05-09-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/sd-wan/adapter-nokia_nsp_sdn!11

---

## 0.5.3 [03-26-2024]

* Changes made at 2024.03.26_14:08PM

See merge request itentialopensource/adapters/sd-wan/adapter-nokia_nsp_sdn!10

---

## 0.5.2 [03-11-2024]

* Changes made at 2024.03.11_10:42AM

See merge request itentialopensource/adapters/sd-wan/adapter-nokia_nsp_sdn!9

---

## 0.5.1 [02-26-2024]

* Changes made at 2024.02.26_13:05PM

See merge request itentialopensource/adapters/sd-wan/adapter-nokia_nsp_sdn!8

---

## 0.5.0 [01-03-2024]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/sd-wan/adapter-nokia_nsp_sdn!6

---

## 0.4.1 [09-11-2023]

* Revert "More migration changes"

See merge request itentialopensource/adapters/sd-wan/adapter-nokia_nsp_sdn!7

---

## 0.4.0 [05-30-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/sd-wan/adapter-nokia_nsp_sdn!3

---

## 0.3.0 [02-10-2022]

- Add new calls

See merge request itentialopensource/adapters/sd-wan/adapter-nokia_nsp_sdn!2

---

## 0.2.0 [01-24-2022]

- Migration to the latest Adapter Foundation
  - Add some items to .gitignore (e.g. DS_Store) to keep them out of the repos.
  - Changes to the README (some typo fixes - Add how to extend the adapter). Split the README into various markdown files (AUTH, BROKER, CALLS, ENHANCE, PROPERTIES, SUMMARY, SYSTEMINFO, TROUBLESHOOT)
  - Fix the issues with Confluence in the markdowns (Tables, Lists, Links)
  - Add scripts for easier authentication, removing hooks, etc
  - Script changes (install script as well as database changes in other scripts)
  - Double # of path vars on generic call
  - Update versions of foundation (e.g. adapter-utils)
  - Update npm publish so it supports https
  - Update dependencies
  - Add preinstall for minimist
  - Fix new lint issues that came from eslint dependency change
  - Add more thorough Unit tests for standard files (Package, Pronghorn, Properties (Schema and Sample)
  - Add the adapter type in the package.json
  - Add AdapterInfo.js script
  - Add json-query dependency
  - Add the propertiesDecorators.json for product encryption
  - Change the name of internal IAP/Adapter methods to avoid collisions and make more obvious in Workflow - iapRunAdapterBasicGet, iapRunAdapterConnectivity, iapRunAdapterHealthcheck, iapTroubleshootAdapter, iapGetAdapterQueue, iapUnsuspendAdapter, iapSuspendAdapter, iapFindAdapterPath, iapUpdateAdapterConfiguration, iapGetAdapterWorkflowFunctions
  - Add the adapter config in the database support - iapMoveAdapterEntitiesToDB
  - Add standard broker calls - hasEntities, getDevice, getDevicesFiltered, isAlive, getConfig and iapGetDeviceCount
  - Add genericAdapterRequest that does not use the base_path and version so that the path can be unique - genericAdapterRequestNoBasePath
  - Add AdapterInfo.json
  - Add systemName for documentation

See merge request itentialopensource/adapters/sd-wan/adapter-nokia_nsp_sdn!1

---

## 0.1.1 [08-31-2021]

- Initial Commit

See commit f64b21a

---
