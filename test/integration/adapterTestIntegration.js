/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-nokia_nsp_sdn',
      type: 'NokiaNSPSDN',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const NokiaNSPSDN = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] nokia_nsp_sdn Adapter Test', () => {
  describe('NokiaNSPSDN Class Tests', () => {
    const a = new NokiaNSPSDN(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    const sdnGenericFindByExternalIdBodyParam = {
      data: {
        context: 'NFM_T',
        id: 'string',
        location: [
          'string'
        ]
      }
    };
    describe('#findByExternalId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.findByExternalId(sdnGenericFindByExternalIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnGeneric', 'findByExternalId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnGenericId = 'fakedata';
    describe('#getApplicationId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getApplicationId(sdnGenericId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnGeneric', 'getApplicationId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnGenericAppId = 'fakedata';
    describe('#setApplicationId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.setApplicationId(sdnGenericAppId, sdnGenericId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnGeneric', 'setApplicationId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnGenericUuid = 'fakedata';
    describe('#getConsumed - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getConsumed(sdnGenericUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnGeneric', 'getConsumed', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTenants - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTenants(sdnGenericUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnGeneric', 'getTenants', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#get - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.get(sdnGenericUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnGeneric', 'get', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesServiceUuid = 'fakedata';
    const sdnServicesAddL2BackhaulEndpointBodyParam = {
      data: {
        accessLtpId: {
          ipAddress: {
            ipv4Address: {},
            ipv6Address: {}
          },
          uint32: 3
        },
        accessNodeId: {
          dottedQuad: {
            string: 'string'
          }
        },
        bandwidthProfiles: {
          egressBandwidthProfileName: 'string',
          ingressBandwidthProfileName: 'string',
          ingressEgressBandwidthProfileName: 'string'
        },
        baseEndpointRequest: {
          adminState: 'UP',
          appId: 'string',
          customAttributes: [
            {
              attributeName: 'string',
              attributeValue: 'string'
            }
          ],
          customAttributesTemplateId: 'string',
          description: 'string',
          id: 'string',
          name: 'string',
          readOnly: false
        },
        outerTag: {
          vlanClassification: {
            tagType: {},
            vlanRange: {},
            vlanValue: {}
          }
        },
        secondTag: {
          vlanClassification: {
            tagType: {},
            vlanRange: {},
            vlanValue: {}
          }
        },
        serviceClassificationType: {},
        splitHorizonGroup: 'string',
        vlanOperations: {
          asymmetricalOperation: {
            egress: {},
            ingress: {}
          },
          symmetricalOperation: {
            vlanOperations: {}
          }
        }
      }
    };
    describe('#addL2BackhaulEndpoint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addL2BackhaulEndpoint(sdnServicesAddL2BackhaulEndpointBodyParam, sdnServicesServiceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'addL2BackhaulEndpoint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesAutoPath = true;
    const sdnServicesCreateL2backhaulBodyParam = {
      data: {
        adminState: 'MAINTENANCE',
        appId: 'string',
        bidirectional: 'ASYMMETRIC_STRICT',
        bw: 4,
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        customerId: 2,
        description: 'string',
        endpoints: [
          {
            accessLtpId: {
              ipAddress: {
                ipv4Address: {},
                ipv6Address: {}
              },
              uint32: 8
            },
            accessNodeId: {
              dottedQuad: {
                string: 'string'
              }
            },
            bandwidthProfiles: {
              egressBandwidthProfileName: 'string',
              ingressBandwidthProfileName: 'string',
              ingressEgressBandwidthProfileName: 'string'
            },
            baseEndpointRequest: {
              adminState: 'DOWN',
              appId: 'string',
              customAttributes: [
                {
                  attributeName: 'string',
                  attributeValue: 'string'
                }
              ],
              customAttributesTemplateId: 'string',
              description: 'string',
              id: 'string',
              name: 'string',
              readOnly: true
            },
            outerTag: {
              vlanClassification: {
                tagType: {},
                vlanRange: {},
                vlanValue: {}
              }
            },
            secondTag: {
              vlanClassification: {
                tagType: {},
                vlanRange: {},
                vlanValue: {}
              }
            },
            serviceClassificationType: {},
            splitHorizonGroup: 'string',
            vlanOperations: {
              asymmetricalOperation: {
                egress: {},
                ingress: {}
              },
              symmetricalOperation: {
                vlanOperations: {}
              }
            }
          }
        ],
        ethtSvcDescr: 'string',
        ethtSvcName: 'string',
        ethtSvcType: {
          string: 'string'
        },
        groupId: 'string',
        maxCost: 2,
        maxHops: 10,
        maxLatency: 8,
        name: 'string',
        nodeServiceId: 6,
        objective: 'STAR_WEIGHT',
        pathProfileId: 'string',
        readOnly: true,
        requestedEndState: 'Saved',
        reverseBW: 8,
        templateId: 'string',
        tunnelSelectionId: 'string',
        workflowProfileId: 'string'
      }
    };
    describe('#createL2backhaul - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createL2backhaul(sdnServicesAutoPath, sdnServicesCreateL2backhaulBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'createL2backhaul', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesCreateClinesBodyParam = {
      data: {
        adminState: 'DOWN',
        appId: 'string',
        bidirectional: 'SYMMETRIC_STRICT',
        bw: 8,
        cemUseRtpHeader: false,
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        customerId: 3,
        description: 'string',
        endpoints: [
          {
            adminState: 'DOWN',
            appId: 'string',
            customAttributes: [
              {
                attributeName: 'string',
                attributeValue: 'string'
              }
            ],
            customAttributesTemplateId: 'string',
            description: 'string',
            id: 'string',
            name: 'string',
            readOnly: false,
            siteServiceQosProfile: {
              egressOverrideQueues: [
                {}
              ],
              egressParam: {},
              ingressOverrideQueues: [
                {}
              ],
              ingressParam: {},
              qosProfile: 8
            },
            timeSlots: 'string'
          }
        ],
        groupId: 'string',
        maxCost: 10,
        maxHops: 4,
        maxLatency: 1,
        mtu: 3,
        name: 'string',
        nodeServiceId: 4,
        objective: 'COST',
        pathProfileId: 'string',
        readOnly: false,
        requestedEndState: 'Saved',
        reverseBW: 2,
        templateId: 'string',
        transportSliceName: 'string',
        tunnelSelectionId: 'string',
        vcType: 'ETHERNET',
        workflowProfileId: 'string'
      }
    };
    describe('#createClines - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createClines(sdnServicesCreateClinesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'createClines', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesCreateEaccessBodyParam = {
      data: {
        adjacencies: [
          {
            farEndIpAddress: {
              ipv4Address: {},
              ipv6Address: {}
            },
            vcId: 9
          }
        ],
        adminState: 'UP',
        appId: 'string',
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        customerId: 8,
        endpoints: [
          {
            adminState: 'UP',
            appId: 'string',
            customAttributes: [
              {
                attributeName: 'string',
                attributeValue: 'string'
              }
            ],
            customAttributesTemplateId: 'string',
            description: 'string',
            encapType: 'Null',
            id: 'string',
            innerTag: 4,
            isHub: true,
            name: 'string',
            outerTag: 6,
            portMode: 'ACCESS',
            readOnly: false,
            siteServiceQosProfile: {
              egressOverrideQueues: [
                {}
              ],
              egressParam: {},
              ingressOverrideQueues: [
                {}
              ],
              ingressParam: {},
              qosProfile: 1
            }
          }
        ],
        mtu: 10,
        name: 'string',
        readOnly: true,
        requestedEndState: 'Deployed',
        templateId: 'string',
        tunnelSelectionId: 'string',
        vcType: 'ETHERNET'
      }
    };
    describe('#createEaccess - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createEaccess(sdnServicesCreateEaccessBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'createEaccess', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesCreateElansBodyParam = {
      data: {
        adminState: 'UP',
        appId: 'string',
        autoBindType: 'rsvp_te',
        bidirectional: 'ANY_REVERSE_ROUTE',
        bw: 3,
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        customerId: 1,
        description: 'string',
        endpointExtensions: [
          {
            connectedInner: 9,
            connectedOuter: 10,
            connectedPortId: 'string',
            customAttributes: [
              {
                attributeName: 'string',
                attributeValue: 'string'
              }
            ],
            nniPortConfig: {
              adminState: 'MAINTENANCE',
              appId: 'string',
              customAttributes: [
                {}
              ],
              customAttributesTemplateId: 'string',
              description: 'string',
              id: 'string',
              innerTag: 6,
              name: 'string',
              outerTag: 10,
              readOnly: false
            },
            uniPortConfig: {
              adminState: 'DOWN',
              appId: 'string',
              customAttributes: [
                {}
              ],
              customAttributesTemplateId: 'string',
              description: 'string',
              id: 'string',
              innerTag: 1,
              name: 'string',
              outerTag: 3,
              readOnly: true
            }
          }
        ],
        endpoints: [
          {
            adminState: 'DOWN',
            appId: 'string',
            customAttributes: [
              {
                attributeName: 'string',
                attributeValue: 'string'
              }
            ],
            customAttributesTemplateId: 'string',
            description: 'string',
            encapType: 'Unknown',
            id: 'string',
            innerTag: 5,
            isHub: true,
            name: 'string',
            outerTag: 2,
            portMode: 'ACCESS',
            readOnly: true,
            siteServiceQosProfile: {
              egressOverrideQueues: [
                {}
              ],
              egressParam: {},
              ingressOverrideQueues: [
                {}
              ],
              ingressParam: {},
              qosProfile: 10
            }
          }
        ],
        groupId: 'string',
        maxCost: 2,
        maxHops: 8,
        maxLatency: 9,
        mtu: 7,
        name: 'string',
        nodeServiceId: 9,
        objective: 'TE_METRIC',
        pathProfileId: 'string',
        readOnly: true,
        requestedEndState: 'Saved',
        reverseBW: 2,
        templateId: 'string',
        transportSliceName: 'string',
        tunnelSelectionId: 'string',
        vcType: 'SATOP_T1',
        workflowProfileId: 'string'
      }
    };
    describe('#createElans - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createElans(sdnServicesCreateElansBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'createElans', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesAddElanEndpointBodyParam = {
      data: {
        adminState: 'MAINTENANCE',
        appId: 'string',
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        description: 'string',
        encapType: 'Dot1Q',
        id: 'string',
        innerTag: 7,
        isHub: false,
        name: 'string',
        outerTag: 9,
        portMode: 'HYBRID',
        readOnly: true,
        siteServiceQosProfile: {
          egressOverrideQueues: [
            {}
          ],
          egressParam: {},
          ingressOverrideQueues: [
            {}
          ],
          ingressParam: {},
          qosProfile: 4
        }
      }
    };
    describe('#addElanEndpoint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addElanEndpoint(sdnServicesAddElanEndpointBodyParam, sdnServicesServiceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'addElanEndpoint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesCreateElinesBodyParam = {
      data: {
        adminState: 'UP',
        appId: 'string',
        autoBindType: 'sr_te',
        bidirectional: 'ANY_REVERSE_ROUTE',
        bw: 5,
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        customerId: 9,
        description: 'string',
        diverseFrom: 'string',
        endpointExtensions: [
          {
            connectedInner: 5,
            connectedOuter: 2,
            connectedPortId: 'string',
            customAttributes: [
              {
                attributeName: 'string',
                attributeValue: 'string'
              }
            ],
            nniPortConfig: {
              adminState: 'UP',
              appId: 'string',
              customAttributes: [
                {}
              ],
              customAttributesTemplateId: 'string',
              description: 'string',
              id: 'string',
              innerTag: 2,
              name: 'string',
              outerTag: 3,
              readOnly: false
            },
            uniPortConfig: {
              adminState: 'MAINTENANCE',
              appId: 'string',
              customAttributes: [
                {}
              ],
              customAttributesTemplateId: 'string',
              description: 'string',
              id: 'string',
              innerTag: 8,
              name: 'string',
              outerTag: 2,
              readOnly: true
            }
          }
        ],
        endpoints: [
          {
            adminState: 'MAINTENANCE',
            appId: 'string',
            customAttributes: [
              {
                attributeName: 'string',
                attributeValue: 'string'
              }
            ],
            customAttributesTemplateId: 'string',
            description: 'string',
            encapType: 'Unknown',
            id: 'string',
            innerTag: 8,
            isHub: true,
            name: 'string',
            outerTag: 6,
            portMode: 'UNDEFINED',
            readOnly: true,
            siteServiceQosProfile: {
              egressOverrideQueues: [
                {}
              ],
              egressParam: {},
              ingressOverrideQueues: [
                {}
              ],
              ingressParam: {},
              qosProfile: 8
            }
          }
        ],
        groupId: 'string',
        maxCost: 1,
        maxHops: 3,
        maxLatency: 3,
        monitorBandwidth: true,
        mtu: 7,
        name: 'string',
        nodeServiceId: 7,
        objective: 'TE_METRIC',
        pathProfileId: 'string',
        readOnly: false,
        requestedEndState: 'Saved',
        reverseBW: 10,
        templateId: 'string',
        transportSliceName: 'string',
        tunnelSelectionId: 'string',
        vcType: 'SATOP_E1',
        workflowProfileId: 'string'
      }
    };
    describe('#createElines - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createElines(sdnServicesCreateElinesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'createElines', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesCreateIesBodyParam = {
      data: {
        adminState: 'DOWN',
        appId: 'string',
        bidirectional: 'ASYMMETRIC_LOOSE',
        bw: 8,
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        customerId: 8,
        description: 'string',
        endpoints: [
          {
            adminState: 'DOWN',
            appId: 'string',
            customAttributes: [
              {
                attributeName: 'string',
                attributeValue: 'string'
              }
            ],
            customAttributesTemplateId: 'string',
            description: 'string',
            encapType: 'Unknown',
            id: 'string',
            innerTag: 10,
            isHub: false,
            name: 'string',
            outerTag: 1,
            portMode: 'NETWORK',
            primaryAddress: {
              ipv4Prefix: {},
              ipv6Prefix: {}
            },
            readOnly: true,
            routingBgp: {
              routes: [
                {}
              ]
            },
            routingStatic: {
              routes: [
                {}
              ]
            },
            secondaryAddresses: [
              {
                ipv4Prefix: {},
                ipv6Prefix: {}
              }
            ],
            siteServiceQosProfile: {
              egressOverrideQueues: [
                {}
              ],
              egressParam: {},
              ingressOverrideQueues: [
                {}
              ],
              ingressParam: {},
              qosProfile: 5
            },
            vrfName: 'string'
          }
        ],
        groupId: 'string',
        loopbackEndpoints: [
          {
            adminState: 'DOWN',
            appId: 'string',
            customAttributes: [
              {
                attributeName: 'string',
                attributeValue: 'string'
              }
            ],
            customAttributesTemplateId: 'string',
            description: 'string',
            id: 'string',
            isHub: true,
            name: 'string',
            primaryAddress: {
              ipv4Prefix: {},
              ipv6Prefix: {}
            },
            readOnly: true,
            vrfName: 'string'
          }
        ],
        maxCost: 10,
        maxHops: 3,
        maxLatency: 1,
        mtu: 7,
        name: 'string',
        nodeServiceId: 9,
        objective: 'STAR_WEIGHT',
        pathProfileId: 'string',
        readOnly: true,
        requestedEndState: 'Saved',
        reverseBW: 5,
        templateId: 'string',
        transportSliceName: 'string',
        tunnelSelectionId: 'string',
        vcType: 'SATOP_E3',
        workflowProfileId: 'string'
      }
    };
    describe('#createIes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createIes(sdnServicesCreateIesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'createIes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesAddIesEndpointBodyParam = {
      data: {
        adminState: 'UP',
        appId: 'string',
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        description: 'string',
        encapType: 'Dot1Q',
        id: 'string',
        innerTag: 10,
        isHub: true,
        name: 'string',
        outerTag: 7,
        portMode: 'UNDEFINED',
        primaryAddress: {
          ipv4Prefix: {},
          ipv6Prefix: {}
        },
        readOnly: true,
        routingBgp: {
          routes: [
            {}
          ]
        },
        routingStatic: {
          routes: [
            {}
          ]
        },
        secondaryAddresses: [
          {
            ipv4Prefix: {},
            ipv6Prefix: {}
          }
        ],
        siteServiceQosProfile: {
          egressOverrideQueues: [
            {}
          ],
          egressParam: {},
          ingressOverrideQueues: [
            {}
          ],
          ingressParam: {},
          qosProfile: 2
        },
        vrfName: 'string'
      }
    };
    describe('#addIesEndpoint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addIesEndpoint(sdnServicesAddIesEndpointBodyParam, sdnServicesServiceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'addIesEndpoint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesAddIesLoopbackEndpointBodyParam = {
      data: {
        adminState: 'MAINTENANCE',
        appId: 'string',
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        description: 'string',
        id: 'string',
        isHub: false,
        name: 'string',
        primaryAddress: {
          ipv4Prefix: {},
          ipv6Prefix: {}
        },
        readOnly: false,
        vrfName: 'string'
      }
    };
    describe('#addIesLoopbackEndpoint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addIesLoopbackEndpoint(sdnServicesAddIesLoopbackEndpointBodyParam, sdnServicesServiceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'addIesLoopbackEndpoint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesCreateL3DciVpnsBodyParam = {
      data: {
        adminState: 'DOWN',
        appId: 'string',
        autobind: 'sr_te',
        backhaulMtu: 10,
        bidirectional: 'ASYMMETRIC_LOOSE',
        bw: 7,
        connectivity: 'VXLAN_STITCHED',
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        customerId: 5,
        dcEndpoints: [
          {
            adminState: 'DOWN',
            appId: 'string',
            customAttributes: [
              {
                attributeName: 'string',
                attributeValue: 'string'
              }
            ],
            customAttributesTemplateId: 'string',
            description: 'string',
            domainTemplateId: 'string',
            id: 'string',
            name: 'string',
            readOnly: false
          }
        ],
        description: 'string',
        dualHomed: true,
        ecmp: 5,
        encryption: true,
        endpoints: [
          {
            adminState: 'UP',
            appId: 'string',
            customAttributes: [
              {
                attributeName: 'string',
                attributeValue: 'string'
              }
            ],
            customAttributesTemplateId: 'string',
            description: 'string',
            encapType: 'Dot1Q',
            id: 'string',
            innerTag: 7,
            isHub: false,
            name: 'string',
            outerTag: 6,
            portMode: 'ACCESS',
            primaryAddress: {
              ipv4Prefix: {},
              ipv6Prefix: {}
            },
            readOnly: false,
            routingBgp: {
              routes: [
                {}
              ]
            },
            routingStatic: {
              routes: [
                {}
              ]
            },
            secondaryAddresses: [
              {
                ipv4Prefix: {},
                ipv6Prefix: {}
              }
            ],
            siteServiceQosProfile: {
              egressOverrideQueues: [
                {}
              ],
              egressParam: {},
              ingressOverrideQueues: [
                {}
              ],
              ingressParam: {},
              qosProfile: 4
            },
            vrfName: 'string'
          }
        ],
        groupId: 'string',
        loopbackEndpoints: [
          {
            adminState: 'MAINTENANCE',
            appId: 'string',
            customAttributes: [
              {
                attributeName: 'string',
                attributeValue: 'string'
              }
            ],
            customAttributesTemplateId: 'string',
            description: 'string',
            id: 'string',
            isHub: true,
            name: 'string',
            primaryAddress: {
              ipv4Prefix: {},
              ipv6Prefix: {}
            },
            readOnly: true,
            vrfName: 'string'
          }
        ],
        maxCost: 5,
        maxHops: 1,
        maxLatency: 10,
        mtu: 10,
        name: 'string',
        nodeServiceId: 2,
        objective: 'HOPS',
        pathProfileId: 'string',
        readOnly: true,
        requestedEndState: 'Saved',
        reverseBW: 6,
        templateId: 'string',
        transportSliceName: 'string',
        tunnelSelectionId: 'string',
        vcType: 'SATOP_E3',
        workflowProfileId: 'string'
      }
    };
    describe('#createL3DciVpns - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createL3DciVpns(sdnServicesCreateL3DciVpnsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'createL3DciVpns', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesCreateL3VpnsBodyParam = {
      data: {
        adminState: 'UP',
        appId: 'string',
        autobind: 'ldp',
        bidirectional: 'ASYMMETRIC_STRICT',
        bw: 1,
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        customerId: 1,
        description: 'string',
        encryption: true,
        endpoints: [
          {
            adminState: 'MAINTENANCE',
            appId: 'string',
            customAttributes: [
              {
                attributeName: 'string',
                attributeValue: 'string'
              }
            ],
            customAttributesTemplateId: 'string',
            description: 'string',
            encapType: 'Null',
            id: 'string',
            innerTag: 10,
            isHub: true,
            name: 'string',
            outerTag: 1,
            portMode: 'HYBRID',
            primaryAddress: {
              ipv4Prefix: {},
              ipv6Prefix: {}
            },
            readOnly: false,
            routingBgp: {
              routes: [
                {}
              ]
            },
            routingStatic: {
              routes: [
                {}
              ]
            },
            secondaryAddresses: [
              {
                ipv4Prefix: {},
                ipv6Prefix: {}
              }
            ],
            siteServiceQosProfile: {
              egressOverrideQueues: [
                {}
              ],
              egressParam: {},
              ingressOverrideQueues: [
                {}
              ],
              ingressParam: {},
              qosProfile: 5
            },
            vrfName: 'string'
          }
        ],
        groupId: 'string',
        loopbackEndpoints: [
          {
            adminState: 'UP',
            appId: 'string',
            customAttributes: [
              {
                attributeName: 'string',
                attributeValue: 'string'
              }
            ],
            customAttributesTemplateId: 'string',
            description: 'string',
            id: 'string',
            isHub: false,
            name: 'string',
            primaryAddress: {
              ipv4Prefix: {},
              ipv6Prefix: {}
            },
            readOnly: false,
            vrfName: 'string'
          }
        ],
        maxCost: 3,
        maxHops: 5,
        maxLatency: 1,
        mtu: 7,
        name: 'string',
        nodeServiceId: 3,
        objective: 'STAR_WEIGHT',
        pathProfileId: 'string',
        readOnly: true,
        requestedEndState: 'Deployed',
        reverseBW: 10,
        templateId: 'string',
        transportSliceName: 'string',
        tunnelSelectionId: 'string',
        vcType: 'SATOP_T1',
        workflowProfileId: 'string'
      }
    };
    describe('#createL3Vpns - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createL3Vpns(sdnServicesCreateL3VpnsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'createL3Vpns', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesAddL3VpnEndpointBodyParam = {
      data: {
        adminState: 'UP',
        appId: 'string',
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        description: 'string',
        encapType: 'Dot1Q',
        id: 'string',
        innerTag: 2,
        isHub: false,
        name: 'string',
        outerTag: 8,
        portMode: 'NETWORK',
        primaryAddress: {
          ipv4Prefix: {},
          ipv6Prefix: {}
        },
        readOnly: true,
        routingBgp: {
          routes: [
            {}
          ]
        },
        routingStatic: {
          routes: [
            {}
          ]
        },
        secondaryAddresses: [
          {
            ipv4Prefix: {},
            ipv6Prefix: {}
          }
        ],
        siteServiceQosProfile: {
          egressOverrideQueues: [
            {}
          ],
          egressParam: {},
          ingressOverrideQueues: [
            {}
          ],
          ingressParam: {},
          qosProfile: 3
        },
        vrfName: 'string'
      }
    };
    describe('#addL3VpnEndpoint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addL3VpnEndpoint(sdnServicesAddL3VpnEndpointBodyParam, sdnServicesServiceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'addL3VpnEndpoint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesAddL3VpnLoopbackEndpointBodyParam = {
      data: {
        adminState: 'DOWN',
        appId: 'string',
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        description: 'string',
        id: 'string',
        isHub: false,
        name: 'string',
        primaryAddress: {
          ipv4Prefix: {},
          ipv6Prefix: {}
        },
        readOnly: false,
        vrfName: 'string'
      }
    };
    describe('#addL3VpnLoopbackEndpoint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addL3VpnLoopbackEndpoint(sdnServicesAddL3VpnLoopbackEndpointBodyParam, sdnServicesServiceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'addL3VpnLoopbackEndpoint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesCreateLagsBodyParam = {
      data: {
        adminState: 'DOWN',
        appId: 'string',
        bidirectional: 'SYMMETRIC_LOOSE',
        bw: 5,
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        customerId: 2,
        description: 'string',
        destinationTimeslot: 'string',
        destinations: [
          'string'
        ],
        encryption: false,
        excludeRouteObject: [
          {
            asNumber: 7,
            interfaceId: 7,
            linkId: 'string',
            linkLoose: false,
            regenInterfaceIds: [
              'string'
            ],
            regenLoose: false,
            regenRouterId: {
              string: 'string'
            },
            routerId: {
              ipv4Address: {},
              ipv6Address: {}
            },
            srlgLoose: false,
            srlgs: [
              3
            ],
            v4Address: {
              string: 'string'
            },
            v4Loose: false,
            v4PrefixLength: 10,
            v6Address: {
              string: 'string'
            },
            v6Loose: true,
            v6PrefixLength: 5,
            value: 2
          }
        ],
        facility: 4,
        groupId: 'string',
        includeRouteObject: [
          {
            asNumber: 9,
            interfaceId: 3,
            linkId: 'string',
            linkLoose: false,
            regenInterfaceIds: [
              'string'
            ],
            regenLoose: true,
            regenRouterId: {
              string: 'string'
            },
            routerId: {
              ipv4Address: {},
              ipv6Address: {}
            },
            srlgLoose: true,
            srlgs: [
              9
            ],
            v4Address: {
              string: 'string'
            },
            v4Loose: true,
            v4PrefixLength: 4,
            v6Address: {
              string: 'string'
            },
            v6Loose: false,
            v6PrefixLength: 6,
            value: 10
          }
        ],
        lambda: 7,
        maxCost: 5,
        maxHops: 10,
        maxLatency: 1,
        modulation: 'MOD_QPSK',
        monitorBandwidth: true,
        name: 'string',
        nodeServiceId: 3,
        objective: 'DISTANCE',
        pathProfileId: 'string',
        phaseEncoding: 'absolute',
        protection: true,
        protectionExcludeRouteObject: [
          {
            asNumber: 8,
            interfaceId: 5,
            linkId: 'string',
            linkLoose: true,
            regenInterfaceIds: [
              'string'
            ],
            regenLoose: false,
            regenRouterId: {
              string: 'string'
            },
            routerId: {
              ipv4Address: {},
              ipv6Address: {}
            },
            srlgLoose: false,
            srlgs: [
              1
            ],
            v4Address: {
              string: 'string'
            },
            v4Loose: false,
            v4PrefixLength: 6,
            v6Address: {
              string: 'string'
            },
            v6Loose: false,
            v6PrefixLength: 3,
            value: 6
          }
        ],
        protectionIncludeRouteObject: [
          {
            asNumber: 5,
            interfaceId: 3,
            linkId: 'string',
            linkLoose: true,
            regenInterfaceIds: [
              'string'
            ],
            regenLoose: false,
            regenRouterId: {
              string: 'string'
            },
            routerId: {
              ipv4Address: {},
              ipv6Address: {}
            },
            srlgLoose: false,
            srlgs: [
              10
            ],
            v4Address: {
              string: 'string'
            },
            v4Loose: false,
            v4PrefixLength: 2,
            v6Address: {
              string: 'string'
            },
            v6Loose: false,
            v6PrefixLength: 3,
            value: 8
          }
        ],
        protectionType: 'NA',
        readOnly: true,
        regeneration: false,
        requestedEndState: 'Saved',
        restoration: 'GR',
        reverseBW: 9,
        reversionMode: 'manual',
        servicePlaneType: 'mixedPlane',
        sourceTimeslot: 'string',
        sources: [
          'string'
        ],
        templateId: 'string',
        tunnelSelectionId: 'string',
        waveshape: 'alien',
        workflowProfileId: 'string'
      }
    };
    describe('#createLags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createLags(sdnServicesCreateLagsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'createLags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesCreateNormalizedL3VpnsBodyParam = {
      data: {
        adminState: 'DOWN',
        appId: 'string',
        autobind: 'bgp',
        bidirectional: 'ASYMMETRIC_STRICT',
        bw: 5,
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        customerId: 6,
        description: 'string',
        encryption: false,
        endpoints: [
          {
            adminState: 'UP',
            appId: 'string',
            customAttributes: [
              {
                attributeName: 'string',
                attributeValue: 'string'
              }
            ],
            customAttributesTemplateId: 'string',
            description: 'string',
            encapType: 'Unknown',
            id: 'string',
            innerTag: 2,
            isHub: true,
            name: 'string',
            outerTag: 1,
            portMode: 'HYBRID',
            primaryAddress: {
              ipv4Prefix: {},
              ipv6Prefix: {}
            },
            rd: 'string',
            readOnly: true,
            routeTargets: [
              {
                rt: 'string',
                targetType: 'exp'
              }
            ],
            routingBgp: {
              routes: [
                {}
              ]
            },
            routingStatic: {
              routes: [
                {}
              ]
            },
            secondaryAddresses: [
              {
                ipv4Prefix: {},
                ipv6Prefix: {}
              }
            ],
            siteServiceQosProfile: {
              egressOverrideQueues: [
                {}
              ],
              egressParam: {},
              ingressOverrideQueues: [
                {}
              ],
              ingressParam: {},
              qosProfile: 1
            },
            vrfName: 'string'
          }
        ],
        groupId: 'string',
        loopbackEndpoints: [
          {
            adminState: 'DOWN',
            appId: 'string',
            customAttributes: [
              {
                attributeName: 'string',
                attributeValue: 'string'
              }
            ],
            customAttributesTemplateId: 'string',
            description: 'string',
            id: 'string',
            isHub: true,
            name: 'string',
            primaryAddress: {
              ipv4Prefix: {},
              ipv6Prefix: {}
            },
            readOnly: false,
            vrfName: 'string'
          }
        ],
        maxCost: 3,
        maxHops: 6,
        maxLatency: 4,
        mtu: 6,
        name: 'string',
        nodeServiceId: 10,
        objective: 'LATENCY',
        pathProfileId: 'string',
        readOnly: true,
        requestedEndState: 'Deployed',
        reverseBW: 2,
        templateId: 'string',
        transportSliceName: 'string',
        tunnelSelectionId: 'string',
        vcType: 'SATOP_T1',
        workflowProfileId: 'string'
      }
    };
    describe('#createNormalizedL3Vpns - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createNormalizedL3Vpns(sdnServicesCreateNormalizedL3VpnsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'createNormalizedL3Vpns', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesAddL3VpnNormalizedEndpointBodyParam = {
      data: {
        adminState: 'UP',
        appId: 'string',
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        description: 'string',
        encapType: 'Unknown',
        id: 'string',
        innerTag: 5,
        isHub: false,
        name: 'string',
        outerTag: 9,
        portMode: 'HYBRID',
        primaryAddress: {
          ipv4Prefix: {},
          ipv6Prefix: {}
        },
        rd: 'string',
        readOnly: false,
        routeTargets: [
          {
            rt: 'string',
            targetType: 'imp'
          }
        ],
        routingBgp: {
          routes: [
            {}
          ]
        },
        routingStatic: {
          routes: [
            {}
          ]
        },
        secondaryAddresses: [
          {
            ipv4Prefix: {},
            ipv6Prefix: {}
          }
        ],
        siteServiceQosProfile: {
          egressOverrideQueues: [
            {}
          ],
          egressParam: {},
          ingressOverrideQueues: [
            {}
          ],
          ingressParam: {},
          qosProfile: 2
        },
        vrfName: 'string'
      }
    };
    describe('#addL3VpnNormalizedEndpoint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addL3VpnNormalizedEndpoint(sdnServicesAddL3VpnNormalizedEndpointBodyParam, sdnServicesServiceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'addL3VpnNormalizedEndpoint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesCreateOpticalBodyParam = {
      data: {
        adminState: 'DOWN',
        appId: 'string',
        bidirectional: 'ASYMMETRIC_STRICT',
        bw: 7,
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        customerId: 4,
        description: 'string',
        destinationTimeslot: 'string',
        destinations: [
          'string'
        ],
        encryption: true,
        excludeRouteObject: [
          {
            asNumber: 1,
            interfaceId: 4,
            linkId: 'string',
            linkLoose: false,
            regenInterfaceIds: [
              'string'
            ],
            regenLoose: false,
            regenRouterId: {
              string: 'string'
            },
            routerId: {
              ipv4Address: {},
              ipv6Address: {}
            },
            srlgLoose: true,
            srlgs: [
              7
            ],
            v4Address: {
              string: 'string'
            },
            v4Loose: true,
            v4PrefixLength: 1,
            v6Address: {
              string: 'string'
            },
            v6Loose: false,
            v6PrefixLength: 2,
            value: 2
          }
        ],
        facility: 2,
        groupId: 'string',
        includeRouteObject: [
          {
            asNumber: 10,
            interfaceId: 3,
            linkId: 'string',
            linkLoose: true,
            regenInterfaceIds: [
              'string'
            ],
            regenLoose: true,
            regenRouterId: {
              string: 'string'
            },
            routerId: {
              ipv4Address: {},
              ipv6Address: {}
            },
            srlgLoose: false,
            srlgs: [
              1
            ],
            v4Address: {
              string: 'string'
            },
            v4Loose: true,
            v4PrefixLength: 7,
            v6Address: {
              string: 'string'
            },
            v6Loose: false,
            v6PrefixLength: 5,
            value: 7
          }
        ],
        lambda: 2,
        maxCost: 1,
        maxHops: 3,
        maxLatency: 9,
        modulation: 'MOD_8QAM',
        name: 'string',
        nodeServiceId: 4,
        objective: 'COST',
        pathProfileId: 'string',
        phaseEncoding: 'absolute',
        protection: false,
        protectionExcludeRouteObject: [
          {
            asNumber: 7,
            interfaceId: 6,
            linkId: 'string',
            linkLoose: false,
            regenInterfaceIds: [
              'string'
            ],
            regenLoose: true,
            regenRouterId: {
              string: 'string'
            },
            routerId: {
              ipv4Address: {},
              ipv6Address: {}
            },
            srlgLoose: true,
            srlgs: [
              4
            ],
            v4Address: {
              string: 'string'
            },
            v4Loose: true,
            v4PrefixLength: 7,
            v6Address: {
              string: 'string'
            },
            v6Loose: true,
            v6PrefixLength: 6,
            value: 7
          }
        ],
        protectionIncludeRouteObject: [
          {
            asNumber: 6,
            interfaceId: 8,
            linkId: 'string',
            linkLoose: false,
            regenInterfaceIds: [
              'string'
            ],
            regenLoose: true,
            regenRouterId: {
              string: 'string'
            },
            routerId: {
              ipv4Address: {},
              ipv6Address: {}
            },
            srlgLoose: false,
            srlgs: [
              3
            ],
            v4Address: {
              string: 'string'
            },
            v4Loose: true,
            v4PrefixLength: 5,
            v6Address: {
              string: 'string'
            },
            v6Loose: true,
            v6PrefixLength: 6,
            value: 2
          }
        ],
        protectionType: 'SNCP_N_SERVER_PROTECTED',
        readOnly: true,
        regeneration: false,
        requestedEndState: 'Deployed',
        restoration: 'GR',
        reverseBW: 10,
        reversionMode: 'manual',
        servicePlaneType: 'mixedPlane',
        sourceTimeslot: 'string',
        sources: [
          'string'
        ],
        templateId: 'string',
        tunnelSelectionId: 'string',
        waveshape: '_super',
        workflowProfileId: 'string'
      }
    };
    describe('#createOptical - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createOptical(sdnServicesCreateOpticalBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'createOptical', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesEndpointUuid = 'fakedata';
    const sdnServicesUpdateL2BackhaulEndpointBodyParam = {
      data: {
        accessLtpId: {
          ipAddress: {
            ipv4Address: {},
            ipv6Address: {}
          },
          uint32: 9
        },
        accessNodeId: {
          dottedQuad: {
            string: 'string'
          }
        },
        bandwidthProfiles: {
          egressBandwidthProfileName: 'string',
          ingressBandwidthProfileName: 'string',
          ingressEgressBandwidthProfileName: 'string'
        },
        baseEndpointRequest: {
          adminState: 'DOWN',
          appId: 'string',
          customAttributes: [
            {
              attributeName: 'string',
              attributeValue: 'string'
            }
          ],
          customAttributesTemplateId: 'string',
          description: 'string',
          id: 'string',
          name: 'string',
          readOnly: false
        },
        outerTag: {
          vlanClassification: {
            tagType: {},
            vlanRange: {},
            vlanValue: {}
          }
        },
        secondTag: {
          vlanClassification: {
            tagType: {},
            vlanRange: {},
            vlanValue: {}
          }
        },
        serviceClassificationType: {},
        splitHorizonGroup: 'string',
        vlanOperations: {
          asymmetricalOperation: {
            egress: {},
            ingress: {}
          },
          symmetricalOperation: {
            vlanOperations: {}
          }
        }
      }
    };
    describe('#updateL2BackhaulEndpoint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateL2BackhaulEndpoint(sdnServicesEndpointUuid, sdnServicesUpdateL2BackhaulEndpointBodyParam, sdnServicesServiceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'updateL2BackhaulEndpoint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesPatchL2BackhaulEndpointBodyParam = {
      data: {
        accessLtpId: {
          ipAddress: {
            ipv4Address: {},
            ipv6Address: {}
          },
          uint32: 1
        },
        accessNodeId: {
          dottedQuad: {
            string: 'string'
          }
        },
        bandwidthProfiles: {
          egressBandwidthProfileName: 'string',
          ingressBandwidthProfileName: 'string',
          ingressEgressBandwidthProfileName: 'string'
        },
        baseEndpointRequest: {
          adminState: 'DOWN',
          appId: 'string',
          customAttributes: [
            {
              attributeName: 'string',
              attributeValue: 'string'
            }
          ],
          customAttributesTemplateId: 'string',
          description: 'string',
          id: 'string',
          name: 'string',
          readOnly: false
        },
        outerTag: {
          vlanClassification: {
            tagType: {},
            vlanRange: {},
            vlanValue: {}
          }
        },
        secondTag: {
          vlanClassification: {
            tagType: {},
            vlanRange: {},
            vlanValue: {}
          }
        },
        serviceClassificationType: {},
        splitHorizonGroup: 'string',
        vlanOperations: {
          asymmetricalOperation: {
            egress: {},
            ingress: {}
          },
          symmetricalOperation: {
            vlanOperations: {}
          }
        }
      }
    };
    describe('#patchL2BackhaulEndpoint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchL2BackhaulEndpoint(sdnServicesEndpointUuid, sdnServicesPatchL2BackhaulEndpointBodyParam, sdnServicesServiceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'patchL2BackhaulEndpoint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesUpdateL2backhaulBodyParam = {
      data: {
        adminState: 'DOWN',
        appId: 'string',
        bidirectional: 'NO',
        bw: 8,
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        customerId: 7,
        description: 'string',
        endpoints: [
          {
            accessLtpId: {
              ipAddress: {
                ipv4Address: {},
                ipv6Address: {}
              },
              uint32: 8
            },
            accessNodeId: {
              dottedQuad: {
                string: 'string'
              }
            },
            bandwidthProfiles: {
              egressBandwidthProfileName: 'string',
              ingressBandwidthProfileName: 'string',
              ingressEgressBandwidthProfileName: 'string'
            },
            baseEndpointRequest: {
              adminState: 'UP',
              appId: 'string',
              customAttributes: [
                {
                  attributeName: 'string',
                  attributeValue: 'string'
                }
              ],
              customAttributesTemplateId: 'string',
              description: 'string',
              id: 'string',
              name: 'string',
              readOnly: false
            },
            outerTag: {
              vlanClassification: {
                tagType: {},
                vlanRange: {},
                vlanValue: {}
              }
            },
            secondTag: {
              vlanClassification: {
                tagType: {},
                vlanRange: {},
                vlanValue: {}
              }
            },
            serviceClassificationType: {},
            splitHorizonGroup: 'string',
            vlanOperations: {
              asymmetricalOperation: {
                egress: {},
                ingress: {}
              },
              symmetricalOperation: {
                vlanOperations: {}
              }
            }
          }
        ],
        ethtSvcDescr: 'string',
        ethtSvcName: 'string',
        ethtSvcType: {
          string: 'string'
        },
        groupId: 'string',
        maxCost: 9,
        maxHops: 3,
        maxLatency: 9,
        name: 'string',
        nodeServiceId: 1,
        objective: 'DISTANCE',
        pathProfileId: 'string',
        readOnly: false,
        requestedEndState: 'Deployed',
        reverseBW: 8,
        templateId: 'string',
        tunnelSelectionId: 'string',
        workflowProfileId: 'string'
      }
    };
    describe('#updateL2backhaul - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateL2backhaul(sdnServicesUpdateL2backhaulBodyParam, sdnServicesServiceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'updateL2backhaul', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesPatchL2backhaulBodyParam = {
      data: {
        adminState: 'DOWN',
        appId: 'string',
        bidirectional: 'ASYMMETRIC_LOOSE',
        bw: 1,
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        customerId: 8,
        description: 'string',
        endpoints: [
          {
            accessLtpId: {
              ipAddress: {
                ipv4Address: {},
                ipv6Address: {}
              },
              uint32: 4
            },
            accessNodeId: {
              dottedQuad: {
                string: 'string'
              }
            },
            bandwidthProfiles: {
              egressBandwidthProfileName: 'string',
              ingressBandwidthProfileName: 'string',
              ingressEgressBandwidthProfileName: 'string'
            },
            baseEndpointRequest: {
              adminState: 'UP',
              appId: 'string',
              customAttributes: [
                {
                  attributeName: 'string',
                  attributeValue: 'string'
                }
              ],
              customAttributesTemplateId: 'string',
              description: 'string',
              id: 'string',
              name: 'string',
              readOnly: true
            },
            outerTag: {
              vlanClassification: {
                tagType: {},
                vlanRange: {},
                vlanValue: {}
              }
            },
            secondTag: {
              vlanClassification: {
                tagType: {},
                vlanRange: {},
                vlanValue: {}
              }
            },
            serviceClassificationType: {},
            splitHorizonGroup: 'string',
            vlanOperations: {
              asymmetricalOperation: {
                egress: {},
                ingress: {}
              },
              symmetricalOperation: {
                vlanOperations: {}
              }
            }
          }
        ],
        ethtSvcDescr: 'string',
        ethtSvcName: 'string',
        ethtSvcType: {
          string: 'string'
        },
        groupId: 'string',
        maxCost: 10,
        maxHops: 6,
        maxLatency: 9,
        name: 'string',
        nodeServiceId: 5,
        objective: 'STAR_WEIGHT',
        pathProfileId: 'string',
        readOnly: true,
        requestedEndState: 'Saved',
        reverseBW: 2,
        templateId: 'string',
        tunnelSelectionId: 'string',
        workflowProfileId: 'string'
      }
    };
    describe('#patchL2backhaul - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchL2backhaul(sdnServicesPatchL2backhaulBodyParam, sdnServicesServiceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'patchL2backhaul', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4Services - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4Services((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'getV4Services', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesUpdateClinesBodyParam = {
      data: {
        adminState: 'MAINTENANCE',
        appId: 'string',
        bidirectional: 'ASYMMETRIC_LOOSE',
        bw: 1,
        cemUseRtpHeader: true,
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        customerId: 5,
        description: 'string',
        endpoints: [
          {
            adminState: 'MAINTENANCE',
            appId: 'string',
            customAttributes: [
              {
                attributeName: 'string',
                attributeValue: 'string'
              }
            ],
            customAttributesTemplateId: 'string',
            description: 'string',
            id: 'string',
            name: 'string',
            readOnly: true,
            siteServiceQosProfile: {
              egressOverrideQueues: [
                {}
              ],
              egressParam: {},
              ingressOverrideQueues: [
                {}
              ],
              ingressParam: {},
              qosProfile: 5
            },
            timeSlots: 'string'
          }
        ],
        groupId: 'string',
        maxCost: 6,
        maxHops: 5,
        maxLatency: 10,
        mtu: 9,
        name: 'string',
        nodeServiceId: 1,
        objective: 'DISTANCE',
        pathProfileId: 'string',
        readOnly: false,
        requestedEndState: 'Deployed',
        reverseBW: 6,
        templateId: 'string',
        transportSliceName: 'string',
        tunnelSelectionId: 'string',
        vcType: 'SATOP_E1',
        workflowProfileId: 'string'
      }
    };
    describe('#updateClines - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateClines(sdnServicesUpdateClinesBodyParam, sdnServicesServiceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'updateClines', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesPatchClinesBodyParam = {
      data: {
        adminState: 'MAINTENANCE',
        appId: 'string',
        bidirectional: 'ASYMMETRIC_LOOSE',
        bw: 10,
        cemUseRtpHeader: true,
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        customerId: 6,
        description: 'string',
        endpoints: [
          {
            adminState: 'MAINTENANCE',
            appId: 'string',
            customAttributes: [
              {
                attributeName: 'string',
                attributeValue: 'string'
              }
            ],
            customAttributesTemplateId: 'string',
            description: 'string',
            id: 'string',
            name: 'string',
            readOnly: true,
            siteServiceQosProfile: {
              egressOverrideQueues: [
                {}
              ],
              egressParam: {},
              ingressOverrideQueues: [
                {}
              ],
              ingressParam: {},
              qosProfile: 6
            },
            timeSlots: 'string'
          }
        ],
        groupId: 'string',
        maxCost: 10,
        maxHops: 7,
        maxLatency: 3,
        mtu: 8,
        name: 'string',
        nodeServiceId: 3,
        objective: 'HOPS',
        pathProfileId: 'string',
        readOnly: false,
        requestedEndState: 'Deployed',
        reverseBW: 7,
        templateId: 'string',
        transportSliceName: 'string',
        tunnelSelectionId: 'string',
        vcType: 'ETHERNET_TAGGED_MODE',
        workflowProfileId: 'string'
      }
    };
    describe('#patchClines - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchClines(sdnServicesPatchClinesBodyParam, sdnServicesServiceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'patchClines', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesUpdateClineEndpointBodyParam = {
      data: {
        adminState: 'MAINTENANCE',
        appId: 'string',
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        description: 'string',
        id: 'string',
        name: 'string',
        readOnly: false,
        siteServiceQosProfile: {
          egressOverrideQueues: [
            {}
          ],
          egressParam: {},
          ingressOverrideQueues: [
            {}
          ],
          ingressParam: {},
          qosProfile: 3
        },
        timeSlots: 'string'
      }
    };
    describe('#updateClineEndpoint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateClineEndpoint(sdnServicesEndpointUuid, sdnServicesServiceUuid, sdnServicesUpdateClineEndpointBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'updateClineEndpoint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesPatchClineEndpointBodyParam = {
      data: {
        adminState: 'UP',
        appId: 'string',
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        description: 'string',
        id: 'string',
        name: 'string',
        readOnly: true,
        siteServiceQosProfile: {
          egressOverrideQueues: [
            {}
          ],
          egressParam: {},
          ingressOverrideQueues: [
            {}
          ],
          ingressParam: {},
          qosProfile: 5
        },
        timeSlots: 'string'
      }
    };
    describe('#patchClineEndpoint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchClineEndpoint(sdnServicesEndpointUuid, sdnServicesServiceUuid, sdnServicesPatchClineEndpointBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'patchClineEndpoint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesUuid = 'fakedata';
    describe('#getComponents - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getComponents(sdnServicesUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'getComponents', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesUpdateEaccessBodyParam = {
      data: {
        adjacencies: [
          {
            farEndIpAddress: {
              ipv4Address: {},
              ipv6Address: {}
            },
            vcId: 3
          }
        ],
        adminState: 'DOWN',
        appId: 'string',
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        customerId: 6,
        endpoints: [
          {
            adminState: 'MAINTENANCE',
            appId: 'string',
            customAttributes: [
              {
                attributeName: 'string',
                attributeValue: 'string'
              }
            ],
            customAttributesTemplateId: 'string',
            description: 'string',
            encapType: 'Unknown',
            id: 'string',
            innerTag: 3,
            isHub: true,
            name: 'string',
            outerTag: 8,
            portMode: 'UNDEFINED',
            readOnly: false,
            siteServiceQosProfile: {
              egressOverrideQueues: [
                {}
              ],
              egressParam: {},
              ingressOverrideQueues: [
                {}
              ],
              ingressParam: {},
              qosProfile: 7
            }
          }
        ],
        mtu: 8,
        name: 'string',
        readOnly: false,
        requestedEndState: 'Saved',
        templateId: 'string',
        tunnelSelectionId: 'string',
        vcType: 'ETHERNET_TAGGED_MODE'
      }
    };
    describe('#updateEaccess - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateEaccess(sdnServicesUpdateEaccessBodyParam, sdnServicesServiceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'updateEaccess', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesPatchEaccessBodyParam = {
      data: {
        adjacencies: [
          {
            farEndIpAddress: {
              ipv4Address: {},
              ipv6Address: {}
            },
            vcId: 1
          }
        ],
        adminState: 'DOWN',
        appId: 'string',
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        customerId: 5,
        endpoints: [
          {
            adminState: 'MAINTENANCE',
            appId: 'string',
            customAttributes: [
              {
                attributeName: 'string',
                attributeValue: 'string'
              }
            ],
            customAttributesTemplateId: 'string',
            description: 'string',
            encapType: 'Dot1Q',
            id: 'string',
            innerTag: 5,
            isHub: true,
            name: 'string',
            outerTag: 3,
            portMode: 'UNDEFINED',
            readOnly: true,
            siteServiceQosProfile: {
              egressOverrideQueues: [
                {}
              ],
              egressParam: {},
              ingressOverrideQueues: [
                {}
              ],
              ingressParam: {},
              qosProfile: 10
            }
          }
        ],
        mtu: 4,
        name: 'string',
        readOnly: true,
        requestedEndState: 'Deployed',
        templateId: 'string',
        tunnelSelectionId: 'string',
        vcType: 'CESOPSN_CAS'
      }
    };
    describe('#patchEaccess - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchEaccess(sdnServicesPatchEaccessBodyParam, sdnServicesServiceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'patchEaccess', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesUpdateElansBodyParam = {
      data: {
        adminState: 'MAINTENANCE',
        appId: 'string',
        autoBindType: 'any',
        bidirectional: 'SYMMETRIC_LOOSE',
        bw: 8,
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        customerId: 7,
        description: 'string',
        endpointExtensions: [
          {
            connectedInner: 8,
            connectedOuter: 5,
            connectedPortId: 'string',
            customAttributes: [
              {
                attributeName: 'string',
                attributeValue: 'string'
              }
            ],
            nniPortConfig: {
              adminState: 'DOWN',
              appId: 'string',
              customAttributes: [
                {}
              ],
              customAttributesTemplateId: 'string',
              description: 'string',
              id: 'string',
              innerTag: 5,
              name: 'string',
              outerTag: 4,
              readOnly: true
            },
            uniPortConfig: {
              adminState: 'UP',
              appId: 'string',
              customAttributes: [
                {}
              ],
              customAttributesTemplateId: 'string',
              description: 'string',
              id: 'string',
              innerTag: 8,
              name: 'string',
              outerTag: 1,
              readOnly: false
            }
          }
        ],
        endpoints: [
          {
            adminState: 'MAINTENANCE',
            appId: 'string',
            customAttributes: [
              {
                attributeName: 'string',
                attributeValue: 'string'
              }
            ],
            customAttributesTemplateId: 'string',
            description: 'string',
            encapType: 'Null',
            id: 'string',
            innerTag: 3,
            isHub: true,
            name: 'string',
            outerTag: 9,
            portMode: 'HYBRID',
            readOnly: true,
            siteServiceQosProfile: {
              egressOverrideQueues: [
                {}
              ],
              egressParam: {},
              ingressOverrideQueues: [
                {}
              ],
              ingressParam: {},
              qosProfile: 4
            }
          }
        ],
        groupId: 'string',
        maxCost: 7,
        maxHops: 1,
        maxLatency: 4,
        mtu: 4,
        name: 'string',
        nodeServiceId: 4,
        objective: 'HOPS',
        pathProfileId: 'string',
        readOnly: false,
        requestedEndState: 'Saved',
        reverseBW: 4,
        templateId: 'string',
        transportSliceName: 'string',
        tunnelSelectionId: 'string',
        vcType: 'ETHERNET',
        workflowProfileId: 'string'
      }
    };
    describe('#updateElans - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateElans(sdnServicesUpdateElansBodyParam, sdnServicesServiceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'updateElans', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesPatchElansBodyParam = {
      data: {
        adminState: 'UP',
        appId: 'string',
        autoBindType: 'sr_ospf',
        bidirectional: 'ASYMMETRIC_STRICT',
        bw: 3,
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        customerId: 1,
        description: 'string',
        endpointExtensions: [
          {
            connectedInner: 7,
            connectedOuter: 5,
            connectedPortId: 'string',
            customAttributes: [
              {
                attributeName: 'string',
                attributeValue: 'string'
              }
            ],
            nniPortConfig: {
              adminState: 'DOWN',
              appId: 'string',
              customAttributes: [
                {}
              ],
              customAttributesTemplateId: 'string',
              description: 'string',
              id: 'string',
              innerTag: 6,
              name: 'string',
              outerTag: 10,
              readOnly: false
            },
            uniPortConfig: {
              adminState: 'DOWN',
              appId: 'string',
              customAttributes: [
                {}
              ],
              customAttributesTemplateId: 'string',
              description: 'string',
              id: 'string',
              innerTag: 7,
              name: 'string',
              outerTag: 3,
              readOnly: true
            }
          }
        ],
        endpoints: [
          {
            adminState: 'UP',
            appId: 'string',
            customAttributes: [
              {
                attributeName: 'string',
                attributeValue: 'string'
              }
            ],
            customAttributesTemplateId: 'string',
            description: 'string',
            encapType: 'Dot1Q',
            id: 'string',
            innerTag: 3,
            isHub: true,
            name: 'string',
            outerTag: 10,
            portMode: 'ACCESS',
            readOnly: true,
            siteServiceQosProfile: {
              egressOverrideQueues: [
                {}
              ],
              egressParam: {},
              ingressOverrideQueues: [
                {}
              ],
              ingressParam: {},
              qosProfile: 1
            }
          }
        ],
        groupId: 'string',
        maxCost: 3,
        maxHops: 8,
        maxLatency: 8,
        mtu: 7,
        name: 'string',
        nodeServiceId: 10,
        objective: 'TE_METRIC',
        pathProfileId: 'string',
        readOnly: true,
        requestedEndState: 'Saved',
        reverseBW: 9,
        templateId: 'string',
        transportSliceName: 'string',
        tunnelSelectionId: 'string',
        vcType: 'ETHERNET_TAGGED_MODE',
        workflowProfileId: 'string'
      }
    };
    describe('#patchElans - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchElans(sdnServicesPatchElansBodyParam, sdnServicesServiceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'patchElans', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesUpdateElanEndpointBodyParam = {
      data: {
        adminState: 'DOWN',
        appId: 'string',
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        description: 'string',
        encapType: 'Null',
        id: 'string',
        innerTag: 10,
        isHub: false,
        name: 'string',
        outerTag: 10,
        portMode: 'HYBRID',
        readOnly: true,
        siteServiceQosProfile: {
          egressOverrideQueues: [
            {}
          ],
          egressParam: {},
          ingressOverrideQueues: [
            {}
          ],
          ingressParam: {},
          qosProfile: 2
        }
      }
    };
    describe('#updateElanEndpoint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateElanEndpoint(sdnServicesEndpointUuid, sdnServicesUpdateElanEndpointBodyParam, sdnServicesServiceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'updateElanEndpoint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesPatchElanEndpointBodyParam = {
      data: {
        adminState: 'MAINTENANCE',
        appId: 'string',
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        description: 'string',
        encapType: 'Null',
        id: 'string',
        innerTag: 2,
        isHub: true,
        name: 'string',
        outerTag: 1,
        portMode: 'NETWORK',
        readOnly: true,
        siteServiceQosProfile: {
          egressOverrideQueues: [
            {}
          ],
          egressParam: {},
          ingressOverrideQueues: [
            {}
          ],
          ingressParam: {},
          qosProfile: 10
        }
      }
    };
    describe('#patchElanEndpoint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchElanEndpoint(sdnServicesEndpointUuid, sdnServicesPatchElanEndpointBodyParam, sdnServicesServiceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'patchElanEndpoint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesUpdateElinesBodyParam = {
      data: {
        adminState: 'MAINTENANCE',
        appId: 'string',
        autoBindType: 'none',
        bidirectional: 'SYMMETRIC_STRICT',
        bw: 8,
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        customerId: 2,
        description: 'string',
        diverseFrom: 'string',
        endpointExtensions: [
          {
            connectedInner: 10,
            connectedOuter: 3,
            connectedPortId: 'string',
            customAttributes: [
              {
                attributeName: 'string',
                attributeValue: 'string'
              }
            ],
            nniPortConfig: {
              adminState: 'MAINTENANCE',
              appId: 'string',
              customAttributes: [
                {}
              ],
              customAttributesTemplateId: 'string',
              description: 'string',
              id: 'string',
              innerTag: 3,
              name: 'string',
              outerTag: 9,
              readOnly: false
            },
            uniPortConfig: {
              adminState: 'MAINTENANCE',
              appId: 'string',
              customAttributes: [
                {}
              ],
              customAttributesTemplateId: 'string',
              description: 'string',
              id: 'string',
              innerTag: 5,
              name: 'string',
              outerTag: 5,
              readOnly: false
            }
          }
        ],
        endpoints: [
          {
            adminState: 'UP',
            appId: 'string',
            customAttributes: [
              {
                attributeName: 'string',
                attributeValue: 'string'
              }
            ],
            customAttributesTemplateId: 'string',
            description: 'string',
            encapType: 'QinQ',
            id: 'string',
            innerTag: 9,
            isHub: false,
            name: 'string',
            outerTag: 6,
            portMode: 'UNDEFINED',
            readOnly: true,
            siteServiceQosProfile: {
              egressOverrideQueues: [
                {}
              ],
              egressParam: {},
              ingressOverrideQueues: [
                {}
              ],
              ingressParam: {},
              qosProfile: 1
            }
          }
        ],
        groupId: 'string',
        maxCost: 7,
        maxHops: 1,
        maxLatency: 7,
        monitorBandwidth: true,
        mtu: 5,
        name: 'string',
        nodeServiceId: 9,
        objective: 'TE_METRIC',
        pathProfileId: 'string',
        readOnly: true,
        requestedEndState: 'Saved',
        reverseBW: 8,
        templateId: 'string',
        transportSliceName: 'string',
        tunnelSelectionId: 'string',
        vcType: 'CESOPSN',
        workflowProfileId: 'string'
      }
    };
    describe('#updateElines - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateElines(sdnServicesUpdateElinesBodyParam, sdnServicesServiceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'updateElines', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesPatchElinesBodyParam = {
      data: {
        adminState: 'MAINTENANCE',
        appId: 'string',
        autoBindType: 'sr_isis',
        bidirectional: 'ANY_REVERSE_ROUTE',
        bw: 4,
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        customerId: 1,
        description: 'string',
        diverseFrom: 'string',
        endpointExtensions: [
          {
            connectedInner: 7,
            connectedOuter: 7,
            connectedPortId: 'string',
            customAttributes: [
              {
                attributeName: 'string',
                attributeValue: 'string'
              }
            ],
            nniPortConfig: {
              adminState: 'MAINTENANCE',
              appId: 'string',
              customAttributes: [
                {}
              ],
              customAttributesTemplateId: 'string',
              description: 'string',
              id: 'string',
              innerTag: 9,
              name: 'string',
              outerTag: 7,
              readOnly: false
            },
            uniPortConfig: {
              adminState: 'DOWN',
              appId: 'string',
              customAttributes: [
                {}
              ],
              customAttributesTemplateId: 'string',
              description: 'string',
              id: 'string',
              innerTag: 8,
              name: 'string',
              outerTag: 1,
              readOnly: true
            }
          }
        ],
        endpoints: [
          {
            adminState: 'DOWN',
            appId: 'string',
            customAttributes: [
              {
                attributeName: 'string',
                attributeValue: 'string'
              }
            ],
            customAttributesTemplateId: 'string',
            description: 'string',
            encapType: 'Unknown',
            id: 'string',
            innerTag: 7,
            isHub: false,
            name: 'string',
            outerTag: 6,
            portMode: 'HYBRID',
            readOnly: true,
            siteServiceQosProfile: {
              egressOverrideQueues: [
                {}
              ],
              egressParam: {},
              ingressOverrideQueues: [
                {}
              ],
              ingressParam: {},
              qosProfile: 5
            }
          }
        ],
        groupId: 'string',
        maxCost: 8,
        maxHops: 3,
        maxLatency: 4,
        monitorBandwidth: false,
        mtu: 4,
        name: 'string',
        nodeServiceId: 2,
        objective: 'HOPS',
        pathProfileId: 'string',
        readOnly: true,
        requestedEndState: 'Deployed',
        reverseBW: 8,
        templateId: 'string',
        transportSliceName: 'string',
        tunnelSelectionId: 'string',
        vcType: 'SATOP_T3',
        workflowProfileId: 'string'
      }
    };
    describe('#patchElines - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchElines(sdnServicesPatchElinesBodyParam, sdnServicesServiceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'patchElines', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesUpdateElineEndpointBodyParam = {
      data: {
        adminState: 'UP',
        appId: 'string',
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        description: 'string',
        encapType: 'Unknown',
        id: 'string',
        innerTag: 2,
        isHub: false,
        name: 'string',
        outerTag: 9,
        portMode: 'HYBRID',
        readOnly: false,
        siteServiceQosProfile: {
          egressOverrideQueues: [
            {}
          ],
          egressParam: {},
          ingressOverrideQueues: [
            {}
          ],
          ingressParam: {},
          qosProfile: 2
        }
      }
    };
    describe('#updateElineEndpoint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateElineEndpoint(sdnServicesEndpointUuid, sdnServicesUpdateElineEndpointBodyParam, sdnServicesServiceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'updateElineEndpoint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesPatchElineEndpointBodyParam = {
      data: {
        adminState: 'UP',
        appId: 'string',
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        description: 'string',
        encapType: 'QinQ',
        id: 'string',
        innerTag: 8,
        isHub: true,
        name: 'string',
        outerTag: 6,
        portMode: 'ACCESS',
        readOnly: false,
        siteServiceQosProfile: {
          egressOverrideQueues: [
            {}
          ],
          egressParam: {},
          ingressOverrideQueues: [
            {}
          ],
          ingressParam: {},
          qosProfile: 3
        }
      }
    };
    describe('#patchElineEndpoint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchElineEndpoint(sdnServicesEndpointUuid, sdnServicesPatchElineEndpointBodyParam, sdnServicesServiceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'patchElineEndpoint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateFixL3VpnVrfMembership - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateFixL3VpnVrfMembership(sdnServicesServiceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'updateFixL3VpnVrfMembership', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesUpdateIesBodyParam = {
      data: {
        adminState: 'UP',
        appId: 'string',
        bidirectional: 'SYMMETRIC_STRICT',
        bw: 4,
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        customerId: 6,
        description: 'string',
        endpoints: [
          {
            adminState: 'UP',
            appId: 'string',
            customAttributes: [
              {
                attributeName: 'string',
                attributeValue: 'string'
              }
            ],
            customAttributesTemplateId: 'string',
            description: 'string',
            encapType: 'Dot1Q',
            id: 'string',
            innerTag: 7,
            isHub: true,
            name: 'string',
            outerTag: 8,
            portMode: 'UNDEFINED',
            primaryAddress: {
              ipv4Prefix: {},
              ipv6Prefix: {}
            },
            readOnly: true,
            routingBgp: {
              routes: [
                {}
              ]
            },
            routingStatic: {
              routes: [
                {}
              ]
            },
            secondaryAddresses: [
              {
                ipv4Prefix: {},
                ipv6Prefix: {}
              }
            ],
            siteServiceQosProfile: {
              egressOverrideQueues: [
                {}
              ],
              egressParam: {},
              ingressOverrideQueues: [
                {}
              ],
              ingressParam: {},
              qosProfile: 9
            },
            vrfName: 'string'
          }
        ],
        groupId: 'string',
        loopbackEndpoints: [
          {
            adminState: 'MAINTENANCE',
            appId: 'string',
            customAttributes: [
              {
                attributeName: 'string',
                attributeValue: 'string'
              }
            ],
            customAttributesTemplateId: 'string',
            description: 'string',
            id: 'string',
            isHub: true,
            name: 'string',
            primaryAddress: {
              ipv4Prefix: {},
              ipv6Prefix: {}
            },
            readOnly: true,
            vrfName: 'string'
          }
        ],
        maxCost: 8,
        maxHops: 10,
        maxLatency: 6,
        mtu: 10,
        name: 'string',
        nodeServiceId: 6,
        objective: 'LATENCY',
        pathProfileId: 'string',
        readOnly: true,
        requestedEndState: 'Deployed',
        reverseBW: 3,
        templateId: 'string',
        transportSliceName: 'string',
        tunnelSelectionId: 'string',
        vcType: 'SATOP_T3',
        workflowProfileId: 'string'
      }
    };
    describe('#updateIes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateIes(sdnServicesUpdateIesBodyParam, sdnServicesServiceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'updateIes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesPatchIesBodyParam = {
      data: {
        adminState: 'DOWN',
        appId: 'string',
        bidirectional: 'ASYMMETRIC_STRICT',
        bw: 4,
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        customerId: 4,
        description: 'string',
        endpoints: [
          {
            adminState: 'UP',
            appId: 'string',
            customAttributes: [
              {
                attributeName: 'string',
                attributeValue: 'string'
              }
            ],
            customAttributesTemplateId: 'string',
            description: 'string',
            encapType: 'Null',
            id: 'string',
            innerTag: 3,
            isHub: true,
            name: 'string',
            outerTag: 1,
            portMode: 'HYBRID',
            primaryAddress: {
              ipv4Prefix: {},
              ipv6Prefix: {}
            },
            readOnly: false,
            routingBgp: {
              routes: [
                {}
              ]
            },
            routingStatic: {
              routes: [
                {}
              ]
            },
            secondaryAddresses: [
              {
                ipv4Prefix: {},
                ipv6Prefix: {}
              }
            ],
            siteServiceQosProfile: {
              egressOverrideQueues: [
                {}
              ],
              egressParam: {},
              ingressOverrideQueues: [
                {}
              ],
              ingressParam: {},
              qosProfile: 5
            },
            vrfName: 'string'
          }
        ],
        groupId: 'string',
        loopbackEndpoints: [
          {
            adminState: 'MAINTENANCE',
            appId: 'string',
            customAttributes: [
              {
                attributeName: 'string',
                attributeValue: 'string'
              }
            ],
            customAttributesTemplateId: 'string',
            description: 'string',
            id: 'string',
            isHub: true,
            name: 'string',
            primaryAddress: {
              ipv4Prefix: {},
              ipv6Prefix: {}
            },
            readOnly: true,
            vrfName: 'string'
          }
        ],
        maxCost: 9,
        maxHops: 9,
        maxLatency: 3,
        mtu: 4,
        name: 'string',
        nodeServiceId: 1,
        objective: 'COST',
        pathProfileId: 'string',
        readOnly: true,
        requestedEndState: 'Saved',
        reverseBW: 2,
        templateId: 'string',
        transportSliceName: 'string',
        tunnelSelectionId: 'string',
        vcType: 'ETHERNET',
        workflowProfileId: 'string'
      }
    };
    describe('#patchIes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchIes(sdnServicesPatchIesBodyParam, sdnServicesServiceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'patchIes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesUpdateIesEndpointBodyParam = {
      data: {
        adminState: 'DOWN',
        appId: 'string',
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        description: 'string',
        encapType: 'Unknown',
        id: 'string',
        innerTag: 4,
        isHub: false,
        name: 'string',
        outerTag: 10,
        portMode: 'UNDEFINED',
        primaryAddress: {
          ipv4Prefix: {},
          ipv6Prefix: {}
        },
        readOnly: true,
        routingBgp: {
          routes: [
            {}
          ]
        },
        routingStatic: {
          routes: [
            {}
          ]
        },
        secondaryAddresses: [
          {
            ipv4Prefix: {},
            ipv6Prefix: {}
          }
        ],
        siteServiceQosProfile: {
          egressOverrideQueues: [
            {}
          ],
          egressParam: {},
          ingressOverrideQueues: [
            {}
          ],
          ingressParam: {},
          qosProfile: 9
        },
        vrfName: 'string'
      }
    };
    describe('#updateIesEndpoint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateIesEndpoint(sdnServicesEndpointUuid, sdnServicesUpdateIesEndpointBodyParam, sdnServicesServiceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'updateIesEndpoint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesPatchIesEndpointBodyParam = {
      data: {
        adminState: 'DOWN',
        appId: 'string',
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        description: 'string',
        encapType: 'QinQ',
        id: 'string',
        innerTag: 7,
        isHub: false,
        name: 'string',
        outerTag: 10,
        portMode: 'ACCESS',
        primaryAddress: {
          ipv4Prefix: {},
          ipv6Prefix: {}
        },
        readOnly: false,
        routingBgp: {
          routes: [
            {}
          ]
        },
        routingStatic: {
          routes: [
            {}
          ]
        },
        secondaryAddresses: [
          {
            ipv4Prefix: {},
            ipv6Prefix: {}
          }
        ],
        siteServiceQosProfile: {
          egressOverrideQueues: [
            {}
          ],
          egressParam: {},
          ingressOverrideQueues: [
            {}
          ],
          ingressParam: {},
          qosProfile: 7
        },
        vrfName: 'string'
      }
    };
    describe('#patchIesEndpoint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchIesEndpoint(sdnServicesEndpointUuid, sdnServicesPatchIesEndpointBodyParam, sdnServicesServiceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'patchIesEndpoint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesUpdateIesLoopbackEndpointBodyParam = {
      data: {
        adminState: 'UP',
        appId: 'string',
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        description: 'string',
        id: 'string',
        isHub: false,
        name: 'string',
        primaryAddress: {
          ipv4Prefix: {},
          ipv6Prefix: {}
        },
        readOnly: false,
        vrfName: 'string'
      }
    };
    describe('#updateIesLoopbackEndpoint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateIesLoopbackEndpoint(sdnServicesEndpointUuid, sdnServicesUpdateIesLoopbackEndpointBodyParam, sdnServicesServiceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'updateIesLoopbackEndpoint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesPatchIesLoopbackEndpointBodyParam = {
      data: {
        adminState: 'UP',
        appId: 'string',
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        description: 'string',
        id: 'string',
        isHub: true,
        name: 'string',
        primaryAddress: {
          ipv4Prefix: {},
          ipv6Prefix: {}
        },
        readOnly: false,
        vrfName: 'string'
      }
    };
    describe('#patchIesLoopbackEndpoint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchIesLoopbackEndpoint(sdnServicesEndpointUuid, sdnServicesPatchIesLoopbackEndpointBodyParam, sdnServicesServiceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'patchIesLoopbackEndpoint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesPatchL3DciL3EndpointBodyParam = {
      data: {
        adminState: 'UP',
        appId: 'string',
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        description: 'string',
        encapType: 'Dot1Q',
        id: 'string',
        innerTag: 6,
        isHub: false,
        name: 'string',
        outerTag: 3,
        portMode: 'HYBRID',
        primaryAddress: {
          ipv4Prefix: {},
          ipv6Prefix: {}
        },
        readOnly: false,
        routingBgp: {
          routes: [
            {}
          ]
        },
        routingStatic: {
          routes: [
            {}
          ]
        },
        secondaryAddresses: [
          {
            ipv4Prefix: {},
            ipv6Prefix: {}
          }
        ],
        siteServiceQosProfile: {
          egressOverrideQueues: [
            {}
          ],
          egressParam: {},
          ingressOverrideQueues: [
            {}
          ],
          ingressParam: {},
          qosProfile: 5
        },
        vrfName: 'string'
      }
    };
    describe('#patchL3DciL3Endpoint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchL3DciL3Endpoint(sdnServicesEndpointUuid, sdnServicesPatchL3DciL3EndpointBodyParam, sdnServicesServiceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'patchL3DciL3Endpoint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getL3VpnRtAudit - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getL3VpnRtAudit((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'getL3VpnRtAudit', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesUpdateL3VpnsBodyParam = {
      data: {
        adminState: 'MAINTENANCE',
        appId: 'string',
        autobind: 'none',
        bidirectional: 'NO',
        bw: 6,
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        customerId: 7,
        description: 'string',
        encryption: true,
        endpoints: [
          {
            adminState: 'DOWN',
            appId: 'string',
            customAttributes: [
              {
                attributeName: 'string',
                attributeValue: 'string'
              }
            ],
            customAttributesTemplateId: 'string',
            description: 'string',
            encapType: 'Unknown',
            id: 'string',
            innerTag: 5,
            isHub: true,
            name: 'string',
            outerTag: 4,
            portMode: 'NETWORK',
            primaryAddress: {
              ipv4Prefix: {},
              ipv6Prefix: {}
            },
            readOnly: true,
            routingBgp: {
              routes: [
                {}
              ]
            },
            routingStatic: {
              routes: [
                {}
              ]
            },
            secondaryAddresses: [
              {
                ipv4Prefix: {},
                ipv6Prefix: {}
              }
            ],
            siteServiceQosProfile: {
              egressOverrideQueues: [
                {}
              ],
              egressParam: {},
              ingressOverrideQueues: [
                {}
              ],
              ingressParam: {},
              qosProfile: 8
            },
            vrfName: 'string'
          }
        ],
        groupId: 'string',
        loopbackEndpoints: [
          {
            adminState: 'DOWN',
            appId: 'string',
            customAttributes: [
              {
                attributeName: 'string',
                attributeValue: 'string'
              }
            ],
            customAttributesTemplateId: 'string',
            description: 'string',
            id: 'string',
            isHub: true,
            name: 'string',
            primaryAddress: {
              ipv4Prefix: {},
              ipv6Prefix: {}
            },
            readOnly: true,
            vrfName: 'string'
          }
        ],
        maxCost: 2,
        maxHops: 8,
        maxLatency: 2,
        mtu: 5,
        name: 'string',
        nodeServiceId: 4,
        objective: 'COST',
        pathProfileId: 'string',
        readOnly: true,
        requestedEndState: 'Saved',
        reverseBW: 6,
        templateId: 'string',
        transportSliceName: 'string',
        tunnelSelectionId: 'string',
        vcType: 'SATOP_E3',
        workflowProfileId: 'string'
      }
    };
    describe('#updateL3Vpns - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateL3Vpns(sdnServicesUpdateL3VpnsBodyParam, sdnServicesServiceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'updateL3Vpns', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesPatchL3VpnsBodyParam = {
      data: {
        adminState: 'UP',
        appId: 'string',
        autobind: 'sr_ospf',
        bidirectional: 'ASYMMETRIC_LOOSE',
        bw: 4,
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        customerId: 7,
        description: 'string',
        encryption: false,
        endpoints: [
          {
            adminState: 'MAINTENANCE',
            appId: 'string',
            customAttributes: [
              {
                attributeName: 'string',
                attributeValue: 'string'
              }
            ],
            customAttributesTemplateId: 'string',
            description: 'string',
            encapType: 'Dot1Q',
            id: 'string',
            innerTag: 10,
            isHub: true,
            name: 'string',
            outerTag: 5,
            portMode: 'NETWORK',
            primaryAddress: {
              ipv4Prefix: {},
              ipv6Prefix: {}
            },
            readOnly: false,
            routingBgp: {
              routes: [
                {}
              ]
            },
            routingStatic: {
              routes: [
                {}
              ]
            },
            secondaryAddresses: [
              {
                ipv4Prefix: {},
                ipv6Prefix: {}
              }
            ],
            siteServiceQosProfile: {
              egressOverrideQueues: [
                {}
              ],
              egressParam: {},
              ingressOverrideQueues: [
                {}
              ],
              ingressParam: {},
              qosProfile: 2
            },
            vrfName: 'string'
          }
        ],
        groupId: 'string',
        loopbackEndpoints: [
          {
            adminState: 'MAINTENANCE',
            appId: 'string',
            customAttributes: [
              {
                attributeName: 'string',
                attributeValue: 'string'
              }
            ],
            customAttributesTemplateId: 'string',
            description: 'string',
            id: 'string',
            isHub: true,
            name: 'string',
            primaryAddress: {
              ipv4Prefix: {},
              ipv6Prefix: {}
            },
            readOnly: true,
            vrfName: 'string'
          }
        ],
        maxCost: 7,
        maxHops: 8,
        maxLatency: 8,
        mtu: 8,
        name: 'string',
        nodeServiceId: 6,
        objective: 'DISTANCE',
        pathProfileId: 'string',
        readOnly: true,
        requestedEndState: 'Saved',
        reverseBW: 5,
        templateId: 'string',
        transportSliceName: 'string',
        tunnelSelectionId: 'string',
        vcType: 'ETHERNET_TAGGED_MODE',
        workflowProfileId: 'string'
      }
    };
    describe('#patchL3Vpns - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchL3Vpns(sdnServicesPatchL3VpnsBodyParam, sdnServicesServiceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'patchL3Vpns', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesUpdateL3VpnEndpointBodyParam = {
      data: {
        adminState: 'MAINTENANCE',
        appId: 'string',
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        description: 'string',
        encapType: 'Null',
        id: 'string',
        innerTag: 2,
        isHub: true,
        name: 'string',
        outerTag: 5,
        portMode: 'ACCESS',
        primaryAddress: {
          ipv4Prefix: {},
          ipv6Prefix: {}
        },
        readOnly: false,
        routingBgp: {
          routes: [
            {}
          ]
        },
        routingStatic: {
          routes: [
            {}
          ]
        },
        secondaryAddresses: [
          {
            ipv4Prefix: {},
            ipv6Prefix: {}
          }
        ],
        siteServiceQosProfile: {
          egressOverrideQueues: [
            {}
          ],
          egressParam: {},
          ingressOverrideQueues: [
            {}
          ],
          ingressParam: {},
          qosProfile: 4
        },
        vrfName: 'string'
      }
    };
    describe('#updateL3VpnEndpoint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateL3VpnEndpoint(sdnServicesEndpointUuid, sdnServicesUpdateL3VpnEndpointBodyParam, sdnServicesServiceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'updateL3VpnEndpoint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesPatchL3VpnEndpointBodyParam = {
      data: {
        adminState: 'DOWN',
        appId: 'string',
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        description: 'string',
        encapType: 'QinQ',
        id: 'string',
        innerTag: 1,
        isHub: true,
        name: 'string',
        outerTag: 2,
        portMode: 'NETWORK',
        primaryAddress: {
          ipv4Prefix: {},
          ipv6Prefix: {}
        },
        readOnly: true,
        routingBgp: {
          routes: [
            {}
          ]
        },
        routingStatic: {
          routes: [
            {}
          ]
        },
        secondaryAddresses: [
          {
            ipv4Prefix: {},
            ipv6Prefix: {}
          }
        ],
        siteServiceQosProfile: {
          egressOverrideQueues: [
            {}
          ],
          egressParam: {},
          ingressOverrideQueues: [
            {}
          ],
          ingressParam: {},
          qosProfile: 3
        },
        vrfName: 'string'
      }
    };
    describe('#patchL3VpnEndpoint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchL3VpnEndpoint(sdnServicesEndpointUuid, sdnServicesPatchL3VpnEndpointBodyParam, sdnServicesServiceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'patchL3VpnEndpoint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesUpdateL3VpnLoopbackEndpointBodyParam = {
      data: {
        adminState: 'DOWN',
        appId: 'string',
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        description: 'string',
        id: 'string',
        isHub: true,
        name: 'string',
        primaryAddress: {
          ipv4Prefix: {},
          ipv6Prefix: {}
        },
        readOnly: true,
        vrfName: 'string'
      }
    };
    describe('#updateL3VpnLoopbackEndpoint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateL3VpnLoopbackEndpoint(sdnServicesEndpointUuid, sdnServicesUpdateL3VpnLoopbackEndpointBodyParam, sdnServicesServiceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'updateL3VpnLoopbackEndpoint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesPatchL3VpnLoopbackEndpointBodyParam = {
      data: {
        adminState: 'DOWN',
        appId: 'string',
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        description: 'string',
        id: 'string',
        isHub: false,
        name: 'string',
        primaryAddress: {
          ipv4Prefix: {},
          ipv6Prefix: {}
        },
        readOnly: true,
        vrfName: 'string'
      }
    };
    describe('#patchL3VpnLoopbackEndpoint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchL3VpnLoopbackEndpoint(sdnServicesEndpointUuid, sdnServicesPatchL3VpnLoopbackEndpointBodyParam, sdnServicesServiceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'patchL3VpnLoopbackEndpoint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesUpdateLagsBodyParam = {
      data: {
        adminState: 'DOWN',
        appId: 'string',
        bidirectional: 'SYMMETRIC_STRICT',
        bw: 2,
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        customerId: 3,
        description: 'string',
        destinationTimeslot: 'string',
        destinations: [
          'string'
        ],
        encryption: false,
        excludeRouteObject: [
          {
            asNumber: 1,
            interfaceId: 2,
            linkId: 'string',
            linkLoose: true,
            regenInterfaceIds: [
              'string'
            ],
            regenLoose: true,
            regenRouterId: {
              string: 'string'
            },
            routerId: {
              ipv4Address: {},
              ipv6Address: {}
            },
            srlgLoose: true,
            srlgs: [
              10
            ],
            v4Address: {
              string: 'string'
            },
            v4Loose: false,
            v4PrefixLength: 3,
            v6Address: {
              string: 'string'
            },
            v6Loose: true,
            v6PrefixLength: 3,
            value: 6
          }
        ],
        facility: 9,
        groupId: 'string',
        includeRouteObject: [
          {
            asNumber: 2,
            interfaceId: 8,
            linkId: 'string',
            linkLoose: true,
            regenInterfaceIds: [
              'string'
            ],
            regenLoose: true,
            regenRouterId: {
              string: 'string'
            },
            routerId: {
              ipv4Address: {},
              ipv6Address: {}
            },
            srlgLoose: false,
            srlgs: [
              8
            ],
            v4Address: {
              string: 'string'
            },
            v4Loose: true,
            v4PrefixLength: 1,
            v6Address: {
              string: 'string'
            },
            v6Loose: true,
            v6PrefixLength: 5,
            value: 1
          }
        ],
        lambda: 10,
        maxCost: 8,
        maxHops: 6,
        maxLatency: 3,
        modulation: 'SYSTEM_ASSIGNED',
        monitorBandwidth: true,
        name: 'string',
        nodeServiceId: 8,
        objective: 'COST',
        pathProfileId: 'string',
        phaseEncoding: 'absolute',
        protection: true,
        protectionExcludeRouteObject: [
          {
            asNumber: 2,
            interfaceId: 3,
            linkId: 'string',
            linkLoose: true,
            regenInterfaceIds: [
              'string'
            ],
            regenLoose: false,
            regenRouterId: {
              string: 'string'
            },
            routerId: {
              ipv4Address: {},
              ipv6Address: {}
            },
            srlgLoose: true,
            srlgs: [
              1
            ],
            v4Address: {
              string: 'string'
            },
            v4Loose: true,
            v4PrefixLength: 4,
            v6Address: {
              string: 'string'
            },
            v6Loose: false,
            v6PrefixLength: 9,
            value: 5
          }
        ],
        protectionIncludeRouteObject: [
          {
            asNumber: 2,
            interfaceId: 10,
            linkId: 'string',
            linkLoose: false,
            regenInterfaceIds: [
              'string'
            ],
            regenLoose: false,
            regenRouterId: {
              string: 'string'
            },
            routerId: {
              ipv4Address: {},
              ipv6Address: {}
            },
            srlgLoose: false,
            srlgs: [
              7
            ],
            v4Address: {
              string: 'string'
            },
            v4Loose: true,
            v4PrefixLength: 3,
            v6Address: {
              string: 'string'
            },
            v6Loose: true,
            v6PrefixLength: 1,
            value: 10
          }
        ],
        protectionType: 'SERVER_PROTECTED',
        readOnly: true,
        regeneration: true,
        requestedEndState: 'Deployed',
        restoration: 'SBR',
        reverseBW: 6,
        reversionMode: 'auto',
        servicePlaneType: 'mixedPlane',
        sourceTimeslot: 'string',
        sources: [
          'string'
        ],
        templateId: 'string',
        tunnelSelectionId: 'string',
        waveshape: 'system_assigned',
        workflowProfileId: 'string'
      }
    };
    describe('#updateLags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateLags(sdnServicesUpdateLagsBodyParam, sdnServicesServiceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'updateLags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesUpdateNormalizedL3VpnsBodyParam = {
      data: {
        adminState: 'MAINTENANCE',
        appId: 'string',
        autobind: 'any',
        bidirectional: 'NO',
        bw: 1,
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        customerId: 4,
        description: 'string',
        encryption: true,
        endpoints: [
          {
            adminState: 'UP',
            appId: 'string',
            customAttributes: [
              {
                attributeName: 'string',
                attributeValue: 'string'
              }
            ],
            customAttributesTemplateId: 'string',
            description: 'string',
            encapType: 'QinQ',
            id: 'string',
            innerTag: 2,
            isHub: true,
            name: 'string',
            outerTag: 7,
            portMode: 'UNDEFINED',
            primaryAddress: {
              ipv4Prefix: {},
              ipv6Prefix: {}
            },
            rd: 'string',
            readOnly: false,
            routeTargets: [
              {
                rt: 'string',
                targetType: 'exp'
              }
            ],
            routingBgp: {
              routes: [
                {}
              ]
            },
            routingStatic: {
              routes: [
                {}
              ]
            },
            secondaryAddresses: [
              {
                ipv4Prefix: {},
                ipv6Prefix: {}
              }
            ],
            siteServiceQosProfile: {
              egressOverrideQueues: [
                {}
              ],
              egressParam: {},
              ingressOverrideQueues: [
                {}
              ],
              ingressParam: {},
              qosProfile: 5
            },
            vrfName: 'string'
          }
        ],
        groupId: 'string',
        loopbackEndpoints: [
          {
            adminState: 'UP',
            appId: 'string',
            customAttributes: [
              {
                attributeName: 'string',
                attributeValue: 'string'
              }
            ],
            customAttributesTemplateId: 'string',
            description: 'string',
            id: 'string',
            isHub: false,
            name: 'string',
            primaryAddress: {
              ipv4Prefix: {},
              ipv6Prefix: {}
            },
            readOnly: false,
            vrfName: 'string'
          }
        ],
        maxCost: 9,
        maxHops: 1,
        maxLatency: 4,
        mtu: 6,
        name: 'string',
        nodeServiceId: 3,
        objective: 'LATENCY',
        pathProfileId: 'string',
        readOnly: false,
        requestedEndState: 'Deployed',
        reverseBW: 3,
        templateId: 'string',
        transportSliceName: 'string',
        tunnelSelectionId: 'string',
        vcType: 'CESOPSN',
        workflowProfileId: 'string'
      }
    };
    describe('#updateNormalizedL3Vpns - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNormalizedL3Vpns(sdnServicesUpdateNormalizedL3VpnsBodyParam, sdnServicesServiceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'updateNormalizedL3Vpns', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesPatchNormalizedL3VpnsBodyParam = {
      data: {
        adminState: 'MAINTENANCE',
        appId: 'string',
        autobind: 'ldp',
        bidirectional: 'NO',
        bw: 4,
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        customerId: 7,
        description: 'string',
        encryption: true,
        endpoints: [
          {
            adminState: 'MAINTENANCE',
            appId: 'string',
            customAttributes: [
              {
                attributeName: 'string',
                attributeValue: 'string'
              }
            ],
            customAttributesTemplateId: 'string',
            description: 'string',
            encapType: 'QinQ',
            id: 'string',
            innerTag: 2,
            isHub: true,
            name: 'string',
            outerTag: 7,
            portMode: 'NETWORK',
            primaryAddress: {
              ipv4Prefix: {},
              ipv6Prefix: {}
            },
            rd: 'string',
            readOnly: false,
            routeTargets: [
              {
                rt: 'string',
                targetType: 'import_export'
              }
            ],
            routingBgp: {
              routes: [
                {}
              ]
            },
            routingStatic: {
              routes: [
                {}
              ]
            },
            secondaryAddresses: [
              {
                ipv4Prefix: {},
                ipv6Prefix: {}
              }
            ],
            siteServiceQosProfile: {
              egressOverrideQueues: [
                {}
              ],
              egressParam: {},
              ingressOverrideQueues: [
                {}
              ],
              ingressParam: {},
              qosProfile: 5
            },
            vrfName: 'string'
          }
        ],
        groupId: 'string',
        loopbackEndpoints: [
          {
            adminState: 'MAINTENANCE',
            appId: 'string',
            customAttributes: [
              {
                attributeName: 'string',
                attributeValue: 'string'
              }
            ],
            customAttributesTemplateId: 'string',
            description: 'string',
            id: 'string',
            isHub: false,
            name: 'string',
            primaryAddress: {
              ipv4Prefix: {},
              ipv6Prefix: {}
            },
            readOnly: false,
            vrfName: 'string'
          }
        ],
        maxCost: 6,
        maxHops: 2,
        maxLatency: 5,
        mtu: 10,
        name: 'string',
        nodeServiceId: 3,
        objective: 'STAR_WEIGHT',
        pathProfileId: 'string',
        readOnly: true,
        requestedEndState: 'Deployed',
        reverseBW: 1,
        templateId: 'string',
        transportSliceName: 'string',
        tunnelSelectionId: 'string',
        vcType: 'SATOP_E3',
        workflowProfileId: 'string'
      }
    };
    describe('#patchNormalizedL3Vpns - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchNormalizedL3Vpns(sdnServicesPatchNormalizedL3VpnsBodyParam, sdnServicesServiceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'patchNormalizedL3Vpns', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesUpdateOpticalBodyParam = {
      data: {
        adminState: 'UP',
        appId: 'string',
        bidirectional: 'ASYMMETRIC_STRICT',
        bw: 5,
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        customerId: 2,
        description: 'string',
        destinationTimeslot: 'string',
        destinations: [
          'string'
        ],
        encryption: true,
        excludeRouteObject: [
          {
            asNumber: 6,
            interfaceId: 5,
            linkId: 'string',
            linkLoose: true,
            regenInterfaceIds: [
              'string'
            ],
            regenLoose: false,
            regenRouterId: {
              string: 'string'
            },
            routerId: {
              ipv4Address: {},
              ipv6Address: {}
            },
            srlgLoose: false,
            srlgs: [
              1
            ],
            v4Address: {
              string: 'string'
            },
            v4Loose: true,
            v4PrefixLength: 10,
            v6Address: {
              string: 'string'
            },
            v6Loose: false,
            v6PrefixLength: 1,
            value: 8
          }
        ],
        facility: 8,
        groupId: 'string',
        includeRouteObject: [
          {
            asNumber: 4,
            interfaceId: 9,
            linkId: 'string',
            linkLoose: false,
            regenInterfaceIds: [
              'string'
            ],
            regenLoose: false,
            regenRouterId: {
              string: 'string'
            },
            routerId: {
              ipv4Address: {},
              ipv6Address: {}
            },
            srlgLoose: false,
            srlgs: [
              3
            ],
            v4Address: {
              string: 'string'
            },
            v4Loose: true,
            v4PrefixLength: 8,
            v6Address: {
              string: 'string'
            },
            v6Loose: true,
            v6PrefixLength: 10,
            value: 3
          }
        ],
        lambda: 9,
        maxCost: 9,
        maxHops: 10,
        maxLatency: 2,
        modulation: 'MOD_SP_QPSK',
        name: 'string',
        nodeServiceId: 10,
        objective: 'COST',
        pathProfileId: 'string',
        phaseEncoding: 'absolute',
        protection: false,
        protectionExcludeRouteObject: [
          {
            asNumber: 8,
            interfaceId: 3,
            linkId: 'string',
            linkLoose: false,
            regenInterfaceIds: [
              'string'
            ],
            regenLoose: true,
            regenRouterId: {
              string: 'string'
            },
            routerId: {
              ipv4Address: {},
              ipv6Address: {}
            },
            srlgLoose: false,
            srlgs: [
              7
            ],
            v4Address: {
              string: 'string'
            },
            v4Loose: true,
            v4PrefixLength: 2,
            v6Address: {
              string: 'string'
            },
            v6Loose: true,
            v6PrefixLength: 5,
            value: 2
          }
        ],
        protectionIncludeRouteObject: [
          {
            asNumber: 4,
            interfaceId: 2,
            linkId: 'string',
            linkLoose: false,
            regenInterfaceIds: [
              'string'
            ],
            regenLoose: true,
            regenRouterId: {
              string: 'string'
            },
            routerId: {
              ipv4Address: {},
              ipv6Address: {}
            },
            srlgLoose: true,
            srlgs: [
              9
            ],
            v4Address: {
              string: 'string'
            },
            v4Loose: false,
            v4PrefixLength: 6,
            v6Address: {
              string: 'string'
            },
            v6Loose: false,
            v6PrefixLength: 7,
            value: 6
          }
        ],
        protectionType: 'PROTECTED',
        readOnly: false,
        regeneration: true,
        requestedEndState: 'Deployed',
        restoration: 'GR',
        reverseBW: 5,
        reversionMode: 'auto',
        servicePlaneType: 'mrn',
        sourceTimeslot: 'string',
        sources: [
          'string'
        ],
        templateId: 'string',
        tunnelSelectionId: 'string',
        waveshape: 'alien',
        workflowProfileId: 'string'
      }
    };
    describe('#updateOptical - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateOptical(sdnServicesUpdateOpticalBodyParam, sdnServicesServiceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'updateOptical', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesPatchOpticalBodyParam = {
      data: {
        adminState: 'UP',
        appId: 'string',
        bidirectional: 'ANY_REVERSE_ROUTE',
        bw: 8,
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        customerId: 8,
        description: 'string',
        destinationTimeslot: 'string',
        destinations: [
          'string'
        ],
        encryption: false,
        excludeRouteObject: [
          {
            asNumber: 10,
            interfaceId: 10,
            linkId: 'string',
            linkLoose: true,
            regenInterfaceIds: [
              'string'
            ],
            regenLoose: true,
            regenRouterId: {
              string: 'string'
            },
            routerId: {
              ipv4Address: {},
              ipv6Address: {}
            },
            srlgLoose: true,
            srlgs: [
              5
            ],
            v4Address: {
              string: 'string'
            },
            v4Loose: true,
            v4PrefixLength: 10,
            v6Address: {
              string: 'string'
            },
            v6Loose: true,
            v6PrefixLength: 3,
            value: 3
          }
        ],
        facility: 1,
        groupId: 'string',
        includeRouteObject: [
          {
            asNumber: 3,
            interfaceId: 9,
            linkId: 'string',
            linkLoose: false,
            regenInterfaceIds: [
              'string'
            ],
            regenLoose: true,
            regenRouterId: {
              string: 'string'
            },
            routerId: {
              ipv4Address: {},
              ipv6Address: {}
            },
            srlgLoose: true,
            srlgs: [
              8
            ],
            v4Address: {
              string: 'string'
            },
            v4Loose: false,
            v4PrefixLength: 2,
            v6Address: {
              string: 'string'
            },
            v6Loose: true,
            v6PrefixLength: 5,
            value: 2
          }
        ],
        lambda: 4,
        maxCost: 2,
        maxHops: 3,
        maxLatency: 9,
        modulation: 'MOD_QPSK',
        name: 'string',
        nodeServiceId: 6,
        objective: 'HOPS',
        pathProfileId: 'string',
        phaseEncoding: 'system_assigned',
        protection: false,
        protectionExcludeRouteObject: [
          {
            asNumber: 6,
            interfaceId: 2,
            linkId: 'string',
            linkLoose: true,
            regenInterfaceIds: [
              'string'
            ],
            regenLoose: false,
            regenRouterId: {
              string: 'string'
            },
            routerId: {
              ipv4Address: {},
              ipv6Address: {}
            },
            srlgLoose: true,
            srlgs: [
              3
            ],
            v4Address: {
              string: 'string'
            },
            v4Loose: false,
            v4PrefixLength: 3,
            v6Address: {
              string: 'string'
            },
            v6Loose: false,
            v6PrefixLength: 4,
            value: 9
          }
        ],
        protectionIncludeRouteObject: [
          {
            asNumber: 7,
            interfaceId: 10,
            linkId: 'string',
            linkLoose: true,
            regenInterfaceIds: [
              'string'
            ],
            regenLoose: false,
            regenRouterId: {
              string: 'string'
            },
            routerId: {
              ipv4Address: {},
              ipv6Address: {}
            },
            srlgLoose: true,
            srlgs: [
              4
            ],
            v4Address: {
              string: 'string'
            },
            v4Loose: false,
            v4PrefixLength: 5,
            v6Address: {
              string: 'string'
            },
            v6Loose: false,
            v6PrefixLength: 5,
            value: 5
          }
        ],
        protectionType: 'SNCP_N',
        readOnly: true,
        regeneration: false,
        requestedEndState: 'Deployed',
        restoration: 'GR',
        reverseBW: 7,
        reversionMode: 'auto',
        servicePlaneType: 'mrn',
        sourceTimeslot: 'string',
        sources: [
          'string'
        ],
        templateId: 'string',
        tunnelSelectionId: 'string',
        waveshape: 'alien',
        workflowProfileId: 'string'
      }
    };
    describe('#patchOptical - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchOptical(sdnServicesPatchOpticalBodyParam, sdnServicesServiceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'patchOptical', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPhysicalLinksOnService - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPhysicalLinksOnService(sdnServicesServiceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'getPhysicalLinksOnService', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesResourceType = 'fakedata';
    const sdnServicesTenantUuid = 'fakedata';
    describe('#getResourceCount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getResourceCount(sdnServicesResourceType, sdnServicesTenantUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'getResourceCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesTunnelUuid = 'fakedata';
    describe('#getServicesOnTunnel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getServicesOnTunnel(sdnServicesTunnelUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'getServicesOnTunnel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ServicesTenantUuid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4ServicesTenantUuid(sdnServicesUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'getV4ServicesTenantUuid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesUpdateTunnelBodyParam = {
      data: {
        availableBw: 1,
        ownership: {
          consumable: true,
          deletable: false,
          modifiable: true
        },
        steeringParameters: [
          {
            name: {
              string: 'string'
            }
          }
        ]
      }
    };
    describe('#updateTunnel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateTunnel(sdnServicesUpdateTunnelBodyParam, sdnServicesTunnelUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'updateTunnel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTunnel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTunnel(sdnServicesTunnelUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'getTunnel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTunnels - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTunnels((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'getTunnels', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTunnelsOnService - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTunnelsOnService(sdnServicesServiceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'getTunnelsOnService', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEndpoints - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getEndpoints(sdnServicesServiceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'getEndpoints', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ServicesUuid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4ServicesUuid(sdnServicesUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'getV4ServicesUuid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnIetfLinkId = 'fakedata';
    describe('#getLink - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getLink(sdnIetfLinkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnIetf', 'getLink', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnIetfLinkUuid = 'fakedata';
    const sdnIetfPatchLinkBodyParam = {
      data: {
        bundledLinks: {
          bundledLink: {}
        },
        componentLinks: {
          componentLink: {}
        },
        teLinkAttributes: {
          accessType: 'point_to_point',
          adminStatus: 'maintenance',
          externalDomain: {
            plugId: 6,
            remoteTeLinkTpId: {},
            remoteTeNodeId: {}
          },
          isAbstract: {},
          name: 'string',
          performanceMetricThrottleContainer: {
            performanceMetricThrottle: {}
          },
          schedules: {
            schedules: {}
          },
          teLinkInfoAttributes: {
            administrativeGroup: {},
            interfaceSwitchingCapabilityList: {},
            linkIndex: 10,
            linkProtectionType: 'unprotected',
            teLinkConnectivityAttributes: {}
          },
          underlay: {
            teLinkStateUnderlayAttributes: {},
            teLinkUnderlayAttributes: {},
            teTopologyHierarchy: {}
          }
        },
        teLinkTemplate: [
          'string'
        ],
        template: {}
      }
    };
    describe('#patchLink - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchLink(sdnIetfLinkUuid, sdnIetfPatchLinkBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnIetf', 'patchLink', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnIetfNetworkId = 'fakedata';
    describe('#getNetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetwork(sdnIetfNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnIetf', 'getNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworks((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnIetf', 'getNetworks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnIetfNodeId = 'fakedata';
    describe('#getNode - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNode(sdnIetfNodeId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnIetf', 'getNode', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnIetfTpId = 'fakedata';
    describe('#getTerminationPoint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTerminationPoint(sdnIetfTpId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnIetf', 'getTerminationPoint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnBackhaulPathsGetL2PathsBodyParam = {
      data: {
        destination: {
          destNode: 'string',
          destTp: 'string'
        },
        erpInstance: 4,
        minBandwidth: 10,
        source: {
          sourceNode: 'string',
          sourceTp: 'string'
        },
        uuid: 'string',
        vlanId: {
          uint16: 10
        }
      }
    };
    describe('#getL2Paths - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getL2Paths(sdnBackhaulPathsGetL2PathsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnBackhaulPaths', 'getL2Paths', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnMediationCreateAugmentationMetaBodyParam = {
      data: {
        augmentationMetaInput: {
          id: 'string',
          pathName: 'string',
          templateName: 'string'
        },
        augmentationMetaJsonFileName: {
          jsonFileName: 'string'
        }
      }
    };
    describe('#createAugmentationMeta - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createAugmentationMeta(sdnMediationCreateAugmentationMetaBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnMediation', 'createAugmentationMeta', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnMediationGetMediationAugmentationBodyParam = {
      data: {
        neId: 'string',
        pathName: 'string',
        serviceType: 'L3_DCI',
        templateId: 'string'
      }
    };
    describe('#getMediationAugmentation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMediationAugmentation(sdnMediationGetMediationAugmentationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnMediation', 'getMediationAugmentation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAmiVersionTemplates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAmiVersionTemplates((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnMediation', 'getAmiVersionTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAmisVersions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAmisVersions((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnMediation', 'getAmisVersions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllAugmentationMeta - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllAugmentationMeta((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnMediation', 'getAllAugmentationMeta', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnMediationId = 'fakedata';
    const sdnMediationUpdateAugmentationMetaBodyParam = {
      data: {
        jsonFileName: 'string'
      }
    };
    describe('#updateAugmentationMeta - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAugmentationMeta(sdnMediationUpdateAugmentationMetaBodyParam, sdnMediationId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnMediation', 'updateAugmentationMeta', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnMplsFetchLspListBodyParam = {
      data: [
        'string'
      ]
    };
    describe('#fetchLspList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.fetchLspList(sdnMplsFetchLspListBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnMpls', 'fetchLspList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnMplsCreateLspPathBodyParam = {
      data: {
        administrativeState: 'UP',
        creationType: 'UNKNOWN',
        destinationAddress: {
          ipv4Address: {},
          ipv6Address: {}
        },
        paramsConfig: {
          pathParams: {
            adminGroupExcludeAny: {},
            adminGroupIncludeAll: {},
            adminGroupIncludeAny: {},
            bandwidth: 9,
            maxCost: 7,
            maxHops: 7,
            maxLatency: 5,
            maxTeMetric: 5,
            msd: 6,
            objective: 'COST',
            pathProfile: {},
            pathProfileOverride: {},
            setupPriority: 8,
            templateId: 9
          }
        },
        pathName: 'string',
        pathType: 'RSVP',
        sourceAddress: {
          ipv4Address: {},
          ipv6Address: {}
        },
        sourceRouterAddress: {
          ipv4Address: {},
          ipv6Address: {}
        }
      }
    };
    describe('#createLspPath - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createLspPath(sdnMplsCreateLspPathBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnMpls', 'createLspPath', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnMplsFetchLspPathListBodyParam = {
      data: [
        'string'
      ]
    };
    describe('#fetchLspPathList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.fetchLspPathList(sdnMplsFetchLspPathListBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnMpls', 'fetchLspPathList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnMplsCreateLspPathsBodyParam = {
      data: [
        {
          administrativeState: 'DOWN',
          creationType: 'REQUESTED',
          destinationAddress: {
            ipv4Address: {},
            ipv6Address: {}
          },
          paramsConfig: {
            pathParams: {
              adminGroupExcludeAny: {},
              adminGroupIncludeAll: {},
              adminGroupIncludeAny: {},
              bandwidth: 6,
              maxCost: 10,
              maxHops: 7,
              maxLatency: 8,
              maxTeMetric: 5,
              msd: 6,
              objective: 'TE_METRIC',
              pathProfile: {},
              pathProfileOverride: {},
              setupPriority: 8,
              templateId: 7
            }
          },
          pathName: 'string',
          pathType: 'SRTE',
          sourceAddress: {
            ipv4Address: {},
            ipv6Address: {}
          },
          sourceRouterAddress: {
            ipv4Address: {},
            ipv6Address: {}
          }
        }
      ]
    };
    describe('#createLspPaths - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createLspPaths(sdnMplsCreateLspPathsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnMpls', 'createLspPaths', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnMplsCreateOptimizationBodyParam = {
      data: [
        'string'
      ]
    };
    describe('#createOptimization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createOptimization(sdnMplsCreateOptimizationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnMpls', 'createOptimization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnMplsCreateResignalBodyParam = {
      data: [
        'string'
      ]
    };
    describe('#createResignal - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createResignal(sdnMplsCreateResignalBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnMpls', 'createResignal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnMplsPathId = 'fakedata';
    const sdnMplsPatchLspPathBodyParam = {
      data: {
        administrativeState: 'MAINTENANCE',
        creationType: 'PROVISIONED',
        destinationAddress: {
          ipv4Address: {},
          ipv6Address: {}
        },
        paramsConfig: {
          pathParams: {
            adminGroupExcludeAny: {},
            adminGroupIncludeAll: {},
            adminGroupIncludeAny: {},
            bandwidth: 2,
            maxCost: 3,
            maxHops: 1,
            maxLatency: 10,
            maxTeMetric: 8,
            msd: 3,
            objective: 'HOPS',
            pathProfile: {},
            pathProfileOverride: {},
            setupPriority: 9,
            templateId: 9
          }
        },
        pathName: 'string',
        pathType: 'RSVP',
        sourceAddress: {
          ipv4Address: {},
          ipv6Address: {}
        },
        sourceRouterAddress: {
          ipv4Address: {},
          ipv6Address: {}
        }
      }
    };
    describe('#patchLspPath - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchLspPath(sdnMplsPatchLspPathBodyParam, sdnMplsPathId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnMpls', 'patchLspPath', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLspPath - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getLspPath(sdnMplsPathId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnMpls', 'getLspPath', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLspPaths - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getLspPaths((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnMpls', 'getLspPaths', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnMplsLimit = 555;
    const sdnMplsPage = 555;
    describe('#getLspPathsPaginated - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getLspPathsPaginated(sdnMplsLimit, sdnMplsPage, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnMpls', 'getLspPathsPaginated', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnMplsLspId = 'fakedata';
    describe('#getLsp - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getLsp(sdnMplsLspId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnMpls', 'getLsp', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLsps - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getLsps((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnMpls', 'getLsps', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnMplsLspPathLinkId = 'fakedata';
    const sdnMplsLspPathRequestType = 'fakedata';
    describe('#getOnLink - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOnLink(sdnMplsLspPathLinkId, sdnMplsLspPathRequestType, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnMplsLspPath', 'getOnLink', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#gets - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.gets((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnNe', 'gets', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnNeSystemId = 'fakedata';
    describe('#getSystem - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSystem(sdnNeSystemId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnNe', 'getSystem', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnNeId = 'fakedata';
    const sdnNeUpdateBodyParam = {
      data: {
        l3vpnCapable: true,
        pwSwitchingCapable: false
      }
    };
    describe('#update - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.update(sdnNeId, sdnNeUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnNe', 'update', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnNeUuid = 'fakedata';
    describe('#getV4NeUuid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4NeUuid(sdnNeUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnNe', 'getV4NeUuid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnNrcpConfigurationPatchLatencyBodyParam = {
      data: {
        classic: false,
        modelDriven: true,
        twampTests: [
          {
            destination: 'string',
            session: 'string',
            source: 'string'
          }
        ]
      }
    };
    describe('#patchLatency - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchLatency(sdnNrcpConfigurationPatchLatencyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnNrcpConfiguration', 'patchLatency', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLatency - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getLatency((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnNrcpConfiguration', 'getLatency', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnNrcpConfigurationPatchTcaConfigPolicyBodyParam = {
      data: {
        ipTcaPolicyFdn: 'string',
        mplsTcaPolicyFdn: 'string',
        policyEnabled: false,
        sarIpTcaPolicyFdn: 'string'
      }
    };
    describe('#patchTcaConfigPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchTcaConfigPolicy(sdnNrcpConfigurationPatchTcaConfigPolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnNrcpConfiguration', 'patchTcaConfigPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTcaConfigPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTcaConfigPolicy((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnNrcpConfiguration', 'getTcaConfigPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnNrcpConfigurationPatchTrafficDataCollectionBodyParam = {
      data: {
        enabled: false,
        flowCollection: {
          mplsLabelStackQueryRate: 6,
          srTeLspEnabled: false,
          staleStatsWipeInterval: 9,
          statsTimeToLive: 5
        },
        linkBwTargetThreshold: 1,
        linkBwTriggerThreshold: 6,
        source: 'nfmp'
      }
    };
    describe('#patchTrafficDataCollection - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchTrafficDataCollection(sdnNrcpConfigurationPatchTrafficDataCollectionBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnNrcpConfiguration', 'patchTrafficDataCollection', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTrafficDataCollection - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTrafficDataCollection((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnNrcpConfiguration', 'getTrafficDataCollection', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnNspTpId = 'fakedata';
    describe('#getLinkTp - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getLinkTp(sdnNspTpId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnNsp', 'getLinkTp', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnNspLinkId = 'fakedata';
    describe('#getV4NspNetL3LinkLinkId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4NspNetL3LinkLinkId(sdnNspLinkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnNsp', 'getV4NspNetL3LinkLinkId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnNspLinkUuid = 'fakedata';
    const sdnNspPatchV4NspNetL3LinkLinkUuidBodyParam = {
      data: {
        accessType: 'point_to_point',
        adminStatus: 'MAINTENANCE',
        administrativeGroup: {
          adminGroup: {
            binary: 'string'
          },
          extendedAdminGroup: {
            binary: 'string'
          }
        },
        igpMetric: 8,
        isAbstract: {},
        latency: 7,
        maxResvLinkBandwidth: 9,
        measuredIpBw: 10,
        measuredMplsBw: 1,
        name: 'string',
        srlgValue: [
          {
            uint32: 1
          }
        ],
        teMetric: 3,
        unreservedBandwidth: 5
      }
    };
    describe('#patchV4NspNetL3LinkLinkUuid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchV4NspNetL3LinkLinkUuid(sdnNspPatchV4NspNetL3LinkLinkUuidBodyParam, sdnNspLinkUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnNsp', 'patchV4NspNetL3LinkLinkUuid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnNspNetworkId = 'fakedata';
    describe('#getV4NspNetL3NetworkNetworkId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4NspNetL3NetworkNetworkId(sdnNspNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnNsp', 'getV4NspNetL3NetworkNetworkId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4NspNetL3Networks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4NspNetL3Networks((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnNsp', 'getV4NspNetL3Networks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnNspNodeId = 'fakedata';
    describe('#getV4NspNetL3NodeNodeId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4NspNetL3NodeNodeId(sdnNspNodeId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnNsp', 'getV4NspNetL3NodeNodeId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnNspRouterId = 'fakedata';
    describe('#getNodesRouter - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNodesRouter(sdnNspRouterId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnNsp', 'getNodesRouter', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnNspSiteIp = 'fakedata';
    describe('#getNodesSite - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNodesSite(sdnNspSiteIp, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnNsp', 'getNodesSite', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4NspNetL3TerminationPointTpId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4NspNetL3TerminationPointTpId(sdnNspTpId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnNsp', 'getV4NspNetL3TerminationPointTpId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTerminationPointReverse - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTerminationPointReverse(sdnNspTpId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnNsp', 'getTerminationPointReverse', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnNspIpAddress = 'fakedata';
    describe('#getTerminationPointsNodeip - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTerminationPointsNodeip(sdnNspIpAddress, sdnNspNodeId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnNsp', 'getTerminationPointsNodeip', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnOpenflowCreateFlowsBodyParam = {
      data: [
        {
          applicationGroupId: 6,
          applicationId: 3,
          cookie: 'string',
          datapathId: 'string',
          openflowInstruction: {
            actions: [
              {
                actionType: 'EXPERIMENTER',
                experimenterActionType: 'REDIRECT_TO_SVCT',
                ipAddress: {},
                isIndirect: false,
                outerVlanId: 4,
                portId: 1,
                sdpId: 1,
                vcId: 4,
                vlanId: 5
              }
            ],
            instructionType: 'APPLY_ACTIONS'
          },
          openflowMatch: {
            destIpAddress: {
              ipv4Prefix: {},
              ipv6Prefix: {}
            },
            destPort: 4,
            dscp: {
              uint8: 1
            },
            ethernetType: 'string',
            inPort: 9,
            ipProtocolType: 1,
            outerVlanId: 8,
            sourceIpAddress: {
              ipv4Prefix: {},
              ipv6Prefix: {}
            },
            sourcePort: 4,
            vlanId: 3
          },
          priority: 9,
          tableId: 6
        }
      ]
    };
    describe('#createFlows - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createFlows(sdnOpenflowCreateFlowsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.result);
                assert.equal(true, data.response.setOrExpired);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnOpenflow', 'createFlows', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnOpenflowGetFlowsearchBodyParam = {
      data: {
        applicationGroupId: 2,
        applicationId: 10,
        cookie: 'string',
        datapathId: 'string',
        headerOnly: true,
        openflowMatch: {
          destIpAddress: {
            ipv4Prefix: {},
            ipv6Prefix: {}
          },
          destPort: 7,
          dscp: {
            uint8: 5
          },
          ethernetType: 'string',
          inPort: 10,
          ipProtocolType: 10,
          outerVlanId: 1,
          sourceIpAddress: {
            ipv4Prefix: {},
            ipv6Prefix: {}
          },
          sourcePort: 7,
          vlanId: 10
        },
        priority: 1,
        tableId: 9
      }
    };
    describe('#getFlowsearch - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFlowsearch(sdnOpenflowGetFlowsearchBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.result);
                assert.equal(true, data.response.setOrExpired);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnOpenflow', 'getFlowsearch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnOpenflowGetFlowsearchByIdBodyParam = {
      data: {
        flowIds: [
          'string'
        ]
      }
    };
    describe('#getFlowsearchById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFlowsearchById(sdnOpenflowGetFlowsearchByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnOpenflow', 'getFlowsearchById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnOpenflowPatchFlowsBodyParam = {
      data: [
        {
          flowIds: [
            'string'
          ],
          openflowInstruction: {
            actions: [
              {
                actionType: 'EXPERIMENTER',
                experimenterActionType: 'REDIRECT_TO_SVCT',
                ipAddress: {},
                isIndirect: true,
                outerVlanId: 6,
                portId: 8,
                sdpId: 10,
                vcId: 7,
                vlanId: 3
              }
            ],
            instructionType: 'CLEAR_ACTIONS'
          }
        }
      ]
    };
    describe('#patchFlows - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchFlows(sdnOpenflowPatchFlowsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnOpenflow', 'patchFlows', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnOpenflowDatapathId = 'fakedata';
    describe('#getPorts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPorts(sdnOpenflowDatapathId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnOpenflow', 'getPorts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSwitches - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSwitches((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnOpenflow', 'getSwitches', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnOpenflowNeId = 'fakedata';
    describe('#getSwitchesInRouter - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSwitchesInRouter(sdnOpenflowNeId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnOpenflow', 'getSwitchesInRouter', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTables - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTables(sdnOpenflowDatapathId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnOpenflow', 'getTables', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnPhysicallinksDestId = 'fakedata';
    const sdnPhysicallinksSrcId = 'fakedata';
    describe('#create - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.create(sdnPhysicallinksDestId, sdnPhysicallinksSrcId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnPhysicallinks', 'create', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4Physicallinks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4Physicallinks((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnPhysicallinks', 'getV4Physicallinks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnPhysicallinksUuid = 'fakedata';
    describe('#getV4PhysicallinksUuid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4PhysicallinksUuid(sdnPhysicallinksUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnPhysicallinks', 'getV4PhysicallinksUuid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnPolicyCreateIpOpticalCorrelationPolicyBodyParam = {
      data: {
        appId: 'string',
        id: 'string',
        latencyUploadPolicy: 'auto',
        name: 'string',
        objectDescription: 'string',
        srlgUploadPolicy: 'auto'
      }
    };
    describe('#createIpOpticalCorrelationPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createIpOpticalCorrelationPolicy(sdnPolicyCreateIpOpticalCorrelationPolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnPolicy', 'createIpOpticalCorrelationPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnPolicyCreateRdRtRangesBodyParam = {
      data: {
        appId: 'string',
        id: 'string',
        maxAssignedForRd: 7,
        maxAssignedForRt: 6,
        maxAssignedForRtExtranet: 4,
        minAssignedForRd: 10,
        minAssignedForRt: 2,
        minAssignedForRtExtranet: 6,
        name: 'string',
        networkASForRd: true,
        networkASForRt: true,
        networkASForRtExtranet: false,
        objectDescription: 'string',
        policyType: 'RD_AND_RT',
        rdAS: 4,
        rdType: 5,
        rtAS: 5,
        rtExtranetAS: 5,
        rtExtranetType: 10,
        rtType: 10
      }
    };
    describe('#createRdRtRanges - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createRdRtRanges(sdnPolicyCreateRdRtRangesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnPolicy', 'createRdRtRanges', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnPolicyCreateRouterPortProtectionGroupPolicyBodyParam = {
      data: {
        appId: 'string',
        id: 'string',
        name: 'string',
        objectDescription: 'string',
        reversionPolicy: {
          reversionMode: 'revertive',
          waitToRevertTime: {
            time: {}
          }
        },
        switchPolicy: {
          waitToSwitchTime: {
            time: {}
          }
        }
      }
    };
    describe('#createRouterPortProtectionGroupPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createRouterPortProtectionGroupPolicy(sdnPolicyCreateRouterPortProtectionGroupPolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnPolicy', 'createRouterPortProtectionGroupPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnPolicyCreateSteeringParameterBodyParam = {
      data: {
        name: {
          string: 'string'
        }
      }
    };
    describe('#createSteeringParameter - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createSteeringParameter(sdnPolicyCreateSteeringParameterBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnPolicy', 'createSteeringParameter', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnPolicyCreateTunnelSelectionsBodyParam = {
      data: {
        appId: 'string',
        bgpPriority: 1,
        canBookBwInCoreForEline: false,
        canCreateNewTunnel: false,
        canRebindTunnel: false,
        canResizeExistingTunnel: false,
        canUseExistingTunnel: false,
        consumable: true,
        deletable: true,
        erpPriority: 2,
        frrEnabled: true,
        grePriority: 2,
        id: 'string',
        igpConnectivityRequiredDuringPathSelection: false,
        ldpPriority: 8,
        looseRsvpPriority: 6,
        looseSrTePriority: 9,
        modifiable: false,
        name: 'string',
        objectDescription: 'string',
        oduPriority: 2,
        pccInitiatedLspEnabled: false,
        pccInitiatedRsvpPriority: 7,
        pccInitiatedSrTePriority: 2,
        protection: false,
        protectionType: 'SECONDARY',
        shouldAvoidOperStateDown: true,
        srIsisPriority: 4,
        srOspfPriority: 3,
        steeringParametersExcluded: [
          {
            name: {
              string: 'string'
            }
          }
        ],
        steeringParametersIncluded: [
          {
            name: {
              string: 'string'
            }
          }
        ],
        strictRsvpPriority: 4,
        strictSrTePriority: 4,
        targetIpAddressInfoList: [
          {
            associatedTunnelType: 'sr_isis',
            primaryAddress: {
              ipv4Prefix: {},
              ipv6Prefix: {}
            }
          }
        ]
      }
    };
    describe('#createTunnelSelections - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createTunnelSelections(sdnPolicyCreateTunnelSelectionsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnPolicy', 'createTunnelSelections', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllIpOpticalCorrelationPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllIpOpticalCorrelationPolicy((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnPolicy', 'getAllIpOpticalCorrelationPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnPolicyPolicyId = 'fakedata';
    const sdnPolicyUpdateIpOpticalCorrelationPolicyBodyParam = {
      data: {
        appId: 'string',
        id: 'string',
        latencyUploadPolicy: 'manual',
        name: 'string',
        objectDescription: 'string',
        srlgUploadPolicy: 'auto'
      }
    };
    describe('#updateIpOpticalCorrelationPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateIpOpticalCorrelationPolicy(sdnPolicyUpdateIpOpticalCorrelationPolicyBodyParam, sdnPolicyPolicyId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnPolicy', 'updateIpOpticalCorrelationPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpOpticalCorrelationPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpOpticalCorrelationPolicy(sdnPolicyPolicyId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnPolicy', 'getIpOpticalCorrelationPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllRdRtRanges - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllRdRtRanges((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnPolicy', 'getAllRdRtRanges', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnPolicyUpdateRdRtRangesBodyParam = {
      data: {
        appId: 'string',
        id: 'string',
        maxAssignedForRd: 1,
        maxAssignedForRt: 4,
        maxAssignedForRtExtranet: 3,
        minAssignedForRd: 7,
        minAssignedForRt: 4,
        minAssignedForRtExtranet: 4,
        name: 'string',
        networkASForRd: true,
        networkASForRt: true,
        networkASForRtExtranet: true,
        objectDescription: 'string',
        policyType: 'RT_ONLY',
        rdAS: 3,
        rdType: 2,
        rtAS: 9,
        rtExtranetAS: 1,
        rtExtranetType: 3,
        rtType: 5
      }
    };
    describe('#updateRdRtRanges - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateRdRtRanges(sdnPolicyUpdateRdRtRangesBodyParam, sdnPolicyPolicyId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnPolicy', 'updateRdRtRanges', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllRouterPortProtectionGroupPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllRouterPortProtectionGroupPolicy((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnPolicy', 'getAllRouterPortProtectionGroupPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnPolicyUpdateRouterPortProtectionGroupPolicyBodyParam = {
      data: {
        appId: 'string',
        id: 'string',
        name: 'string',
        objectDescription: 'string',
        reversionPolicy: {
          reversionMode: 'revertive',
          waitToRevertTime: {
            time: {}
          }
        },
        switchPolicy: {
          waitToSwitchTime: {
            time: {}
          }
        }
      }
    };
    describe('#updateRouterPortProtectionGroupPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateRouterPortProtectionGroupPolicy(sdnPolicyPolicyId, sdnPolicyUpdateRouterPortProtectionGroupPolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnPolicy', 'updateRouterPortProtectionGroupPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRouterPortProtectionGroupPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRouterPortProtectionGroupPolicy(sdnPolicyPolicyId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnPolicy', 'getRouterPortProtectionGroupPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSteeringParameters - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSteeringParameters((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnPolicy', 'getSteeringParameters', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllTunnelSelections - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllTunnelSelections((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnPolicy', 'getAllTunnelSelections', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnPolicyUpdateTunnelSelectionsBodyParam = {
      data: {
        appId: 'string',
        bgpPriority: 6,
        canBookBwInCoreForEline: false,
        canCreateNewTunnel: false,
        canRebindTunnel: false,
        canResizeExistingTunnel: true,
        canUseExistingTunnel: false,
        consumable: true,
        deletable: false,
        erpPriority: 8,
        frrEnabled: false,
        grePriority: 7,
        id: 'string',
        igpConnectivityRequiredDuringPathSelection: true,
        ldpPriority: 7,
        looseRsvpPriority: 7,
        looseSrTePriority: 8,
        modifiable: false,
        name: 'string',
        objectDescription: 'string',
        oduPriority: 8,
        pccInitiatedLspEnabled: false,
        pccInitiatedRsvpPriority: 8,
        pccInitiatedSrTePriority: 2,
        protection: true,
        protectionType: 'PRIMARY',
        shouldAvoidOperStateDown: false,
        srIsisPriority: 8,
        srOspfPriority: 3,
        steeringParametersExcluded: [
          {
            name: {
              string: 'string'
            }
          }
        ],
        steeringParametersIncluded: [
          {
            name: {
              string: 'string'
            }
          }
        ],
        strictRsvpPriority: 1,
        strictSrTePriority: 9,
        targetIpAddressInfoList: [
          {
            associatedTunnelType: 'rsvp_loose',
            primaryAddress: {
              ipv4Prefix: {},
              ipv6Prefix: {}
            }
          }
        ]
      }
    };
    describe('#updateTunnelSelections - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateTunnelSelections(sdnPolicyPolicyId, sdnPolicyUpdateTunnelSelectionsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnPolicy', 'updateTunnelSelections', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTunnelSelections - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTunnelSelections(sdnPolicyPolicyId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnPolicy', 'getTunnelSelections', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4Ports - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4Ports((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnPorts', 'getV4Ports', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnPortsNeId = 'fakedata';
    const sdnPortsTenantUuid = 'fakedata';
    describe('#getNeAllByTenant - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNeAllByTenant(sdnPortsNeId, sdnPortsTenantUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnPorts', 'getNeAllByTenant', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNeAll - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNeAll(sdnPortsNeId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnPorts', 'getNeAll', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnPortsServiceType = 'fakedata';
    describe('#getNeByTenant - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNeByTenant(sdnPortsNeId, sdnPortsServiceType, sdnPortsTenantUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnPorts', 'getNeByTenant', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnPortsNeUuid = 'fakedata';
    describe('#getNe - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNe(sdnPortsNeUuid, sdnPortsServiceType, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnPorts', 'getNe', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnPortsPortUuid = 'fakedata';
    describe('#getServiceCountOnPort - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getServiceCountOnPort(sdnPortsPortUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnPorts', 'getServiceCountOnPort', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServicesOnPort - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getServicesOnPort(sdnPortsPortUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnPorts', 'getServicesOnPort', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServicetype - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getServicetype(sdnPortsServiceType, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnPorts', 'getServicetype', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServicetypeByTenant - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getServicetypeByTenant(sdnPortsServiceType, sdnPortsTenantUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnPorts', 'getServicetypeByTenant', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTenant - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTenant(sdnPortsTenantUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnPorts', 'getTenant', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnPortsId = 'fakedata';
    const sdnPortsPutV4PortsIdBodyParam = {
      data: {
        protectionType: 'protection'
      }
    };
    describe('#putV4PortsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putV4PortsId(sdnPortsId, sdnPortsPutV4PortsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnPorts', 'putV4PortsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4PortsPortUuid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4PortsPortUuid(sdnPortsPortUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnPorts', 'getV4PortsPortUuid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthentication - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAuthentication((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnSecurity', 'getAuthentication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServiceDebugServiceUuid = 'fakedata';
    describe('#createConstraintTest - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createConstraintTest(sdnServiceDebugServiceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServiceDebug', 'createConstraintTest', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDciRecompute - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createDciRecompute(sdnServiceDebugServiceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServiceDebug', 'createDciRecompute', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#isMaster - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.isMaster((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnSystem', 'isMaster', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnSystemPluginKey = 'fakedata';
    const sdnSystemPluginName = 'fakedata';
    const sdnSystemPluginVId = 555;
    describe('#setPluginConnect - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.setPluginConnect(sdnSystemPluginKey, sdnSystemPluginName, sdnSystemPluginVId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnSystem', 'setPluginConnect', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#resyncNms - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.resyncNms((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnSystem', 'resyncNms', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnSystemUuid = 'fakedata';
    describe('#resyncObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.resyncObject(sdnSystemUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnSystem', 'resyncObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getState - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getState((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnSystem', 'getState', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVersion - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVersion((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnSystem', 'getVersion', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnTemplateCreateClineServicesBodyParam = {
      data: {
        adminState: 'MAINTENANCE',
        appId: 'string',
        bidirectional: 'NO',
        cemUseRtpHeader: false,
        customerId: 3,
        id: 'string',
        maxCost: 6,
        maxHops: 2,
        maxLatency: 5,
        mediationProfileId: 'string',
        meta: [
          'string'
        ],
        monitorBandwidth: true,
        mtu: 10,
        name: 'string',
        objectDescription: 'string',
        objective: 'DISTANCE',
        portFilter: [
          {
            condition1: {
              operator: 'string',
              value: 'string'
            },
            condition2: {
              operator: 'string',
              value: 'string'
            },
            connector: 'string',
            groupNum: 6,
            name: 'string',
            operator: 'string'
          }
        ],
        qosId: 'string',
        qosName: 'string',
        tunnelSelectionId: 'string',
        vcType: 'SATOP_T1',
        workflowProfileId: 'string',
        workflowValidate: 'string'
      }
    };
    describe('#createClineServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createClineServices(sdnTemplateCreateClineServicesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'createClineServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnTemplateCreateCustomAttributesBodyParam = {
      data: {
        appId: 'string',
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        id: 'string',
        name: 'string',
        objectDescription: 'string'
      }
    };
    describe('#createCustomAttributes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createCustomAttributes(sdnTemplateCreateCustomAttributesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'createCustomAttributes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnTemplateCreateElanServicesBodyParam = {
      data: {
        adminState: 'DOWN',
        appId: 'string',
        autoBindType: 'sr_ospf',
        bidirectional: 'SYMMETRIC_STRICT',
        customerId: 6,
        extensionTemplate: {
          allowL2ExtensionConfiguration: true,
          enableL2ExtensionAugmentation: true,
          nni: {
            autosetTagsToMatchServiceEndpoint: true,
            innerTag: 3,
            outerTag: 1
          },
          uni: {
            innerTag: 9,
            outerTag: 8
          }
        },
        id: 'string',
        innerTag: 9,
        maxCost: 1,
        maxHops: 6,
        maxLatency: 9,
        mediationProfileId: 'string',
        meta: [
          'string'
        ],
        monitorBandwidth: false,
        mtu: 2,
        name: 'string',
        objectDescription: 'string',
        objective: 'COST',
        outerTag: 6,
        portFilter: [
          {
            condition1: {
              operator: 'string',
              value: 'string'
            },
            condition2: {
              operator: 'string',
              value: 'string'
            },
            connector: 'string',
            groupNum: 1,
            name: 'string',
            operator: 'string'
          }
        ],
        qosId: 'string',
        qosName: 'string',
        relaxEndpointValidation: false,
        tunnelSelectionId: 'string',
        vcType: 'CESOPSN',
        workflowProfileId: 'string',
        workflowValidate: 'string'
      }
    };
    describe('#createElanServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createElanServices(sdnTemplateCreateElanServicesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'createElanServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnTemplateCreateElineServicesBodyParam = {
      data: {
        adminState: 'DOWN',
        appId: 'string',
        autoBindType: 'none',
        bidirectional: 'ANY_REVERSE_ROUTE',
        customerId: 4,
        extensionTemplate: {
          allowL2ExtensionConfiguration: false,
          enableL2ExtensionAugmentation: true,
          nni: {
            autosetTagsToMatchServiceEndpoint: false,
            innerTag: 3,
            outerTag: 8
          },
          uni: {
            innerTag: 6,
            outerTag: 3
          }
        },
        id: 'string',
        innerTag: 8,
        maxCost: 5,
        maxHops: 2,
        maxLatency: 10,
        mediationProfileId: 'string',
        meta: [
          'string'
        ],
        monitorBandwidth: true,
        mtu: 2,
        name: 'string',
        objectDescription: 'string',
        objective: 'DISTANCE',
        outerTag: 4,
        portFilter: [
          {
            condition1: {
              operator: 'string',
              value: 'string'
            },
            condition2: {
              operator: 'string',
              value: 'string'
            },
            connector: 'string',
            groupNum: 8,
            name: 'string',
            operator: 'string'
          }
        ],
        qosId: 'string',
        qosName: 'string',
        relaxEndpointValidation: true,
        tunnelSelectionId: 'string',
        vcType: 'SATOP_E3',
        workflowProfileId: 'string',
        workflowValidate: 'string'
      }
    };
    describe('#createElineServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createElineServices(sdnTemplateCreateElineServicesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'createElineServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnTemplateCreateGenericQosBodyParam = {
      data: {
        appId: 'string',
        egressCosMappingTable: [
          {
            cos: 'string',
            queueId: 2
          }
        ],
        egressParam: {
          tier1Scheduler: {
            scheduler: {}
          }
        },
        egressPolicyList: [
          'string'
        ],
        egressQueueConfigurationTable: [
          {
            cbs: 5,
            cir: 9,
            id: 7,
            mbs: 4,
            pir: 8,
            queueType: 'Policer',
            rateType: 'percent'
          }
        ],
        externalIds: [
          {
            context: 'MDM',
            id: 'string',
            location: [
              'string'
            ]
          }
        ],
        id: 'string',
        ingressCosMappingTable: [
          {
            cos: 'string',
            queueId: 10
          }
        ],
        ingressParam: {
          tier1Scheduler: {
            scheduler: {}
          }
        },
        ingressPolicyList: [
          'string'
        ],
        ingressQueueConfigurationTable: [
          {
            cbs: 1,
            cir: 9,
            id: 6,
            mbs: 6,
            pir: 3,
            queueType: 'Queue',
            rateType: 'kbps'
          }
        ],
        name: 'string',
        objectDescription: 'string'
      }
    };
    describe('#createGenericQos - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createGenericQos(sdnTemplateCreateGenericQosBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'createGenericQos', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnTemplateCreateIesServicesBodyParam = {
      data: {
        adminState: 'DOWN',
        appId: 'string',
        bidirectional: 'NO',
        customerId: 10,
        id: 'string',
        innerTag: 2,
        maxCost: 2,
        maxHops: 1,
        maxLatency: 1,
        mediationProfileId: 'string',
        meta: [
          'string'
        ],
        mtu: 4,
        name: 'string',
        objectDescription: 'string',
        objective: 'TE_METRIC',
        outerTag: 10,
        portFilter: [
          {
            condition1: {
              operator: 'string',
              value: 'string'
            },
            condition2: {
              operator: 'string',
              value: 'string'
            },
            connector: 'string',
            groupNum: 7,
            name: 'string',
            operator: 'string'
          }
        ],
        qosId: 'string',
        qosName: 'string',
        relaxEndpointValidation: true,
        workflowProfileId: 'string',
        workflowValidate: 'string'
      }
    };
    describe('#createIesServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createIesServices(sdnTemplateCreateIesServicesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'createIesServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnTemplateCreateL2backhaulServicesBodyParam = {
      data: {
        adminState: 'UP',
        appId: 'string',
        bidirectional: 'SYMMETRIC_LOOSE',
        customerId: 7,
        id: 'string',
        innerTag: 10,
        maxCost: 7,
        maxHops: 8,
        maxLatency: 8,
        meta: [
          'string'
        ],
        monitorBandwidth: true,
        mtu: 6,
        name: 'string',
        objectDescription: 'string',
        objective: 'TE_METRIC',
        outerTag: 2,
        portFilter: [
          {
            condition1: {
              operator: 'string',
              value: 'string'
            },
            condition2: {
              operator: 'string',
              value: 'string'
            },
            connector: 'string',
            groupNum: 1,
            name: 'string',
            operator: 'string'
          }
        ],
        qosId: 'string',
        qosName: 'string',
        tunnelSelectionId: 'string',
        vcType: 'ETHERNET_TAGGED_MODE',
        workflowProfileId: 'string',
        workflowValidate: 'string'
      }
    };
    describe('#createL2backhaulServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createL2backhaulServices(sdnTemplateCreateL2backhaulServicesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'createL2backhaulServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnTemplateCreateL3DciVpnServicesBodyParam = {
      data: {
        adminState: 'MAINTENANCE',
        appId: 'string',
        autobind: 'ldp',
        backhaulMtu: 3,
        bidirectional: 'NO',
        connectivity: 'VXLAN_STITCHED',
        customerId: 1,
        dualHomed: false,
        ecmp: 10,
        id: 'string',
        innerTag: 2,
        maxCost: 1,
        maxHops: 1,
        maxLatency: 9,
        mediationProfileId: 'string',
        meta: [
          'string'
        ],
        mtu: 4,
        name: 'string',
        objectDescription: 'string',
        objective: 'STAR_WEIGHT',
        outerTag: 3,
        portFilter: [
          {
            condition1: {
              operator: 'string',
              value: 'string'
            },
            condition2: {
              operator: 'string',
              value: 'string'
            },
            connector: 'string',
            groupNum: 7,
            name: 'string',
            operator: 'string'
          }
        ],
        qosId: 'string',
        qosName: 'string',
        rdRtRangePolicyId: 'string',
        relaxEndpointValidation: false,
        transportSliceName: 'string',
        tunnelSelectionId: 'string',
        vrfPerEndpoint: false,
        workflowProfileId: 'string',
        workflowValidate: 'string'
      }
    };
    describe('#createL3DciVpnServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createL3DciVpnServices(sdnTemplateCreateL3DciVpnServicesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'createL3DciVpnServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnTemplateCreateL3VpnServicesBodyParam = {
      data: {
        adminState: 'DOWN',
        appId: 'string',
        autobind: 'bgp',
        bidirectional: 'ASYMMETRIC_LOOSE',
        customerId: 7,
        id: 'string',
        innerTag: 3,
        maxCost: 8,
        maxHops: 6,
        maxLatency: 6,
        mediationProfileId: 'string',
        meta: [
          'string'
        ],
        mtu: 5,
        name: 'string',
        objectDescription: 'string',
        objective: 'STAR_WEIGHT',
        outerTag: 7,
        portFilter: [
          {
            condition1: {
              operator: 'string',
              value: 'string'
            },
            condition2: {
              operator: 'string',
              value: 'string'
            },
            connector: 'string',
            groupNum: 9,
            name: 'string',
            operator: 'string'
          }
        ],
        qosId: 'string',
        qosName: 'string',
        rdRtRangePolicyId: 'string',
        relaxEndpointValidation: true,
        transportSliceName: 'string',
        tunnelSelectionId: 'string',
        vrfPerEndpoint: true,
        workflowProfileId: 'string',
        workflowValidate: 'string'
      }
    };
    describe('#createL3VpnServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createL3VpnServices(sdnTemplateCreateL3VpnServicesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'createL3VpnServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnTemplateCreateLagServicesBodyParam = {
      data: {
        appId: 'string',
        bandwidth: 8,
        bidirectional: 'SYMMETRIC_STRICT',
        customerId: 2,
        id: 'string',
        maxCost: 4,
        maxHops: 4,
        maxLatency: 4,
        monitorBandwidth: false,
        name: 'string',
        objectDescription: 'string',
        objective: 'LATENCY',
        reverseBandwidth: 4
      }
    };
    describe('#createLagServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createLagServices(sdnTemplateCreateLagServicesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'createLagServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnTemplateCreateMediationProfileMappingBodyParam = {
      data: {
        appId: 'string',
        id: 'string',
        mediationInfos: [
          {
            mediationName: 'string',
            mediationVersion: 'string',
            templateGlobalName: 'string'
          }
        ],
        name: 'string',
        objectDescription: 'string',
        profileType: 'L3VPN'
      }
    };
    describe('#createMediationProfileMapping - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createMediationProfileMapping(sdnTemplateCreateMediationProfileMappingBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'createMediationProfileMapping', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnTemplateCreateOchServicesBodyParam = {
      data: {
        appId: 'string',
        bidirectional: 'NO',
        customerId: 8,
        id: 'string',
        maxCost: 6,
        maxHops: 7,
        maxLatency: 3,
        name: 'string',
        objectDescription: 'string',
        objective: 'STAR_WEIGHT',
        restoration: 'SBR',
        reversionMode: 'softauto'
      }
    };
    describe('#createOchServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createOchServices(sdnTemplateCreateOchServicesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'createOchServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnTemplateCreateOduServicesBodyParam = {
      data: {
        appId: 'string',
        bandwidth: 8,
        bidirectional: 'SYMMETRIC_STRICT',
        customerId: 8,
        id: 'string',
        maxCost: 8,
        maxHops: 3,
        maxLatency: 3,
        name: 'string',
        objectDescription: 'string',
        objective: 'TE_METRIC',
        reverseBandwidth: 3
      }
    };
    describe('#createOduServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createOduServices(sdnTemplateCreateOduServicesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'createOduServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnTemplateCreateOpticalConnectivityConstraintBodyParam = {
      data: {
        appId: 'string',
        connectivityConstraint: {
          capacity: {
            capacityValue: {}
          },
          connectivityDirection: 'UNDEFINED_OR_UNKNOWN',
          schedule: {
            timeRange: {}
          },
          serviceLevel: 'string',
          serviceType: 'MULTIPOINT_CONNECTIVITY'
        },
        id: 'string',
        name: 'string',
        objectDescription: 'string'
      }
    };
    describe('#createOpticalConnectivityConstraint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createOpticalConnectivityConstraint(sdnTemplateCreateOpticalConnectivityConstraintBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'createOpticalConnectivityConstraint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnTemplateCreateOpticalConnectivityServiceBodyParam = {
      data: {
        appId: 'string',
        connectivityConstraintTemplateId: 'string',
        id: 'string',
        name: 'string',
        objectDescription: 'string',
        resilienceConstraintTemplateIds: [
          'string'
        ],
        routingConstraintTemplateId: 'string'
      }
    };
    describe('#createOpticalConnectivityService - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createOpticalConnectivityService(sdnTemplateCreateOpticalConnectivityServiceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'createOpticalConnectivityService', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnTemplateCreateOpticalResilienceConstraintBodyParam = {
      data: {
        appId: 'string',
        id: 'string',
        name: 'string',
        objectDescription: 'string',
        resilienceConstraint: {
          holdOffTime: 10,
          isCoordinatedSwitchingBothEnds: false,
          isFrozen: false,
          isLockOut: false,
          maxSwitchTimes: 8,
          resilienceType: {
            protectionType: 'ONE_PLUS_ONE_PROTECTION',
            restorationPolicy: 'NA'
          },
          restorationCoordinateType: 'HOLD_OFF_TIME',
          restorePriority: 4,
          reversionMode: 'NON_REVERTIVE',
          waitToRevertTime: 7
        }
      }
    };
    describe('#createOpticalResilienceConstraint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createOpticalResilienceConstraint(sdnTemplateCreateOpticalResilienceConstraintBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'createOpticalResilienceConstraint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnTemplateCreateOpticalRoutingConstraintBodyParam = {
      data: {
        appId: 'string',
        id: 'string',
        name: 'string',
        objectDescription: 'string',
        routingConstraint: {
          costCharacteristic: [
            {
              costAlgorithm: 'string',
              costName: 'string',
              costValue: 'string'
            }
          ],
          diversityPolicy: 'NODE',
          isExclusive: false,
          latencyCharacteristic: [
            {
              fixedLatencyCharacteristic: 'string',
              jitterCharacteristic: 'string',
              queingLatencyCharacteristic: 'string',
              trafficPropertyName: 'string',
              wanderCharacteristic: 'string'
            }
          ],
          riskDiversityCharacteristic: [
            {
              riskCharacteristicName: 'string',
              riskIdentifierList: [
                'string'
              ]
            }
          ],
          routeDirection: 'BIDIRECTIONAL',
          routeObjectiveFunction: 'MIN_SUM_OF_WORK_AND_PROTECTION_ROUTE_COST'
        }
      }
    };
    describe('#createOpticalRoutingConstraint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createOpticalRoutingConstraint(sdnTemplateCreateOpticalRoutingConstraintBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'createOpticalRoutingConstraint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnTemplateCreatePathProfilesBodyParam = {
      data: {
        appId: 'string',
        bandwidthStrategy: 'STANDARD',
        bidirection: 'ASYMMETRIC_LOOSE',
        bwSplitMonitoringTime: 7,
        bwSplitSupported: true,
        bwSplitThreshold: 2,
        bwSplitWorkflowName: 'string',
        controlRerouteStrategy: 'STANDARD',
        disjoint: 'LINK_STRICT_AND_SRLG',
        excludeRouteObjects: [
          {
            ipv4Address: {},
            ipv6Address: {}
          }
        ],
        explicitRouteStrategy: 'STANDARD',
        explicitRouteStrategyEcmpPreference: 'NODE_SID',
        id: 'string',
        includeRouteObjects: {},
        latencyThreshold: 1,
        maxCost: 3,
        maxHops: 1,
        maxLatency: 7,
        maxTeMetric: 3,
        name: 'string',
        objectDescription: 'string',
        objective: 'DISTANCE',
        profileId: 5,
        sidProtectionStrategy: 'UNPROTECTED_PREFERRED'
      }
    };
    describe('#createPathProfiles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createPathProfiles(sdnTemplateCreatePathProfilesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'createPathProfiles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnTemplateCreateQosBodyParam = {
      data: {
        appId: 'string',
        egressParam: {
          tier1Scheduler: {
            scheduler: {}
          }
        },
        egressQueueOverride: [
          {
            cbs: 4,
            cir: 8,
            id: 10,
            mbs: 5,
            pir: 8,
            queueType: 'Policer',
            rateType: 'kbps'
          }
        ],
        id: 'string',
        ingressParam: {
          tier1Scheduler: {
            scheduler: {}
          }
        },
        ingressQueueOverride: [
          {
            cbs: 1,
            cir: 4,
            id: 6,
            mbs: 8,
            pir: 7,
            queueType: 'Policer',
            rateType: 'percent'
          }
        ],
        name: 'string',
        objectDescription: 'string',
        qosProfile: 7
      }
    };
    describe('#createQos - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createQos(sdnTemplateCreateQosBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'createQos', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnTemplateCreateRouterIdSystemIdMappingBodyParam = {
      data: {
        appId: 'string',
        id: 'string',
        name: 'string',
        objectDescription: 'string',
        pccAddress: {
          ipv4Address: {},
          ipv6Address: {}
        },
        routerInfos: [
          {
            asNumber: {
              uint32: 4
            },
            bgpLsId: 8,
            dcIdentifier: 4,
            networkIdentifier: {
              uint32: 5
            },
            protocol: 'ANY',
            routerId: {
              string: 'string'
            }
          }
        ],
        systemId: {
          ipv4Address: {},
          ipv6Address: {}
        },
        systemName: 'string'
      }
    };
    describe('#createRouterIdSystemIdMapping - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createRouterIdSystemIdMapping(sdnTemplateCreateRouterIdSystemIdMappingBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'createRouterIdSystemIdMapping', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnTemplateCreateWorkflowProfileBodyParam = {
      data: {
        appId: 'string',
        id: 'string',
        name: 'string',
        objectDescription: 'string',
        workflowProfileInfo: [
          {
            blockServiceDeployment: true,
            continueServiceDeploymentOnError: true,
            serviceStep: 'postupdate_success',
            workflowExecutionTimeout: 6,
            workflowId: 'string',
            workflowInputs: 'string',
            workflowParams: 'string'
          }
        ]
      }
    };
    describe('#createWorkflowProfile - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createWorkflowProfile(sdnTemplateCreateWorkflowProfileBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'createWorkflowProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllClineServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllClineServices((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'getAllClineServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnTemplateTemplateId = 'fakedata';
    const sdnTemplateUpdateClineServicesBodyParam = {
      data: {
        adminState: 'MAINTENANCE',
        appId: 'string',
        bidirectional: 'ANY_REVERSE_ROUTE',
        cemUseRtpHeader: false,
        customerId: 4,
        id: 'string',
        maxCost: 7,
        maxHops: 2,
        maxLatency: 2,
        mediationProfileId: 'string',
        meta: [
          'string'
        ],
        monitorBandwidth: false,
        mtu: 5,
        name: 'string',
        objectDescription: 'string',
        objective: 'TE_METRIC',
        portFilter: [
          {
            condition1: {
              operator: 'string',
              value: 'string'
            },
            condition2: {
              operator: 'string',
              value: 'string'
            },
            connector: 'string',
            groupNum: 4,
            name: 'string',
            operator: 'string'
          }
        ],
        qosId: 'string',
        qosName: 'string',
        tunnelSelectionId: 'string',
        vcType: 'SATOP_E1',
        workflowProfileId: 'string',
        workflowValidate: 'string'
      }
    };
    describe('#updateClineServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateClineServices(sdnTemplateUpdateClineServicesBodyParam, sdnTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'updateClineServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getClineServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getClineServices(sdnTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'getClineServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllCustomAttributes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllCustomAttributes((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'getAllCustomAttributes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnTemplateUpdateCustomAttributesBodyParam = {
      data: {
        appId: 'string',
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        id: 'string',
        name: 'string',
        objectDescription: 'string'
      }
    };
    describe('#updateCustomAttributes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateCustomAttributes(sdnTemplateUpdateCustomAttributesBodyParam, sdnTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'updateCustomAttributes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCustomAttributes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCustomAttributes(sdnTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'getCustomAttributes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllElanServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllElanServices((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'getAllElanServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnTemplateUpdateElanServicesBodyParam = {
      data: {
        adminState: 'DOWN',
        appId: 'string',
        autoBindType: 'gre',
        bidirectional: 'SYMMETRIC_STRICT',
        customerId: 2,
        extensionTemplate: {
          allowL2ExtensionConfiguration: false,
          enableL2ExtensionAugmentation: true,
          nni: {
            autosetTagsToMatchServiceEndpoint: false,
            innerTag: 5,
            outerTag: 7
          },
          uni: {
            innerTag: 10,
            outerTag: 10
          }
        },
        id: 'string',
        innerTag: 10,
        maxCost: 10,
        maxHops: 6,
        maxLatency: 7,
        mediationProfileId: 'string',
        meta: [
          'string'
        ],
        monitorBandwidth: false,
        mtu: 5,
        name: 'string',
        objectDescription: 'string',
        objective: 'TE_METRIC',
        outerTag: 4,
        portFilter: [
          {
            condition1: {
              operator: 'string',
              value: 'string'
            },
            condition2: {
              operator: 'string',
              value: 'string'
            },
            connector: 'string',
            groupNum: 2,
            name: 'string',
            operator: 'string'
          }
        ],
        qosId: 'string',
        qosName: 'string',
        relaxEndpointValidation: true,
        tunnelSelectionId: 'string',
        vcType: 'CESOPSN',
        workflowProfileId: 'string',
        workflowValidate: 'string'
      }
    };
    describe('#updateElanServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateElanServices(sdnTemplateUpdateElanServicesBodyParam, sdnTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'updateElanServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getElanServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getElanServices(sdnTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'getElanServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllElineServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllElineServices((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'getAllElineServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnTemplateUpdateElineServicesBodyParam = {
      data: {
        adminState: 'UP',
        appId: 'string',
        autoBindType: 'sr_te',
        bidirectional: 'ASYMMETRIC_LOOSE',
        customerId: 8,
        extensionTemplate: {
          allowL2ExtensionConfiguration: true,
          enableL2ExtensionAugmentation: true,
          nni: {
            autosetTagsToMatchServiceEndpoint: true,
            innerTag: 2,
            outerTag: 6
          },
          uni: {
            innerTag: 7,
            outerTag: 7
          }
        },
        id: 'string',
        innerTag: 4,
        maxCost: 4,
        maxHops: 1,
        maxLatency: 7,
        mediationProfileId: 'string',
        meta: [
          'string'
        ],
        monitorBandwidth: false,
        mtu: 4,
        name: 'string',
        objectDescription: 'string',
        objective: 'TE_METRIC',
        outerTag: 2,
        portFilter: [
          {
            condition1: {
              operator: 'string',
              value: 'string'
            },
            condition2: {
              operator: 'string',
              value: 'string'
            },
            connector: 'string',
            groupNum: 8,
            name: 'string',
            operator: 'string'
          }
        ],
        qosId: 'string',
        qosName: 'string',
        relaxEndpointValidation: false,
        tunnelSelectionId: 'string',
        vcType: 'SATOP_T3',
        workflowProfileId: 'string',
        workflowValidate: 'string'
      }
    };
    describe('#updateElineServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateElineServices(sdnTemplateUpdateElineServicesBodyParam, sdnTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'updateElineServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getElineServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getElineServices(sdnTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'getElineServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllGenericQos - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllGenericQos((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'getAllGenericQos', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnTemplateNeId = 'fakedata';
    describe('#getGenericQosProfiles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getGenericQosProfiles(sdnTemplateNeId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'getGenericQosProfiles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnTemplateGqpId = 'fakedata';
    const sdnTemplateUpdateGenericQosBodyParam = {
      data: {
        appId: 'string',
        egressCosMappingTable: [
          {
            cos: 'string',
            queueId: 2
          }
        ],
        egressParam: {
          tier1Scheduler: {
            scheduler: {}
          }
        },
        egressPolicyList: [
          'string'
        ],
        egressQueueConfigurationTable: [
          {
            cbs: 4,
            cir: 6,
            id: 3,
            mbs: 8,
            pir: 3,
            queueType: 'Queue',
            rateType: 'kbps'
          }
        ],
        externalIds: [
          {
            context: 'NFM_P',
            id: 'string',
            location: [
              'string'
            ]
          }
        ],
        id: 'string',
        ingressCosMappingTable: [
          {
            cos: 'string',
            queueId: 10
          }
        ],
        ingressParam: {
          tier1Scheduler: {
            scheduler: {}
          }
        },
        ingressPolicyList: [
          'string'
        ],
        ingressQueueConfigurationTable: [
          {
            cbs: 5,
            cir: 5,
            id: 7,
            mbs: 1,
            pir: 10,
            queueType: 'Policer',
            rateType: 'kbps'
          }
        ],
        name: 'string',
        objectDescription: 'string'
      }
    };
    describe('#updateGenericQos - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateGenericQos(sdnTemplateGqpId, sdnTemplateUpdateGenericQosBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'updateGenericQos', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGenericQos - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getGenericQos(sdnTemplateGqpId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'getGenericQos', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllIesServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllIesServices((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'getAllIesServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnTemplateUpdateIesServicesBodyParam = {
      data: {
        adminState: 'UP',
        appId: 'string',
        bidirectional: 'ASYMMETRIC_LOOSE',
        customerId: 2,
        id: 'string',
        innerTag: 6,
        maxCost: 10,
        maxHops: 2,
        maxLatency: 1,
        mediationProfileId: 'string',
        meta: [
          'string'
        ],
        mtu: 5,
        name: 'string',
        objectDescription: 'string',
        objective: 'COST',
        outerTag: 8,
        portFilter: [
          {
            condition1: {
              operator: 'string',
              value: 'string'
            },
            condition2: {
              operator: 'string',
              value: 'string'
            },
            connector: 'string',
            groupNum: 8,
            name: 'string',
            operator: 'string'
          }
        ],
        qosId: 'string',
        qosName: 'string',
        relaxEndpointValidation: false,
        workflowProfileId: 'string',
        workflowValidate: 'string'
      }
    };
    describe('#updateIesServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateIesServices(sdnTemplateUpdateIesServicesBodyParam, sdnTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'updateIesServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIesServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIesServices(sdnTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'getIesServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllL2DciVpnServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllL2DciVpnServices((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'getAllL2DciVpnServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnTemplateUpdateL2DciVpnServicesBodyParam = {
      data: {
        appId: 'string',
        autobind: 'bgp',
        descriptionGenerationTemplate: 'string',
        dualHomed: true,
        ecmp: 4,
        id: 'string',
        mediationProfileId: 'string',
        meta: [
          'string'
        ],
        mtu: 3,
        name: 'string',
        nameGenerationMaxLength: 1,
        nameGenerationTemplate: 'string',
        objectDescription: 'string',
        qosId: 'string',
        qosName: 'string',
        rdRtRangePolicyId: 'string',
        tunnelSelectionId: 'string',
        workflowProfileId: 'string'
      }
    };
    describe('#updateL2DciVpnServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateL2DciVpnServices(sdnTemplateUpdateL2DciVpnServicesBodyParam, sdnTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'updateL2DciVpnServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getL2DciVpnServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getL2DciVpnServices(sdnTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'getL2DciVpnServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllL2backhaulServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllL2backhaulServices((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'getAllL2backhaulServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnTemplateUpdateL2backhaulServicesBodyParam = {
      data: {
        adminState: 'MAINTENANCE',
        appId: 'string',
        bidirectional: 'SYMMETRIC_LOOSE',
        customerId: 5,
        id: 'string',
        innerTag: 4,
        maxCost: 3,
        maxHops: 4,
        maxLatency: 7,
        meta: [
          'string'
        ],
        monitorBandwidth: false,
        mtu: 5,
        name: 'string',
        objectDescription: 'string',
        objective: 'DISTANCE',
        outerTag: 8,
        portFilter: [
          {
            condition1: {
              operator: 'string',
              value: 'string'
            },
            condition2: {
              operator: 'string',
              value: 'string'
            },
            connector: 'string',
            groupNum: 7,
            name: 'string',
            operator: 'string'
          }
        ],
        qosId: 'string',
        qosName: 'string',
        tunnelSelectionId: 'string',
        vcType: 'ETHERNET',
        workflowProfileId: 'string',
        workflowValidate: 'string'
      }
    };
    describe('#updateL2backhaulServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateL2backhaulServices(sdnTemplateUpdateL2backhaulServicesBodyParam, sdnTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'updateL2backhaulServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getL2backhaulServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getL2backhaulServices(sdnTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'getL2backhaulServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllL3DciVpnServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllL3DciVpnServices((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'getAllL3DciVpnServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnTemplateUpdateL3DciVpnServicesBodyParam = {
      data: {
        adminState: 'DOWN',
        appId: 'string',
        autobind: 'gre',
        backhaulMtu: 2,
        bidirectional: 'SYMMETRIC_STRICT',
        connectivity: 'VXLAN_STITCHED',
        customerId: 9,
        dualHomed: true,
        ecmp: 9,
        id: 'string',
        innerTag: 7,
        maxCost: 9,
        maxHops: 6,
        maxLatency: 8,
        mediationProfileId: 'string',
        meta: [
          'string'
        ],
        mtu: 4,
        name: 'string',
        objectDescription: 'string',
        objective: 'HOPS',
        outerTag: 2,
        portFilter: [
          {
            condition1: {
              operator: 'string',
              value: 'string'
            },
            condition2: {
              operator: 'string',
              value: 'string'
            },
            connector: 'string',
            groupNum: 7,
            name: 'string',
            operator: 'string'
          }
        ],
        qosId: 'string',
        qosName: 'string',
        rdRtRangePolicyId: 'string',
        relaxEndpointValidation: false,
        transportSliceName: 'string',
        tunnelSelectionId: 'string',
        vrfPerEndpoint: true,
        workflowProfileId: 'string',
        workflowValidate: 'string'
      }
    };
    describe('#updateL3DciVpnServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateL3DciVpnServices(sdnTemplateUpdateL3DciVpnServicesBodyParam, sdnTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'updateL3DciVpnServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getL3DciVpnServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getL3DciVpnServices(sdnTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'getL3DciVpnServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllL3VpnServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllL3VpnServices((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'getAllL3VpnServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnTemplateUpdateL3VpnServicesBodyParam = {
      data: {
        adminState: 'MAINTENANCE',
        appId: 'string',
        autobind: 'ldp',
        bidirectional: 'ANY_REVERSE_ROUTE',
        customerId: 8,
        id: 'string',
        innerTag: 2,
        maxCost: 2,
        maxHops: 5,
        maxLatency: 6,
        mediationProfileId: 'string',
        meta: [
          'string'
        ],
        mtu: 5,
        name: 'string',
        objectDescription: 'string',
        objective: 'DISTANCE',
        outerTag: 5,
        portFilter: [
          {
            condition1: {
              operator: 'string',
              value: 'string'
            },
            condition2: {
              operator: 'string',
              value: 'string'
            },
            connector: 'string',
            groupNum: 3,
            name: 'string',
            operator: 'string'
          }
        ],
        qosId: 'string',
        qosName: 'string',
        rdRtRangePolicyId: 'string',
        relaxEndpointValidation: true,
        transportSliceName: 'string',
        tunnelSelectionId: 'string',
        vrfPerEndpoint: false,
        workflowProfileId: 'string',
        workflowValidate: 'string'
      }
    };
    describe('#updateL3VpnServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateL3VpnServices(sdnTemplateUpdateL3VpnServicesBodyParam, sdnTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'updateL3VpnServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getL3VpnServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getL3VpnServices(sdnTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'getL3VpnServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllLagServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllLagServices((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'getAllLagServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnTemplateUpdateLagServicesBodyParam = {
      data: {
        appId: 'string',
        bandwidth: 10,
        bidirectional: 'ANY_REVERSE_ROUTE',
        customerId: 8,
        id: 'string',
        maxCost: 6,
        maxHops: 9,
        maxLatency: 4,
        monitorBandwidth: false,
        name: 'string',
        objectDescription: 'string',
        objective: 'LATENCY',
        reverseBandwidth: 1
      }
    };
    describe('#updateLagServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateLagServices(sdnTemplateUpdateLagServicesBodyParam, sdnTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'updateLagServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLagServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getLagServices(sdnTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'getLagServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnTemplateProfileId = 'fakedata';
    const sdnTemplateUpdateMediationProfileMappingBodyParam = {
      data: {
        appId: 'string',
        id: 'string',
        mediationInfos: [
          {
            mediationName: 'string',
            mediationVersion: 'string',
            templateGlobalName: 'string'
          }
        ],
        name: 'string',
        objectDescription: 'string',
        profileType: 'ELINE'
      }
    };
    describe('#updateMediationProfileMapping - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateMediationProfileMapping(sdnTemplateUpdateMediationProfileMappingBodyParam, sdnTemplateProfileId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'updateMediationProfileMapping', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMediationProfileMapping - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMediationProfileMapping(sdnTemplateProfileId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'getMediationProfileMapping', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllMediationProfileMappings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllMediationProfileMappings((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'getAllMediationProfileMappings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNfmpTemplate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNfmpTemplate((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'getNfmpTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllOchServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllOchServices((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'getAllOchServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnTemplateUpdateOchServicesBodyParam = {
      data: {
        appId: 'string',
        bidirectional: 'SYMMETRIC_STRICT',
        customerId: 5,
        id: 'string',
        maxCost: 3,
        maxHops: 9,
        maxLatency: 4,
        name: 'string',
        objectDescription: 'string',
        objective: 'STAR_WEIGHT',
        restoration: 'SBR',
        reversionMode: 'auto'
      }
    };
    describe('#updateOchServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateOchServices(sdnTemplateUpdateOchServicesBodyParam, sdnTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'updateOchServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOchServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOchServices(sdnTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'getOchServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllOduServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllOduServices((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'getAllOduServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnTemplateUpdateOduServicesBodyParam = {
      data: {
        appId: 'string',
        bandwidth: 4,
        bidirectional: 'SYMMETRIC_STRICT',
        customerId: 7,
        id: 'string',
        maxCost: 6,
        maxHops: 5,
        maxLatency: 2,
        name: 'string',
        objectDescription: 'string',
        objective: 'TE_METRIC',
        reverseBandwidth: 9
      }
    };
    describe('#updateOduServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateOduServices(sdnTemplateUpdateOduServicesBodyParam, sdnTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'updateOduServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOduServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOduServices(sdnTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'getOduServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllOpticalConnectivityConstraint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllOpticalConnectivityConstraint((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'getAllOpticalConnectivityConstraint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnTemplateUpdateOpticalConnectivityConstraintBodyParam = {
      data: {
        appId: 'string',
        connectivityConstraint: {
          capacity: {
            capacityValue: {}
          },
          connectivityDirection: 'UNDEFINED_OR_UNKNOWN',
          schedule: {
            timeRange: {}
          },
          serviceLevel: 'string',
          serviceType: 'MULTIPOINT_CONNECTIVITY'
        },
        id: 'string',
        name: 'string',
        objectDescription: 'string'
      }
    };
    describe('#updateOpticalConnectivityConstraint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateOpticalConnectivityConstraint(sdnTemplateUpdateOpticalConnectivityConstraintBodyParam, sdnTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'updateOpticalConnectivityConstraint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOpticalConnectivityConstraint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOpticalConnectivityConstraint(sdnTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'getOpticalConnectivityConstraint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllOpticalConnectivityService - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllOpticalConnectivityService((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'getAllOpticalConnectivityService', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnTemplateUpdateOpticalConnectivityServiceBodyParam = {
      data: {
        appId: 'string',
        connectivityConstraintTemplateId: 'string',
        id: 'string',
        name: 'string',
        objectDescription: 'string',
        resilienceConstraintTemplateIds: [
          'string'
        ],
        routingConstraintTemplateId: 'string'
      }
    };
    describe('#updateOpticalConnectivityService - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateOpticalConnectivityService(sdnTemplateUpdateOpticalConnectivityServiceBodyParam, sdnTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'updateOpticalConnectivityService', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOpticalConnectivityService - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOpticalConnectivityService(sdnTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'getOpticalConnectivityService', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllOpticalResilienceConstraint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllOpticalResilienceConstraint((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'getAllOpticalResilienceConstraint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnTemplateUpdateOpticalResilienceConstraintBodyParam = {
      data: {
        appId: 'string',
        id: 'string',
        name: 'string',
        objectDescription: 'string',
        resilienceConstraint: {
          holdOffTime: 8,
          isCoordinatedSwitchingBothEnds: false,
          isFrozen: false,
          isLockOut: false,
          maxSwitchTimes: 5,
          resilienceType: {
            protectionType: 'ONE_FOR_ONE_PROTECTION',
            restorationPolicy: 'END_TO_END_RESTORATION'
          },
          restorationCoordinateType: 'NO_COORDINATE',
          restorePriority: 4,
          reversionMode: 'REVERTIVE',
          waitToRevertTime: 8
        }
      }
    };
    describe('#updateOpticalResilienceConstraint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateOpticalResilienceConstraint(sdnTemplateUpdateOpticalResilienceConstraintBodyParam, sdnTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'updateOpticalResilienceConstraint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOpticalResilienceConstraint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOpticalResilienceConstraint(sdnTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'getOpticalResilienceConstraint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllOpticalRoutingConstraint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllOpticalRoutingConstraint((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'getAllOpticalRoutingConstraint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnTemplateUpdateOpticalRoutingConstraintBodyParam = {
      data: {
        appId: 'string',
        id: 'string',
        name: 'string',
        objectDescription: 'string',
        routingConstraint: {
          costCharacteristic: [
            {
              costAlgorithm: 'string',
              costName: 'string',
              costValue: 'string'
            }
          ],
          diversityPolicy: 'NODE',
          isExclusive: false,
          latencyCharacteristic: [
            {
              fixedLatencyCharacteristic: 'string',
              jitterCharacteristic: 'string',
              queingLatencyCharacteristic: 'string',
              trafficPropertyName: 'string',
              wanderCharacteristic: 'string'
            }
          ],
          riskDiversityCharacteristic: [
            {
              riskCharacteristicName: 'string',
              riskIdentifierList: [
                'string'
              ]
            }
          ],
          routeDirection: 'BIDIRECTIONAL',
          routeObjectiveFunction: 'LOAD_BALANCE_MAX_UNUSED_CAPACITY'
        }
      }
    };
    describe('#updateOpticalRoutingConstraint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateOpticalRoutingConstraint(sdnTemplateUpdateOpticalRoutingConstraintBodyParam, sdnTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'updateOpticalRoutingConstraint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOpticalRoutingConstraint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOpticalRoutingConstraint(sdnTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'getOpticalRoutingConstraint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllPathProfiles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllPathProfiles((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'getAllPathProfiles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnTemplateUpdatePathProfilesBodyParam = {
      data: {
        appId: 'string',
        bandwidthStrategy: 'STANDARD',
        bidirection: 'NO',
        bwSplitMonitoringTime: 4,
        bwSplitSupported: true,
        bwSplitThreshold: 10,
        bwSplitWorkflowName: 'string',
        controlRerouteStrategy: 'LOOSE',
        disjoint: 'SRLG',
        excludeRouteObjects: [
          {
            ipv4Address: {},
            ipv6Address: {}
          }
        ],
        explicitRouteStrategy: 'LOOSE_HOP_ANYCAST_PREFERRED',
        explicitRouteStrategyEcmpPreference: 'NODE_SID',
        id: 'string',
        includeRouteObjects: {},
        latencyThreshold: 9,
        maxCost: 2,
        maxHops: 7,
        maxLatency: 7,
        maxTeMetric: 2,
        name: 'string',
        objectDescription: 'string',
        objective: 'DISTANCE',
        profileId: 9,
        sidProtectionStrategy: 'UNPROTECTED_PREFERRED'
      }
    };
    describe('#updatePathProfiles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updatePathProfiles(sdnTemplateUpdatePathProfilesBodyParam, sdnTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'updatePathProfiles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPathProfiles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPathProfiles(sdnTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'getPathProfiles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllQos - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllQos((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'getAllQos', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllQosPolicies - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllQosPolicies((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'getAllQosPolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getQosPolicies - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getQosPolicies(sdnTemplateNeId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'getQosPolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnTemplateUpdateQosBodyParam = {
      data: {
        appId: 'string',
        egressParam: {
          tier1Scheduler: {
            scheduler: {}
          }
        },
        egressQueueOverride: [
          {
            cbs: 9,
            cir: 9,
            id: 1,
            mbs: 1,
            pir: 5,
            queueType: 'Policer',
            rateType: 'kbps'
          }
        ],
        id: 'string',
        ingressParam: {
          tier1Scheduler: {
            scheduler: {}
          }
        },
        ingressQueueOverride: [
          {
            cbs: 8,
            cir: 3,
            id: 9,
            mbs: 5,
            pir: 8,
            queueType: 'Policer',
            rateType: 'percent'
          }
        ],
        name: 'string',
        objectDescription: 'string',
        qosProfile: 3
      }
    };
    describe('#updateQos - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateQos(sdnTemplateUpdateQosBodyParam, sdnTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'updateQos', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getQos - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getQos(sdnTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'getQos', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllRouterIdSystemIdMappings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllRouterIdSystemIdMappings((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'getAllRouterIdSystemIdMappings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnTemplatePolicyId = 'fakedata';
    const sdnTemplateUpdateRouterIdSystemIdMappingBodyParam = {
      data: {
        appId: 'string',
        id: 'string',
        name: 'string',
        objectDescription: 'string',
        pccAddress: {
          ipv4Address: {},
          ipv6Address: {}
        },
        routerInfos: [
          {
            asNumber: {
              uint32: 5
            },
            bgpLsId: 2,
            dcIdentifier: 4,
            networkIdentifier: {
              uint32: 9
            },
            protocol: 'ANY',
            routerId: {
              string: 'string'
            }
          }
        ],
        systemId: {
          ipv4Address: {},
          ipv6Address: {}
        },
        systemName: 'string'
      }
    };
    describe('#updateRouterIdSystemIdMapping - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateRouterIdSystemIdMapping(sdnTemplatePolicyId, sdnTemplateUpdateRouterIdSystemIdMappingBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'updateRouterIdSystemIdMapping', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRouterIdSystemIdMapping - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRouterIdSystemIdMapping(sdnTemplatePolicyId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'getRouterIdSystemIdMapping', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllSystemIpMplsConfig - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllSystemIpMplsConfig((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'getAllSystemIpMplsConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnTemplateUpdateSystemIpMplsConfigBodyParam = {
      data: {
        appId: 'string',
        id: 'string',
        maintenanceMode: 'AUTOMATIC',
        name: 'string',
        objectDescription: 'string'
      }
    };
    describe('#updateSystemIpMplsConfig - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateSystemIpMplsConfig(sdnTemplateUpdateSystemIpMplsConfigBodyParam, sdnTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'updateSystemIpMplsConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllTunnelCreations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllTunnelCreations((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'getAllTunnelCreations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnTemplateUpdateTunnelCreationsBodyParam = {
      data: {
        appId: 'string',
        consumable: false,
        deletable: false,
        id: 'string',
        modifiable: false,
        name: 'string',
        objectDescription: 'string',
        protection: true,
        protectionType: 'UNKNOWN'
      }
    };
    describe('#updateTunnelCreations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateTunnelCreations(sdnTemplateTemplateId, sdnTemplateUpdateTunnelCreationsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'updateTunnelCreations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllWorkflowProfileMappings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllWorkflowProfileMappings((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'getAllWorkflowProfileMappings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnTemplateUpdateWorkflowProfileBodyParam = {
      data: {
        appId: 'string',
        id: 'string',
        name: 'string',
        objectDescription: 'string',
        workflowProfileInfo: [
          {
            blockServiceDeployment: true,
            continueServiceDeploymentOnError: false,
            serviceStep: 'postcreate_success',
            workflowExecutionTimeout: 3,
            workflowId: 'string',
            workflowInputs: 'string',
            workflowParams: 'string'
          }
        ]
      }
    };
    describe('#updateWorkflowProfile - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateWorkflowProfile(sdnTemplateProfileId, sdnTemplateUpdateWorkflowProfileBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'updateWorkflowProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWorkflowProfile - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWorkflowProfile(sdnTemplateProfileId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'getWorkflowProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnTenantsPostV4TenantsBodyParam = {
      data: {
        address: 'string',
        contactName: 'string',
        customerId: 10,
        id: 'string',
        phoneNumber: 'string',
        tenantName: 'string'
      }
    };
    describe('#postV4Tenants - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postV4Tenants(sdnTenantsPostV4TenantsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTenants', 'postV4Tenants', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnTenantsTenantUuid = 'fakedata';
    const sdnTenantsAddResourcesBodyParam = {
      data: [
        'string'
      ]
    };
    describe('#addResources - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addResources(sdnTenantsAddResourcesBodyParam, sdnTenantsTenantUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTenants', 'addResources', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnTenantsGroupName = 'fakedata';
    const sdnTenantsRoleType = 'fakedata';
    describe('#createUsergroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createUsergroup(sdnTenantsGroupName, sdnTenantsRoleType, sdnTenantsTenantUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTenants', 'createUsergroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4Tenants - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4Tenants((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTenants', 'getV4Tenants', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCustomerAll - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCustomerAll((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTenants', 'getCustomerAll', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnTenantsCustomerId = 555;
    describe('#getCustomer - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCustomer(sdnTenantsCustomerId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTenants', 'getCustomer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnTenantsProvider = 'fakedata';
    describe('#resync - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.resync(sdnTenantsProvider, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTenants', 'resync', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnTenantsPutV4TenantsTenantUuidBodyParam = {
      data: {
        address: 'string',
        contactName: 'string',
        customerId: 5,
        phoneNumber: 'string'
      }
    };
    describe('#putV4TenantsTenantUuid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putV4TenantsTenantUuid(sdnTenantsPutV4TenantsTenantUuidBodyParam, sdnTenantsTenantUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTenants', 'putV4TenantsTenantUuid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4TenantsTenantUuid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4TenantsTenantUuid(sdnTenantsTenantUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTenants', 'getV4TenantsTenantUuid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getResources - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getResources(sdnTenantsTenantUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTenants', 'getResources', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsergroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUsergroup(sdnTenantsGroupName, sdnTenantsTenantUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTenants', 'getUsergroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4Usergroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4Usergroups((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnUsergroups', 'getV4Usergroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnUsergroupsGroupName = 'fakedata';
    describe('#getV4UsergroupsGroupName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4UsergroupsGroupName(sdnUsergroupsGroupName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnUsergroups', 'getV4UsergroupsGroupName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4UsergroupsGroupNameTenants - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getV4UsergroupsGroupNameTenants(sdnUsergroupsGroupName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnUsergroups', 'getV4UsergroupsGroupNameTenants', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteServiceEndpoint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteServiceEndpoint(sdnServicesEndpointUuid, sdnServicesServiceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'deleteServiceEndpoint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ServicesUuid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteV4ServicesUuid(sdnServicesUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'deleteV4ServicesUuid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAugmentationMeta - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteAugmentationMeta(sdnMediationId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnMediation', 'deleteAugmentationMeta', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLspPath - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteLspPath(sdnMplsPathId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnMpls', 'deleteLspPath', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnMplsDeleteLspPathsBodyParam = {
      data: [
        'string'
      ]
    };
    describe('#deleteLspPaths - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteLspPaths(sdnMplsDeleteLspPathsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnMpls', 'deleteLspPaths', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnNrcpConfigurationSession = 'fakedata';
    describe('#deleteLatency - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteLatency(sdnNrcpConfigurationSession, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnNrcpConfiguration', 'deleteLatency', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnOpenflowDeleteFlowsBodyParam = {
      data: [
        'string'
      ]
    };
    describe('#deleteFlows - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteFlows(sdnOpenflowDeleteFlowsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnOpenflow', 'deleteFlows', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnPhysicallinksLinkId = 'fakedata';
    describe('#delete - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.delete(sdnPhysicallinksLinkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnPhysicallinks', 'delete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpOpticalCorrelationPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteIpOpticalCorrelationPolicy(sdnPolicyPolicyId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnPolicy', 'deleteIpOpticalCorrelationPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRdRtRanges - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteRdRtRanges(sdnPolicyPolicyId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnPolicy', 'deleteRdRtRanges', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRouterPortProtectionGroupPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteRouterPortProtectionGroupPolicy(sdnPolicyPolicyId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnPolicy', 'deleteRouterPortProtectionGroupPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnPolicySteeringParameterName = 'fakedata';
    describe('#deleteSteeringParameter - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteSteeringParameter(sdnPolicySteeringParameterName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnPolicy', 'deleteSteeringParameter', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTunnelSelections - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteTunnelSelections(sdnPolicyPolicyId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnPolicy', 'deleteTunnelSelections', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteClineServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteClineServices(sdnTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'deleteClineServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCustomAttributes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteCustomAttributes(sdnTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'deleteCustomAttributes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteElanServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteElanServices(sdnTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'deleteElanServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteElineServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteElineServices(sdnTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'deleteElineServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteGenericQos - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteGenericQos(sdnTemplateGqpId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'deleteGenericQos', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIesServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteIesServices(sdnTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'deleteIesServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteL2backhaulServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteL2backhaulServices(sdnTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'deleteL2backhaulServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteL3DciVpnServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteL3DciVpnServices(sdnTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'deleteL3DciVpnServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteL3VpnServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteL3VpnServices(sdnTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'deleteL3VpnServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLagServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteLagServices(sdnTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'deleteLagServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteMediationProfileMapping - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteMediationProfileMapping(sdnTemplateProfileId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'deleteMediationProfileMapping', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOchServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteOchServices(sdnTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'deleteOchServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOduServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteOduServices(sdnTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'deleteOduServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOpticalConnectivityConstraint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteOpticalConnectivityConstraint(sdnTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'deleteOpticalConnectivityConstraint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOpticalConnectivityService - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteOpticalConnectivityService(sdnTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'deleteOpticalConnectivityService', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOpticalResilienceConstraint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteOpticalResilienceConstraint(sdnTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'deleteOpticalResilienceConstraint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOpticalRoutingConstraint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteOpticalRoutingConstraint(sdnTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'deleteOpticalRoutingConstraint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePathProfiles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deletePathProfiles(sdnTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'deletePathProfiles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteQos - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteQos(sdnTemplateTemplateId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'deleteQos', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRouterIdSystemIdMapping - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteRouterIdSystemIdMapping(sdnTemplatePolicyId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'deleteRouterIdSystemIdMapping', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWorkflowProfile - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteWorkflowProfile(sdnTemplateProfileId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTemplate', 'deleteWorkflowProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4TenantsTenantUuid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteV4TenantsTenantUuid(sdnTenantsTenantUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTenants', 'deleteV4TenantsTenantUuid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnTenantsDeleteResourcesBodyParam = {
      data: [
        'string'
      ]
    };
    describe('#deleteResources - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteResources(sdnTenantsDeleteResourcesBodyParam, sdnTenantsTenantUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTenants', 'deleteResources', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUsergroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteUsergroup(sdnTenantsGroupName, sdnTenantsTenantUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnTenants', 'deleteUsergroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNrcpHistorical - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNrcpHistorical((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnNrcpConfiguration', 'getNrcpHistorical', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnNrcpConfigurationPatchNrcpHistoricalBodyParam = {
      data: {
        historicalIgpTopologyEnabled: false,
        historicalLspStatsEnabled: false,
        historicalPathEnabled: false
      }
    };
    describe('#patchNrcpHistorical - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchNrcpHistorical(sdnNrcpConfigurationPatchNrcpHistoricalBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnNrcpConfiguration', 'patchNrcpHistorical', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSrPolicyConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSrPolicyConfig((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnNrcpConfiguration', 'getSrPolicyConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnNrcpConfigurationPatchSrPolicyConfigBodyParam = {
      data: {
        bsidGenerationEnabled: true,
        defaultBsidRangeEnd: 9,
        defaultBsidRangeStart: 7,
        defaultColorRangeEnd: 7,
        defaultColorRangeStart: 6
      }
    };
    describe('#patchSrPolicyConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchSrPolicyConfig(sdnNrcpConfigurationPatchSrPolicyConfigBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnNrcpConfiguration', 'patchSrPolicyConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesAddElineEndpointBodyParam = {
      data: {
        adminState: 'MAINTENANCE',
        appId: 'string',
        connectionProfileId: 'string',
        connectionProfileVlanTag: 'UNDEFINED',
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        description: 'string',
        encapType: 'Null',
        id: 'string',
        innerTag: 8,
        isHub: true,
        localAcName: 'string',
        localEthTag: 3,
        name: 'string',
        outerTag: 8,
        portMode: 'NETWORK',
        readOnly: false,
        remoteAcName: 'string',
        remoteEthTag: 4,
        siteServiceQosProfile: {
          egressOverrideQueues: [
            {
              cbs: 3,
              cir: 4,
              id: 2,
              mbs: 1,
              pir: 1,
              queueType: 'Policer',
              rateType: 'kbps'
            }
          ],
          egressParam: {
            tier1Scheduler: {
              scheduler: {
                cir: 8,
                pir: 7,
                rateType: 'kbps',
                schedulerType: 'Scheduler'
              }
            }
          },
          ingressOverrideQueues: [
            {
              cbs: 4,
              cir: 1,
              id: 5,
              mbs: 6,
              pir: 8,
              queueType: 'Policer',
              rateType: 'kbps'
            }
          ],
          ingressParam: {
            tier1Scheduler: {
              scheduler: {
                cir: 10,
                pir: 3,
                rateType: 'kbps',
                schedulerType: 'Scheduler'
              }
            }
          },
          qosProfile: 10
        }
      }
    };
    describe('#addElineEndpoint - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addElineEndpoint('fakedata', sdnServicesAddElineEndpointBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'addElineEndpoint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesAddL3DciL3EndpointBodyParam = {
      data: {
        adminState: 'MAINTENANCE',
        appId: 'string',
        connectionProfileId: 'string',
        connectionProfileVlanTag: 'UNDEFINED',
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        description: 'string',
        encapType: 'Null',
        id: 'string',
        innerTag: 6,
        isHub: true,
        name: 'string',
        outerTag: 6,
        portMode: 'NETWORK',
        primaryAddress: {
          ipv4Prefix: {
            string: 'string'
          },
          ipv6Prefix: {
            string: 'string'
          }
        },
        readOnly: true,
        routingBgp: {
          routes: [
            {
              bfdEnabled: false,
              customAttributes: [
                {
                  attributeName: 'string',
                  attributeValue: 'string'
                }
              ],
              peerAS: 10,
              peerIpAddress: {
                ipv4Address: {
                  string: 'string'
                },
                ipv6Address: {
                  string: 'string'
                }
              }
            }
          ]
        },
        routingStatic: {
          routes: [
            {
              customAttributes: [
                {
                  attributeName: 'string',
                  attributeValue: 'string'
                }
              ],
              destination: {
                ipv4Prefix: {
                  string: 'string'
                },
                ipv6Prefix: {
                  string: 'string'
                }
              },
              preference: 8,
              routeType: 'black_hole',
              targetIpAddress: {
                ipv4Address: {
                  string: 'string'
                },
                ipv6Address: {
                  string: 'string'
                }
              }
            }
          ]
        },
        secondaryAddresses: [
          {
            ipv4Prefix: {
              string: 'string'
            },
            ipv6Prefix: {
              string: 'string'
            }
          }
        ],
        siteServiceQosProfile: {
          egressOverrideQueues: [
            {
              cbs: 5,
              cir: 9,
              id: 2,
              mbs: 6,
              pir: 3,
              queueType: 'Policer',
              rateType: 'kbps'
            }
          ],
          egressParam: {
            tier1Scheduler: {
              scheduler: {
                cir: 4,
                pir: 8,
                rateType: 'kbps',
                schedulerType: 'Scheduler'
              }
            }
          },
          ingressOverrideQueues: [
            {
              cbs: 3,
              cir: 5,
              id: 8,
              mbs: 8,
              pir: 8,
              queueType: 'Policer',
              rateType: 'kbps'
            }
          ],
          ingressParam: {
            tier1Scheduler: {
              scheduler: {
                cir: 8,
                pir: 7,
                rateType: 'kbps',
                schedulerType: 'Scheduler'
              }
            }
          },
          qosProfile: 4
        },
        vrfName: 'string'
      }
    };
    describe('#addL3DciL3Endpoint - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addL3DciL3Endpoint('fakedata', sdnServicesAddL3DciL3EndpointBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'addL3DciL3Endpoint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesAddL3DciL3LoopbackEndpointBodyParam = {
      data: {
        adminState: 'MAINTENANCE',
        appId: 'string',
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        description: 'string',
        id: 'string',
        isHub: false,
        name: 'string',
        primaryAddress: {
          ipv4Prefix: {
            string: 'string'
          },
          ipv6Prefix: {
            string: 'string'
          }
        },
        readOnly: false,
        secondaryAddresses: [
          {
            ipv4Prefix: {
              string: 'string'
            },
            ipv6Prefix: {
              string: 'string'
            }
          }
        ],
        vrfName: 'string'
      }
    };
    describe('#addL3DciL3LoopbackEndpoint - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addL3DciL3LoopbackEndpoint('fakedata', sdnServicesAddL3DciL3LoopbackEndpointBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'addL3DciL3LoopbackEndpoint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnServicesPatchL3DciL3LoopbackEndpointBodyParam = {
      data: {
        adminState: 'MAINTENANCE',
        appId: 'string',
        customAttributes: [
          {
            attributeName: 'string',
            attributeValue: 'string'
          }
        ],
        customAttributesTemplateId: 'string',
        description: 'string',
        id: 'string',
        isHub: false,
        name: 'string',
        primaryAddress: {
          ipv4Prefix: {
            string: 'string'
          },
          ipv6Prefix: {
            string: 'string'
          }
        },
        readOnly: false,
        secondaryAddresses: [
          {
            ipv4Prefix: {
              string: 'string'
            },
            ipv6Prefix: {
              string: 'string'
            }
          }
        ],
        vrfName: 'string'
      }
    };
    describe('#patchL3DciL3LoopbackEndpoint - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchL3DciL3LoopbackEndpoint('fakedata', 'fakedata', sdnServicesPatchL3DciL3LoopbackEndpointBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'patchL3DciL3LoopbackEndpoint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateStitchL2Ext - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateStitchL2Ext('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServices', 'updateStitchL2Ext', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTopologyReferencesByDomain - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTopologyReferencesByDomain('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnNsp', 'deleteTopologyReferencesByDomain', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTopologyReferencesByLink - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTopologyReferencesByLink('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnNsp', 'deleteTopologyReferencesByLink', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#diagnoseLsp - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.diagnoseLsp('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnPathTool', 'diagnoseLsp', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnPathToolDiagnosePathBodyParam = {
      data: {
        destinationAddress: {
          ipv4Address: {
            string: 'string'
          },
          ipv6Address: {
            string: 'string'
          }
        },
        pathRequest: {
          pathTeParams: {
            adminGroupExcludeAny: {
              adminGroup: {
                binary: 'string'
              },
              extendedAdminGroup: {
                binary: 'string'
              }
            },
            adminGroupIncludeAll: {
              adminGroup: {
                binary: 'string'
              },
              extendedAdminGroup: {
                binary: 'string'
              }
            },
            adminGroupIncludeAny: {
              adminGroup: {
                binary: 'string'
              },
              extendedAdminGroup: {
                binary: 'string'
              }
            },
            bandwidth: 3,
            bidirectional: 'SYMMETRIC_LOOSE',
            diversity: 'NO',
            excludeRouteObjects: [
              {
                ipv4Address: {
                  string: 'string'
                },
                ipv6Address: {
                  string: 'string'
                }
              }
            ],
            explicitRouteStrategy: 'LOOSE_HOP_ANYCAST_PREFERRED',
            explicitRouteStrategyEcmpPreference: 'ADJACENCY_SID',
            includeRouteObjects: {},
            ipAddressTypeStrategy: 'IPV6',
            maxCost: 9,
            maxHops: 8,
            maxLatency: 3,
            maxTeMetric: 6,
            msd: 5,
            objective: 'TE_METRIC',
            pathProfileId: 5,
            reverseBandwidth: 1
          }
        },
        pathType: 'SRTE',
        secondaryDestinationAddress: {
          ipv4Address: {
            string: 'string'
          },
          ipv6Address: {
            string: 'string'
          }
        },
        secondarySourceAddress: {
          ipv4Address: {
            string: 'string'
          },
          ipv6Address: {
            string: 'string'
          }
        },
        sourceAddress: {
          ipv4Address: {
            string: 'string'
          },
          ipv6Address: {
            string: 'string'
          }
        }
      }
    };
    describe('#diagnosePath - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.diagnosePath(sdnPathToolDiagnosePathBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnPathTool', 'diagnosePath', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnPathToolFindPathBodyParam = {
      data: {
        destinationAddress: {
          ipv4Address: {
            string: 'string'
          },
          ipv6Address: {
            string: 'string'
          }
        },
        pathRequest: {
          pathTeParams: {
            adminGroupExcludeAny: {
              adminGroup: {
                binary: 'string'
              },
              extendedAdminGroup: {
                binary: 'string'
              }
            },
            adminGroupIncludeAll: {
              adminGroup: {
                binary: 'string'
              },
              extendedAdminGroup: {
                binary: 'string'
              }
            },
            adminGroupIncludeAny: {
              adminGroup: {
                binary: 'string'
              },
              extendedAdminGroup: {
                binary: 'string'
              }
            },
            bandwidth: 3,
            bidirectional: 'SYMMETRIC_LOOSE',
            diversity: 'NO',
            excludeRouteObjects: [
              {
                ipv4Address: {
                  string: 'string'
                },
                ipv6Address: {
                  string: 'string'
                }
              }
            ],
            explicitRouteStrategy: 'LOOSE_HOP_ANYCAST_PREFERRED',
            explicitRouteStrategyEcmpPreference: 'ADJACENCY_SID',
            includeRouteObjects: {},
            ipAddressTypeStrategy: 'IPV6',
            maxCost: 3,
            maxHops: 5,
            maxLatency: 4,
            maxTeMetric: 2,
            msd: 10,
            objective: 'TE_METRIC',
            pathProfileId: 7,
            reverseBandwidth: 10
          }
        },
        pathType: 'SRTE',
        secondaryDestinationAddress: {
          ipv4Address: {
            string: 'string'
          },
          ipv6Address: {
            string: 'string'
          }
        },
        secondarySourceAddress: {
          ipv4Address: {
            string: 'string'
          },
          ipv6Address: {
            string: 'string'
          }
        },
        sourceAddress: {
          ipv4Address: {
            string: 'string'
          },
          ipv6Address: {
            string: 'string'
          }
        }
      }
    };
    describe('#findSdnPath - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.findSdnPath(sdnPathToolFindPathBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnPathTool', 'findSdnPath', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addMultipointPort - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addMultipointPort('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnPhysicallinks', 'addMultipointPort', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeMultipointPort - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.removeMultipointPort('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnPhysicallinks', 'removeMultipointPort', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteMultipoint - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteMultipoint('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnPhysicallinks', 'deleteMultipoint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnPhysicallinksCreateMultipointBodyParam = {
      data: [
        'string'
      ]
    };
    describe('#createMultipoint - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createMultipoint('fakedata', sdnPhysicallinksCreateMultipointBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnPhysicallinks', 'createMultipoint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMultipoint - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getMultipoint('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnPhysicallinks', 'getMultipoint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMultipoints - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getMultipoints((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnPhysicallinks', 'getMultipoints', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllConnectionProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAllConnectionProfile('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnPolicy', 'getAllConnectionProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createTriggerTtzAlgorithm - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createTriggerTtzAlgorithm((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnServiceDebug', 'createTriggerTtzAlgorithm', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSRPolicies - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSRPolicies((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnSrPolicies', 'getSRPolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnSrPoliciesCreateSRPolicyBodyParam = {
      data: [
        {
          adminState: 'MAINTENANCE',
          candidatePathsConfig: {
            candidatePath: {}
          },
          color: 3,
          description: 'string',
          endpoint: 'string',
          headend: 'string',
          name: 'string',
          policyParamsConfig: {
            policyProperties: {
              priority: 7
            }
          }
        }
      ]
    };
    describe('#createSRPolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createSRPolicy(sdnSrPoliciesCreateSRPolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnSrPolicies', 'createSRPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createCandidatePathResignal - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createCandidatePathResignal('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnSrPolicies', 'createCandidatePathResignal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchCandidatePathAdmin - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchCandidatePathAdmin('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnSrPolicies', 'patchCandidatePathAdmin', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnSrPoliciesUpdateCandidatePathsBodyParam = {
      data: {
        adminState: 'MAINTENANCE',
        description: 'string',
        paramsConfig: {
          candidatePathConfigType: {
            dynamic: {
              dynamicPathProperties: {
                bounds: {
                  igpMetricBound: 1,
                  latencyMetricBound: 10,
                  maxHops: 5,
                  segmentBound: 1,
                  teMetricBound: 7
                },
                constraints: {
                  affinities: {
                    excludeAny: {
                      adminGroup: {
                        binary: 'string'
                      },
                      extendedAdminGroup: {
                        binary: 'string'
                      }
                    },
                    includeAll: {
                      adminGroup: {
                        binary: 'string'
                      },
                      extendedAdminGroup: {
                        binary: 'string'
                      }
                    },
                    includeAny: {
                      adminGroup: {
                        binary: 'string'
                      },
                      extendedAdminGroup: {
                        binary: 'string'
                      }
                    }
                  }
                },
                objective: 'TE_METRIC',
                pathProfileId: 3,
                sidDataplaneType: 'mpls'
              }
            },
            explicit: {
              segmentListsConfigRequest: {
                segmentListsConfig: {
                  segmentList: [
                    {
                      name: 'string',
                      segmentsConfig: {
                        segment: {}
                      },
                      weight: 2
                    }
                  ]
                }
              }
            }
          },
          candidatePathProperties: {
            bandwidth: 4,
            bsid: {
              bindingSid_properties: {
                dataplane: 'mpls',
                value: {
                  mplsLabel: {
                    int32: 4
                  },
                  srv6Sid: {
                    ipv6Prefix: {
                      string: 'string'
                    }
                  }
                }
              }
            }
          }
        },
        preference: 8
      }
    };
    describe('#updateCandidatePaths - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateCandidatePaths('fakedata', sdnSrPoliciesUpdateCandidatePathsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnSrPolicies', 'updateCandidatePaths', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSRPoliciesOnLink - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSRPoliciesOnLink('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnSrPolicies', 'getSRPoliciesOnLink', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePolicies - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePolicies((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnSrPolicies', 'deletePolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnSrPoliciesUpdatePoliciesBodyParam = {
      data: {
        adminState: 'MAINTENANCE',
        candidatePathsConfig: {
          candidatePath: {}
        },
        description: 'string',
        policyParamsConfig: {
          policyProperties: {
            priority: 4
          }
        }
      }
    };
    describe('#updatePolicies - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updatePolicies('fakedata', sdnSrPoliciesUpdatePoliciesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnSrPolicies', 'updatePolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnSrPoliciesFetchPolicyListBodyParam = {
      data: [
        'string'
      ]
    };
    describe('#fetchPolicyList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.fetchPolicyList(sdnSrPoliciesFetchPolicyListBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnSrPolicies', 'fetchPolicyList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePolicy('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnSrPolicies', 'deletePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPolicyAdmin - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchPolicyAdmin('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnSrPolicies', 'patchPolicyAdmin', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnSrPoliciesCreateSRPoliciesResignalBodyParam = {
      data: [
        'string'
      ]
    };
    describe('#createSRPoliciesResignal - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createSRPoliciesResignal(sdnSrPoliciesCreateSRPoliciesResignalBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnSrPolicies', 'createSRPoliciesResignal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAssocGroupConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAssocGroupConfig((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnNrcpConfiguration', 'getAssocGroupConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnNrcpConfigurationPatchAssocGroupConfigBodyParam = {};
    describe('#patchAssocGroupConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchAssocGroupConfig(sdnNrcpConfigurationPatchAssocGroupConfigBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnNrcpConfiguration', 'patchAssocGroupConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBsidGenerationConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getBsidGenerationConfig((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnNrcpConfiguration', 'getBsidGenerationConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnNrcpConfigurationPatchBsidGenerationConfigBodyParam = {};
    describe('#patchBsidGenerationConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchBsidGenerationConfig(sdnNrcpConfigurationPatchBsidGenerationConfigBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnNrcpConfiguration', 'patchBsidGenerationConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceNameIpService - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDeviceNameIpService((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnNrcpConfiguration', 'getDeviceNameIpService', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnNrcpConfigurationPatchDeviceNameIpServiceBodyParam = {};
    describe('#patchDeviceNameIpService - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchDeviceNameIpService(sdnNrcpConfigurationPatchDeviceNameIpServiceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnNrcpConfiguration', 'patchDeviceNameIpService', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLspBsidGenerationConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getLspBsidGenerationConfig((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnNrcpConfiguration', 'getLspBsidGenerationConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnNrcpConfigurationPatchLspBsidGenerationConfigBodyParam = {};
    describe('#patchLspBsidGenerationConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchLspBsidGenerationConfig(sdnNrcpConfigurationPatchLspBsidGenerationConfigBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnNrcpConfiguration', 'patchLspBsidGenerationConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAssocGroups - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAssocGroups((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnMpls', 'getAssocGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAssocGroupsByType - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAssocGroupsByType('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnMpls', 'getAssocGroupsByType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnMplsLspPathProfileOverrideBodyParam = {};
    describe('#lspPathProfileOverride - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.lspPathProfileOverride(sdnMplsLspPathProfileOverrideBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnMpls', 'lspPathProfileOverride', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnMplsFetchLspPathsByTunnelIdBodyParam = {};
    describe('#fetchLspPathsByTunnelId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.fetchLspPathsByTunnelId(sdnMplsFetchLspPathsByTunnelIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnMpls', 'fetchLspPathsByTunnelId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOnAssocGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getOnAssocGroup(555, 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnMplsLspPath', 'getOnAssocGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOnBsid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getOnBsid('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnMplsLspPath', 'getOnBsid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getProfileGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getProfileGroup(555, 555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnMplsLspPath', 'getProfileGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnNspFetchLinksBodyParam = {};
    describe('#fetchLinks - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.fetchLinks(sdnNspFetchLinksBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnNsp', 'fetchLinks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#findPath - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.findPath(sdnPathToolFindPathBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnPathTool', 'findPath', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnSrPoliciesCreateBodyParam = {};
    describe('#getByKey - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getByKey('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnSrPolicies', 'getByKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnSrPoliciesFetchCandidatePathListBodyParam = {};
    describe('#fetchCandidatePathList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.fetchCandidatePathList(sdnSrPoliciesFetchCandidatePathListBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnSrPolicies', 'fetchCandidatePathList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConsumer - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getConsumer('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnSrPolicies', 'getConsumer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sdnSrPoliciesCreateResignalBodyParam = {};
    describe('#createResignalAll - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createResignalAll((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnSrPolicies', 'createResignalAll', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createResignalSRPolicies - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createResignalSRPolicies(sdnSrPoliciesCreateResignalBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnSrPolicies', 'createResignalSRPolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSRPolicyByID - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSRPolicyByID('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-nokia_nsp_sdn-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SdnSrPolicies', 'getSRPolicyByID', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
