/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-nokia_nsp_sdn',
      type: 'NokiaNSPSDN',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const NokiaNSPSDN = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] nokia_nsp_sdn Adapter Test', () => {
  describe('NokiaNSPSDN Class Tests', () => {
    const a = new NokiaNSPSDN(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('nokia_nsp_sdn'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.7.9', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.8.2', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.17.0', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('nokia_nsp_sdn'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('NokiaNSPSDN', pronghornDotJson.export);
          assert.equal('nokia_nsp_sdn', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-nokia_nsp_sdn', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('nokia_nsp_sdn'));
          assert.equal('NokiaNSPSDN', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-nokia_nsp_sdn', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-nokia_nsp_sdn', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#getApplicationId - errors', () => {
      it('should have a getApplicationId function', (done) => {
        try {
          assert.equal(true, typeof a.getApplicationId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getApplicationId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getApplicationId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setApplicationId - errors', () => {
      it('should have a setApplicationId function', (done) => {
        try {
          assert.equal(true, typeof a.setApplicationId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appId', (done) => {
        try {
          a.setApplicationId(null, null, (data, error) => {
            try {
              const displayE = 'appId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-setApplicationId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.setApplicationId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-setApplicationId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConsumed - errors', () => {
      it('should have a getConsumed function', (done) => {
        try {
          assert.equal(true, typeof a.getConsumed === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.getConsumed(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getConsumed', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#findByExternalId - errors', () => {
      it('should have a findByExternalId function', (done) => {
        try {
          assert.equal(true, typeof a.findByExternalId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTenants - errors', () => {
      it('should have a getTenants function', (done) => {
        try {
          assert.equal(true, typeof a.getTenants === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.getTenants(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getTenants', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#get - errors', () => {
      it('should have a get function', (done) => {
        try {
          assert.equal(true, typeof a.get === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.get(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-get', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addL2BackhaulEndpoint - errors', () => {
      it('should have a addL2BackhaulEndpoint function', (done) => {
        try {
          assert.equal(true, typeof a.addL2BackhaulEndpoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.addL2BackhaulEndpoint('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-addL2BackhaulEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateL2BackhaulEndpoint - errors', () => {
      it('should have a updateL2BackhaulEndpoint function', (done) => {
        try {
          assert.equal(true, typeof a.updateL2BackhaulEndpoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endpointUuid', (done) => {
        try {
          a.updateL2BackhaulEndpoint(null, null, null, (data, error) => {
            try {
              const displayE = 'endpointUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateL2BackhaulEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.updateL2BackhaulEndpoint('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateL2BackhaulEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchL2BackhaulEndpoint - errors', () => {
      it('should have a patchL2BackhaulEndpoint function', (done) => {
        try {
          assert.equal(true, typeof a.patchL2BackhaulEndpoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endpointUuid', (done) => {
        try {
          a.patchL2BackhaulEndpoint(null, null, null, (data, error) => {
            try {
              const displayE = 'endpointUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-patchL2BackhaulEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.patchL2BackhaulEndpoint('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-patchL2BackhaulEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createL2backhaul - errors', () => {
      it('should have a createL2backhaul function', (done) => {
        try {
          assert.equal(true, typeof a.createL2backhaul === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing autoPath', (done) => {
        try {
          a.createL2backhaul(null, null, (data, error) => {
            try {
              const displayE = 'autoPath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-createL2backhaul', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateL2backhaul - errors', () => {
      it('should have a updateL2backhaul function', (done) => {
        try {
          assert.equal(true, typeof a.updateL2backhaul === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.updateL2backhaul('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateL2backhaul', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchL2backhaul - errors', () => {
      it('should have a patchL2backhaul function', (done) => {
        try {
          assert.equal(true, typeof a.patchL2backhaul === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.patchL2backhaul('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-patchL2backhaul', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4Services - errors', () => {
      it('should have a getV4Services function', (done) => {
        try {
          assert.equal(true, typeof a.getV4Services === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createClines - errors', () => {
      it('should have a createClines function', (done) => {
        try {
          assert.equal(true, typeof a.createClines === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateClines - errors', () => {
      it('should have a updateClines function', (done) => {
        try {
          assert.equal(true, typeof a.updateClines === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.updateClines('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateClines', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchClines - errors', () => {
      it('should have a patchClines function', (done) => {
        try {
          assert.equal(true, typeof a.patchClines === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.patchClines('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-patchClines', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateClineEndpoint - errors', () => {
      it('should have a updateClineEndpoint function', (done) => {
        try {
          assert.equal(true, typeof a.updateClineEndpoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endpointUuid', (done) => {
        try {
          a.updateClineEndpoint(null, null, null, (data, error) => {
            try {
              const displayE = 'endpointUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateClineEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.updateClineEndpoint('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateClineEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchClineEndpoint - errors', () => {
      it('should have a patchClineEndpoint function', (done) => {
        try {
          assert.equal(true, typeof a.patchClineEndpoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endpointUuid', (done) => {
        try {
          a.patchClineEndpoint(null, null, null, (data, error) => {
            try {
              const displayE = 'endpointUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-patchClineEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.patchClineEndpoint('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-patchClineEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getComponents - errors', () => {
      it('should have a getComponents function', (done) => {
        try {
          assert.equal(true, typeof a.getComponents === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.getComponents(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getComponents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createEaccess - errors', () => {
      it('should have a createEaccess function', (done) => {
        try {
          assert.equal(true, typeof a.createEaccess === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateEaccess - errors', () => {
      it('should have a updateEaccess function', (done) => {
        try {
          assert.equal(true, typeof a.updateEaccess === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.updateEaccess('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateEaccess', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchEaccess - errors', () => {
      it('should have a patchEaccess function', (done) => {
        try {
          assert.equal(true, typeof a.patchEaccess === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.patchEaccess('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-patchEaccess', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createElans - errors', () => {
      it('should have a createElans function', (done) => {
        try {
          assert.equal(true, typeof a.createElans === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateElans - errors', () => {
      it('should have a updateElans function', (done) => {
        try {
          assert.equal(true, typeof a.updateElans === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.updateElans('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateElans', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchElans - errors', () => {
      it('should have a patchElans function', (done) => {
        try {
          assert.equal(true, typeof a.patchElans === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.patchElans('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-patchElans', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addElanEndpoint - errors', () => {
      it('should have a addElanEndpoint function', (done) => {
        try {
          assert.equal(true, typeof a.addElanEndpoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.addElanEndpoint('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-addElanEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateElanEndpoint - errors', () => {
      it('should have a updateElanEndpoint function', (done) => {
        try {
          assert.equal(true, typeof a.updateElanEndpoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endpointUuid', (done) => {
        try {
          a.updateElanEndpoint(null, null, null, (data, error) => {
            try {
              const displayE = 'endpointUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateElanEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.updateElanEndpoint('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateElanEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchElanEndpoint - errors', () => {
      it('should have a patchElanEndpoint function', (done) => {
        try {
          assert.equal(true, typeof a.patchElanEndpoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endpointUuid', (done) => {
        try {
          a.patchElanEndpoint(null, null, null, (data, error) => {
            try {
              const displayE = 'endpointUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-patchElanEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.patchElanEndpoint('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-patchElanEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createElines - errors', () => {
      it('should have a createElines function', (done) => {
        try {
          assert.equal(true, typeof a.createElines === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateElines - errors', () => {
      it('should have a updateElines function', (done) => {
        try {
          assert.equal(true, typeof a.updateElines === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.updateElines('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateElines', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchElines - errors', () => {
      it('should have a patchElines function', (done) => {
        try {
          assert.equal(true, typeof a.patchElines === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.patchElines('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-patchElines', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateElineEndpoint - errors', () => {
      it('should have a updateElineEndpoint function', (done) => {
        try {
          assert.equal(true, typeof a.updateElineEndpoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endpointUuid', (done) => {
        try {
          a.updateElineEndpoint(null, null, null, (data, error) => {
            try {
              const displayE = 'endpointUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateElineEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.updateElineEndpoint('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateElineEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchElineEndpoint - errors', () => {
      it('should have a patchElineEndpoint function', (done) => {
        try {
          assert.equal(true, typeof a.patchElineEndpoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endpointUuid', (done) => {
        try {
          a.patchElineEndpoint(null, null, null, (data, error) => {
            try {
              const displayE = 'endpointUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-patchElineEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.patchElineEndpoint('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-patchElineEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateFixL3VpnVrfMembership - errors', () => {
      it('should have a updateFixL3VpnVrfMembership function', (done) => {
        try {
          assert.equal(true, typeof a.updateFixL3VpnVrfMembership === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.updateFixL3VpnVrfMembership(null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateFixL3VpnVrfMembership', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIes - errors', () => {
      it('should have a createIes function', (done) => {
        try {
          assert.equal(true, typeof a.createIes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIes - errors', () => {
      it('should have a updateIes function', (done) => {
        try {
          assert.equal(true, typeof a.updateIes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.updateIes('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateIes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchIes - errors', () => {
      it('should have a patchIes function', (done) => {
        try {
          assert.equal(true, typeof a.patchIes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.patchIes('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-patchIes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addIesEndpoint - errors', () => {
      it('should have a addIesEndpoint function', (done) => {
        try {
          assert.equal(true, typeof a.addIesEndpoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addIesEndpoint(null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-addIesEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.addIesEndpoint('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-addIesEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIesEndpoint - errors', () => {
      it('should have a updateIesEndpoint function', (done) => {
        try {
          assert.equal(true, typeof a.updateIesEndpoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endpointUuid', (done) => {
        try {
          a.updateIesEndpoint(null, null, null, (data, error) => {
            try {
              const displayE = 'endpointUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateIesEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIesEndpoint('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateIesEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.updateIesEndpoint('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateIesEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchIesEndpoint - errors', () => {
      it('should have a patchIesEndpoint function', (done) => {
        try {
          assert.equal(true, typeof a.patchIesEndpoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endpointUuid', (done) => {
        try {
          a.patchIesEndpoint(null, null, null, (data, error) => {
            try {
              const displayE = 'endpointUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-patchIesEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchIesEndpoint('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-patchIesEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.patchIesEndpoint('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-patchIesEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addIesLoopbackEndpoint - errors', () => {
      it('should have a addIesLoopbackEndpoint function', (done) => {
        try {
          assert.equal(true, typeof a.addIesLoopbackEndpoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.addIesLoopbackEndpoint('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-addIesLoopbackEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIesLoopbackEndpoint - errors', () => {
      it('should have a updateIesLoopbackEndpoint function', (done) => {
        try {
          assert.equal(true, typeof a.updateIesLoopbackEndpoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endpointUuid', (done) => {
        try {
          a.updateIesLoopbackEndpoint(null, null, null, (data, error) => {
            try {
              const displayE = 'endpointUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateIesLoopbackEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.updateIesLoopbackEndpoint('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateIesLoopbackEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchIesLoopbackEndpoint - errors', () => {
      it('should have a patchIesLoopbackEndpoint function', (done) => {
        try {
          assert.equal(true, typeof a.patchIesLoopbackEndpoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endpointUuid', (done) => {
        try {
          a.patchIesLoopbackEndpoint(null, null, null, (data, error) => {
            try {
              const displayE = 'endpointUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-patchIesLoopbackEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.patchIesLoopbackEndpoint('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-patchIesLoopbackEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createL3DciVpns - errors', () => {
      it('should have a createL3DciVpns function', (done) => {
        try {
          assert.equal(true, typeof a.createL3DciVpns === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchL3DciL3Endpoint - errors', () => {
      it('should have a patchL3DciL3Endpoint function', (done) => {
        try {
          assert.equal(true, typeof a.patchL3DciL3Endpoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endpointUuid', (done) => {
        try {
          a.patchL3DciL3Endpoint(null, null, null, (data, error) => {
            try {
              const displayE = 'endpointUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-patchL3DciL3Endpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchL3DciL3Endpoint('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-patchL3DciL3Endpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.patchL3DciL3Endpoint('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-patchL3DciL3Endpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getL3VpnRtAudit - errors', () => {
      it('should have a getL3VpnRtAudit function', (done) => {
        try {
          assert.equal(true, typeof a.getL3VpnRtAudit === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createL3Vpns - errors', () => {
      it('should have a createL3Vpns function', (done) => {
        try {
          assert.equal(true, typeof a.createL3Vpns === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateL3Vpns - errors', () => {
      it('should have a updateL3Vpns function', (done) => {
        try {
          assert.equal(true, typeof a.updateL3Vpns === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.updateL3Vpns('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateL3Vpns', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchL3Vpns - errors', () => {
      it('should have a patchL3Vpns function', (done) => {
        try {
          assert.equal(true, typeof a.patchL3Vpns === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.patchL3Vpns('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-patchL3Vpns', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addL3VpnEndpoint - errors', () => {
      it('should have a addL3VpnEndpoint function', (done) => {
        try {
          assert.equal(true, typeof a.addL3VpnEndpoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addL3VpnEndpoint(null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-addL3VpnEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.addL3VpnEndpoint('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-addL3VpnEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateL3VpnEndpoint - errors', () => {
      it('should have a updateL3VpnEndpoint function', (done) => {
        try {
          assert.equal(true, typeof a.updateL3VpnEndpoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endpointUuid', (done) => {
        try {
          a.updateL3VpnEndpoint(null, null, null, (data, error) => {
            try {
              const displayE = 'endpointUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateL3VpnEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateL3VpnEndpoint('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateL3VpnEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.updateL3VpnEndpoint('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateL3VpnEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchL3VpnEndpoint - errors', () => {
      it('should have a patchL3VpnEndpoint function', (done) => {
        try {
          assert.equal(true, typeof a.patchL3VpnEndpoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endpointUuid', (done) => {
        try {
          a.patchL3VpnEndpoint(null, null, null, (data, error) => {
            try {
              const displayE = 'endpointUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-patchL3VpnEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchL3VpnEndpoint('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-patchL3VpnEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.patchL3VpnEndpoint('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-patchL3VpnEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addL3VpnLoopbackEndpoint - errors', () => {
      it('should have a addL3VpnLoopbackEndpoint function', (done) => {
        try {
          assert.equal(true, typeof a.addL3VpnLoopbackEndpoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.addL3VpnLoopbackEndpoint('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-addL3VpnLoopbackEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateL3VpnLoopbackEndpoint - errors', () => {
      it('should have a updateL3VpnLoopbackEndpoint function', (done) => {
        try {
          assert.equal(true, typeof a.updateL3VpnLoopbackEndpoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endpointUuid', (done) => {
        try {
          a.updateL3VpnLoopbackEndpoint(null, null, null, (data, error) => {
            try {
              const displayE = 'endpointUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateL3VpnLoopbackEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.updateL3VpnLoopbackEndpoint('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateL3VpnLoopbackEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchL3VpnLoopbackEndpoint - errors', () => {
      it('should have a patchL3VpnLoopbackEndpoint function', (done) => {
        try {
          assert.equal(true, typeof a.patchL3VpnLoopbackEndpoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endpointUuid', (done) => {
        try {
          a.patchL3VpnLoopbackEndpoint(null, null, null, (data, error) => {
            try {
              const displayE = 'endpointUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-patchL3VpnLoopbackEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.patchL3VpnLoopbackEndpoint('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-patchL3VpnLoopbackEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createLags - errors', () => {
      it('should have a createLags function', (done) => {
        try {
          assert.equal(true, typeof a.createLags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateLags - errors', () => {
      it('should have a updateLags function', (done) => {
        try {
          assert.equal(true, typeof a.updateLags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.updateLags('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateLags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNormalizedL3Vpns - errors', () => {
      it('should have a createNormalizedL3Vpns function', (done) => {
        try {
          assert.equal(true, typeof a.createNormalizedL3Vpns === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNormalizedL3Vpns - errors', () => {
      it('should have a updateNormalizedL3Vpns function', (done) => {
        try {
          assert.equal(true, typeof a.updateNormalizedL3Vpns === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.updateNormalizedL3Vpns('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateNormalizedL3Vpns', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchNormalizedL3Vpns - errors', () => {
      it('should have a patchNormalizedL3Vpns function', (done) => {
        try {
          assert.equal(true, typeof a.patchNormalizedL3Vpns === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.patchNormalizedL3Vpns('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-patchNormalizedL3Vpns', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addL3VpnNormalizedEndpoint - errors', () => {
      it('should have a addL3VpnNormalizedEndpoint function', (done) => {
        try {
          assert.equal(true, typeof a.addL3VpnNormalizedEndpoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addL3VpnNormalizedEndpoint(null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-addL3VpnNormalizedEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.addL3VpnNormalizedEndpoint('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-addL3VpnNormalizedEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createOptical - errors', () => {
      it('should have a createOptical function', (done) => {
        try {
          assert.equal(true, typeof a.createOptical === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateOptical - errors', () => {
      it('should have a updateOptical function', (done) => {
        try {
          assert.equal(true, typeof a.updateOptical === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.updateOptical('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateOptical', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchOptical - errors', () => {
      it('should have a patchOptical function', (done) => {
        try {
          assert.equal(true, typeof a.patchOptical === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.patchOptical('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-patchOptical', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPhysicalLinksOnService - errors', () => {
      it('should have a getPhysicalLinksOnService function', (done) => {
        try {
          assert.equal(true, typeof a.getPhysicalLinksOnService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.getPhysicalLinksOnService(null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getPhysicalLinksOnService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getResourceCount - errors', () => {
      it('should have a getResourceCount function', (done) => {
        try {
          assert.equal(true, typeof a.getResourceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceType', (done) => {
        try {
          a.getResourceCount(null, null, (data, error) => {
            try {
              const displayE = 'resourceType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getResourceCount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantUuid', (done) => {
        try {
          a.getResourceCount('fakeparam', null, (data, error) => {
            try {
              const displayE = 'tenantUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getResourceCount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServicesOnTunnel - errors', () => {
      it('should have a getServicesOnTunnel function', (done) => {
        try {
          assert.equal(true, typeof a.getServicesOnTunnel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tunnelUuid', (done) => {
        try {
          a.getServicesOnTunnel(null, (data, error) => {
            try {
              const displayE = 'tunnelUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getServicesOnTunnel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ServicesTenantUuid - errors', () => {
      it('should have a getV4ServicesTenantUuid function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ServicesTenantUuid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.getV4ServicesTenantUuid(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getV4ServicesTenantUuid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTunnel - errors', () => {
      it('should have a getTunnel function', (done) => {
        try {
          assert.equal(true, typeof a.getTunnel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tunnelUuid', (done) => {
        try {
          a.getTunnel(null, (data, error) => {
            try {
              const displayE = 'tunnelUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getTunnel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTunnel - errors', () => {
      it('should have a updateTunnel function', (done) => {
        try {
          assert.equal(true, typeof a.updateTunnel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tunnelUuid', (done) => {
        try {
          a.updateTunnel('fakeparam', null, (data, error) => {
            try {
              const displayE = 'tunnelUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateTunnel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTunnels - errors', () => {
      it('should have a getTunnels function', (done) => {
        try {
          assert.equal(true, typeof a.getTunnels === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTunnelsOnService - errors', () => {
      it('should have a getTunnelsOnService function', (done) => {
        try {
          assert.equal(true, typeof a.getTunnelsOnService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.getTunnelsOnService(null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getTunnelsOnService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteServiceEndpoint - errors', () => {
      it('should have a deleteServiceEndpoint function', (done) => {
        try {
          assert.equal(true, typeof a.deleteServiceEndpoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endpointUuid', (done) => {
        try {
          a.deleteServiceEndpoint(null, null, (data, error) => {
            try {
              const displayE = 'endpointUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-deleteServiceEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.deleteServiceEndpoint('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-deleteServiceEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEndpoints - errors', () => {
      it('should have a getEndpoints function', (done) => {
        try {
          assert.equal(true, typeof a.getEndpoints === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.getEndpoints(null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getEndpoints', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4ServicesUuid - errors', () => {
      it('should have a getV4ServicesUuid function', (done) => {
        try {
          assert.equal(true, typeof a.getV4ServicesUuid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.getV4ServicesUuid(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getV4ServicesUuid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4ServicesUuid - errors', () => {
      it('should have a deleteV4ServicesUuid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4ServicesUuid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.deleteV4ServicesUuid(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-deleteV4ServicesUuid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLink - errors', () => {
      it('should have a getLink function', (done) => {
        try {
          assert.equal(true, typeof a.getLink === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkId', (done) => {
        try {
          a.getLink(null, (data, error) => {
            try {
              const displayE = 'linkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getLink', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchLink - errors', () => {
      it('should have a patchLink function', (done) => {
        try {
          assert.equal(true, typeof a.patchLink === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkUuid', (done) => {
        try {
          a.patchLink(null, null, (data, error) => {
            try {
              const displayE = 'linkUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-patchLink', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetwork - errors', () => {
      it('should have a getNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.getNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetwork(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworks - errors', () => {
      it('should have a getNetworks function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNode - errors', () => {
      it('should have a getNode function', (done) => {
        try {
          assert.equal(true, typeof a.getNode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeId', (done) => {
        try {
          a.getNode(null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getNode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTerminationPoint - errors', () => {
      it('should have a getTerminationPoint function', (done) => {
        try {
          assert.equal(true, typeof a.getTerminationPoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tpId', (done) => {
        try {
          a.getTerminationPoint(null, (data, error) => {
            try {
              const displayE = 'tpId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getTerminationPoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getL2Paths - errors', () => {
      it('should have a getL2Paths function', (done) => {
        try {
          assert.equal(true, typeof a.getL2Paths === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAmiVersionTemplates - errors', () => {
      it('should have a getAmiVersionTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.getAmiVersionTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAmisVersions - errors', () => {
      it('should have a getAmisVersions function', (done) => {
        try {
          assert.equal(true, typeof a.getAmisVersions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllAugmentationMeta - errors', () => {
      it('should have a getAllAugmentationMeta function', (done) => {
        try {
          assert.equal(true, typeof a.getAllAugmentationMeta === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAugmentationMeta - errors', () => {
      it('should have a createAugmentationMeta function', (done) => {
        try {
          assert.equal(true, typeof a.createAugmentationMeta === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAugmentationMeta - errors', () => {
      it('should have a updateAugmentationMeta function', (done) => {
        try {
          assert.equal(true, typeof a.updateAugmentationMeta === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateAugmentationMeta('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateAugmentationMeta', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAugmentationMeta - errors', () => {
      it('should have a deleteAugmentationMeta function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAugmentationMeta === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteAugmentationMeta(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-deleteAugmentationMeta', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMediationAugmentation - errors', () => {
      it('should have a getMediationAugmentation function', (done) => {
        try {
          assert.equal(true, typeof a.getMediationAugmentation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#fetchLspList - errors', () => {
      it('should have a fetchLspList function', (done) => {
        try {
          assert.equal(true, typeof a.fetchLspList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createLspPath - errors', () => {
      it('should have a createLspPath function', (done) => {
        try {
          assert.equal(true, typeof a.createLspPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createLspPath(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-createLspPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#fetchLspPathList - errors', () => {
      it('should have a fetchLspPathList function', (done) => {
        try {
          assert.equal(true, typeof a.fetchLspPathList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLspPath - errors', () => {
      it('should have a getLspPath function', (done) => {
        try {
          assert.equal(true, typeof a.getLspPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathId', (done) => {
        try {
          a.getLspPath(null, (data, error) => {
            try {
              const displayE = 'pathId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getLspPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLspPath - errors', () => {
      it('should have a deleteLspPath function', (done) => {
        try {
          assert.equal(true, typeof a.deleteLspPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathId', (done) => {
        try {
          a.deleteLspPath(null, (data, error) => {
            try {
              const displayE = 'pathId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-deleteLspPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchLspPath - errors', () => {
      it('should have a patchLspPath function', (done) => {
        try {
          assert.equal(true, typeof a.patchLspPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchLspPath(null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-patchLspPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pathId', (done) => {
        try {
          a.patchLspPath('fakeparam', null, (data, error) => {
            try {
              const displayE = 'pathId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-patchLspPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLspPaths - errors', () => {
      it('should have a getLspPaths function', (done) => {
        try {
          assert.equal(true, typeof a.getLspPaths === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createLspPaths - errors', () => {
      it('should have a createLspPaths function', (done) => {
        try {
          assert.equal(true, typeof a.createLspPaths === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createLspPaths(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-createLspPaths', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLspPaths - errors', () => {
      it('should have a deleteLspPaths function', (done) => {
        try {
          assert.equal(true, typeof a.deleteLspPaths === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.deleteLspPaths(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-deleteLspPaths', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLspPathsPaginated - errors', () => {
      it('should have a getLspPathsPaginated function', (done) => {
        try {
          assert.equal(true, typeof a.getLspPathsPaginated === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing limit', (done) => {
        try {
          a.getLspPathsPaginated(null, null, (data, error) => {
            try {
              const displayE = 'limit is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getLspPathsPaginated', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing page', (done) => {
        try {
          a.getLspPathsPaginated('fakeparam', null, (data, error) => {
            try {
              const displayE = 'page is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getLspPathsPaginated', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLsp - errors', () => {
      it('should have a getLsp function', (done) => {
        try {
          assert.equal(true, typeof a.getLsp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing lspId', (done) => {
        try {
          a.getLsp(null, (data, error) => {
            try {
              const displayE = 'lspId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getLsp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLsps - errors', () => {
      it('should have a getLsps function', (done) => {
        try {
          assert.equal(true, typeof a.getLsps === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createOptimization - errors', () => {
      it('should have a createOptimization function', (done) => {
        try {
          assert.equal(true, typeof a.createOptimization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createResignal - errors', () => {
      it('should have a createResignal function', (done) => {
        try {
          assert.equal(true, typeof a.createResignal === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOnLink - errors', () => {
      it('should have a getOnLink function', (done) => {
        try {
          assert.equal(true, typeof a.getOnLink === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkId', (done) => {
        try {
          a.getOnLink(null, null, (data, error) => {
            try {
              const displayE = 'linkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getOnLink', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestType', (done) => {
        try {
          a.getOnLink('fakeparam', null, (data, error) => {
            try {
              const displayE = 'requestType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getOnLink', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#gets - errors', () => {
      it('should have a gets function', (done) => {
        try {
          assert.equal(true, typeof a.gets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSystem - errors', () => {
      it('should have a getSystem function', (done) => {
        try {
          assert.equal(true, typeof a.getSystem === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing systemId', (done) => {
        try {
          a.getSystem(null, (data, error) => {
            try {
              const displayE = 'systemId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getSystem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#update - errors', () => {
      it('should have a update function', (done) => {
        try {
          assert.equal(true, typeof a.update === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.update(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-update', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4NeUuid - errors', () => {
      it('should have a getV4NeUuid function', (done) => {
        try {
          assert.equal(true, typeof a.getV4NeUuid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.getV4NeUuid(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getV4NeUuid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLatency - errors', () => {
      it('should have a getLatency function', (done) => {
        try {
          assert.equal(true, typeof a.getLatency === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchLatency - errors', () => {
      it('should have a patchLatency function', (done) => {
        try {
          assert.equal(true, typeof a.patchLatency === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLatency - errors', () => {
      it('should have a deleteLatency function', (done) => {
        try {
          assert.equal(true, typeof a.deleteLatency === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing session', (done) => {
        try {
          a.deleteLatency(null, (data, error) => {
            try {
              const displayE = 'session is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-deleteLatency', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTcaConfigPolicy - errors', () => {
      it('should have a getTcaConfigPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.getTcaConfigPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchTcaConfigPolicy - errors', () => {
      it('should have a patchTcaConfigPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.patchTcaConfigPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTrafficDataCollection - errors', () => {
      it('should have a getTrafficDataCollection function', (done) => {
        try {
          assert.equal(true, typeof a.getTrafficDataCollection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchTrafficDataCollection - errors', () => {
      it('should have a patchTrafficDataCollection function', (done) => {
        try {
          assert.equal(true, typeof a.patchTrafficDataCollection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLinkTp - errors', () => {
      it('should have a getLinkTp function', (done) => {
        try {
          assert.equal(true, typeof a.getLinkTp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tpId', (done) => {
        try {
          a.getLinkTp(null, (data, error) => {
            try {
              const displayE = 'tpId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getLinkTp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4NspNetL3LinkLinkId - errors', () => {
      it('should have a getV4NspNetL3LinkLinkId function', (done) => {
        try {
          assert.equal(true, typeof a.getV4NspNetL3LinkLinkId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkId', (done) => {
        try {
          a.getV4NspNetL3LinkLinkId(null, (data, error) => {
            try {
              const displayE = 'linkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getV4NspNetL3LinkLinkId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchV4NspNetL3LinkLinkUuid - errors', () => {
      it('should have a patchV4NspNetL3LinkLinkUuid function', (done) => {
        try {
          assert.equal(true, typeof a.patchV4NspNetL3LinkLinkUuid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkUuid', (done) => {
        try {
          a.patchV4NspNetL3LinkLinkUuid('fakeparam', null, (data, error) => {
            try {
              const displayE = 'linkUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-patchV4NspNetL3LinkLinkUuid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4NspNetL3NetworkNetworkId - errors', () => {
      it('should have a getV4NspNetL3NetworkNetworkId function', (done) => {
        try {
          assert.equal(true, typeof a.getV4NspNetL3NetworkNetworkId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getV4NspNetL3NetworkNetworkId(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getV4NspNetL3NetworkNetworkId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4NspNetL3Networks - errors', () => {
      it('should have a getV4NspNetL3Networks function', (done) => {
        try {
          assert.equal(true, typeof a.getV4NspNetL3Networks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4NspNetL3NodeNodeId - errors', () => {
      it('should have a getV4NspNetL3NodeNodeId function', (done) => {
        try {
          assert.equal(true, typeof a.getV4NspNetL3NodeNodeId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeId', (done) => {
        try {
          a.getV4NspNetL3NodeNodeId(null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getV4NspNetL3NodeNodeId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodesRouter - errors', () => {
      it('should have a getNodesRouter function', (done) => {
        try {
          assert.equal(true, typeof a.getNodesRouter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routerId', (done) => {
        try {
          a.getNodesRouter(null, (data, error) => {
            try {
              const displayE = 'routerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getNodesRouter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodesSite - errors', () => {
      it('should have a getNodesSite function', (done) => {
        try {
          assert.equal(true, typeof a.getNodesSite === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteIp', (done) => {
        try {
          a.getNodesSite(null, (data, error) => {
            try {
              const displayE = 'siteIp is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getNodesSite', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4NspNetL3TerminationPointTpId - errors', () => {
      it('should have a getV4NspNetL3TerminationPointTpId function', (done) => {
        try {
          assert.equal(true, typeof a.getV4NspNetL3TerminationPointTpId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tpId', (done) => {
        try {
          a.getV4NspNetL3TerminationPointTpId(null, (data, error) => {
            try {
              const displayE = 'tpId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getV4NspNetL3TerminationPointTpId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTerminationPointReverse - errors', () => {
      it('should have a getTerminationPointReverse function', (done) => {
        try {
          assert.equal(true, typeof a.getTerminationPointReverse === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tpId', (done) => {
        try {
          a.getTerminationPointReverse(null, (data, error) => {
            try {
              const displayE = 'tpId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getTerminationPointReverse', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTerminationPointsNodeip - errors', () => {
      it('should have a getTerminationPointsNodeip function', (done) => {
        try {
          assert.equal(true, typeof a.getTerminationPointsNodeip === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipAddress', (done) => {
        try {
          a.getTerminationPointsNodeip(null, null, (data, error) => {
            try {
              const displayE = 'ipAddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getTerminationPointsNodeip', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeId', (done) => {
        try {
          a.getTerminationPointsNodeip('fakeparam', null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getTerminationPointsNodeip', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createFlows - errors', () => {
      it('should have a createFlows function', (done) => {
        try {
          assert.equal(true, typeof a.createFlows === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createFlows(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-createFlows', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFlows - errors', () => {
      it('should have a deleteFlows function', (done) => {
        try {
          assert.equal(true, typeof a.deleteFlows === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchFlows - errors', () => {
      it('should have a patchFlows function', (done) => {
        try {
          assert.equal(true, typeof a.patchFlows === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchFlows(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-patchFlows', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFlowsearch - errors', () => {
      it('should have a getFlowsearch function', (done) => {
        try {
          assert.equal(true, typeof a.getFlowsearch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFlowsearchById - errors', () => {
      it('should have a getFlowsearchById function', (done) => {
        try {
          assert.equal(true, typeof a.getFlowsearchById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPorts - errors', () => {
      it('should have a getPorts function', (done) => {
        try {
          assert.equal(true, typeof a.getPorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing datapathId', (done) => {
        try {
          a.getPorts(null, (data, error) => {
            try {
              const displayE = 'datapathId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getPorts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSwitches - errors', () => {
      it('should have a getSwitches function', (done) => {
        try {
          assert.equal(true, typeof a.getSwitches === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSwitchesInRouter - errors', () => {
      it('should have a getSwitchesInRouter function', (done) => {
        try {
          assert.equal(true, typeof a.getSwitchesInRouter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.getSwitchesInRouter(null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getSwitchesInRouter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTables - errors', () => {
      it('should have a getTables function', (done) => {
        try {
          assert.equal(true, typeof a.getTables === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing datapathId', (done) => {
        try {
          a.getTables(null, (data, error) => {
            try {
              const displayE = 'datapathId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getTables', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4Physicallinks - errors', () => {
      it('should have a getV4Physicallinks function', (done) => {
        try {
          assert.equal(true, typeof a.getV4Physicallinks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#delete - errors', () => {
      it('should have a delete function', (done) => {
        try {
          assert.equal(true, typeof a.delete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkId', (done) => {
        try {
          a.delete(null, (data, error) => {
            try {
              const displayE = 'linkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-delete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#create - errors', () => {
      it('should have a create function', (done) => {
        try {
          assert.equal(true, typeof a.create === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing destId', (done) => {
        try {
          a.create(null, null, (data, error) => {
            try {
              const displayE = 'destId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-create', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing srcId', (done) => {
        try {
          a.create('fakeparam', null, (data, error) => {
            try {
              const displayE = 'srcId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-create', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4PhysicallinksUuid - errors', () => {
      it('should have a getV4PhysicallinksUuid function', (done) => {
        try {
          assert.equal(true, typeof a.getV4PhysicallinksUuid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.getV4PhysicallinksUuid(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getV4PhysicallinksUuid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllIpOpticalCorrelationPolicy - errors', () => {
      it('should have a getAllIpOpticalCorrelationPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.getAllIpOpticalCorrelationPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIpOpticalCorrelationPolicy - errors', () => {
      it('should have a createIpOpticalCorrelationPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.createIpOpticalCorrelationPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpOpticalCorrelationPolicy - errors', () => {
      it('should have a getIpOpticalCorrelationPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.getIpOpticalCorrelationPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.getIpOpticalCorrelationPolicy(null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getIpOpticalCorrelationPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIpOpticalCorrelationPolicy - errors', () => {
      it('should have a updateIpOpticalCorrelationPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.updateIpOpticalCorrelationPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.updateIpOpticalCorrelationPolicy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateIpOpticalCorrelationPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpOpticalCorrelationPolicy - errors', () => {
      it('should have a deleteIpOpticalCorrelationPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIpOpticalCorrelationPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.deleteIpOpticalCorrelationPolicy(null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-deleteIpOpticalCorrelationPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllRdRtRanges - errors', () => {
      it('should have a getAllRdRtRanges function', (done) => {
        try {
          assert.equal(true, typeof a.getAllRdRtRanges === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createRdRtRanges - errors', () => {
      it('should have a createRdRtRanges function', (done) => {
        try {
          assert.equal(true, typeof a.createRdRtRanges === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateRdRtRanges - errors', () => {
      it('should have a updateRdRtRanges function', (done) => {
        try {
          assert.equal(true, typeof a.updateRdRtRanges === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.updateRdRtRanges('fakeparam', null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateRdRtRanges', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRdRtRanges - errors', () => {
      it('should have a deleteRdRtRanges function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRdRtRanges === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.deleteRdRtRanges(null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-deleteRdRtRanges', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllRouterPortProtectionGroupPolicy - errors', () => {
      it('should have a getAllRouterPortProtectionGroupPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.getAllRouterPortProtectionGroupPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createRouterPortProtectionGroupPolicy - errors', () => {
      it('should have a createRouterPortProtectionGroupPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.createRouterPortProtectionGroupPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRouterPortProtectionGroupPolicy - errors', () => {
      it('should have a getRouterPortProtectionGroupPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.getRouterPortProtectionGroupPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.getRouterPortProtectionGroupPolicy(null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getRouterPortProtectionGroupPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateRouterPortProtectionGroupPolicy - errors', () => {
      it('should have a updateRouterPortProtectionGroupPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.updateRouterPortProtectionGroupPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.updateRouterPortProtectionGroupPolicy(null, null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateRouterPortProtectionGroupPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRouterPortProtectionGroupPolicy - errors', () => {
      it('should have a deleteRouterPortProtectionGroupPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRouterPortProtectionGroupPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.deleteRouterPortProtectionGroupPolicy(null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-deleteRouterPortProtectionGroupPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSteeringParameter - errors', () => {
      it('should have a createSteeringParameter function', (done) => {
        try {
          assert.equal(true, typeof a.createSteeringParameter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSteeringParameter - errors', () => {
      it('should have a deleteSteeringParameter function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSteeringParameter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing steeringParameterName', (done) => {
        try {
          a.deleteSteeringParameter(null, (data, error) => {
            try {
              const displayE = 'steeringParameterName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-deleteSteeringParameter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSteeringParameters - errors', () => {
      it('should have a getSteeringParameters function', (done) => {
        try {
          assert.equal(true, typeof a.getSteeringParameters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllTunnelSelections - errors', () => {
      it('should have a getAllTunnelSelections function', (done) => {
        try {
          assert.equal(true, typeof a.getAllTunnelSelections === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createTunnelSelections - errors', () => {
      it('should have a createTunnelSelections function', (done) => {
        try {
          assert.equal(true, typeof a.createTunnelSelections === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTunnelSelections - errors', () => {
      it('should have a getTunnelSelections function', (done) => {
        try {
          assert.equal(true, typeof a.getTunnelSelections === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.getTunnelSelections(null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getTunnelSelections', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTunnelSelections - errors', () => {
      it('should have a updateTunnelSelections function', (done) => {
        try {
          assert.equal(true, typeof a.updateTunnelSelections === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.updateTunnelSelections(null, null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateTunnelSelections', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTunnelSelections - errors', () => {
      it('should have a deleteTunnelSelections function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTunnelSelections === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.deleteTunnelSelections(null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-deleteTunnelSelections', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4Ports - errors', () => {
      it('should have a getV4Ports function', (done) => {
        try {
          assert.equal(true, typeof a.getV4Ports === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNeAllByTenant - errors', () => {
      it('should have a getNeAllByTenant function', (done) => {
        try {
          assert.equal(true, typeof a.getNeAllByTenant === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.getNeAllByTenant(null, null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getNeAllByTenant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantUuid', (done) => {
        try {
          a.getNeAllByTenant('fakeparam', null, (data, error) => {
            try {
              const displayE = 'tenantUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getNeAllByTenant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNeAll - errors', () => {
      it('should have a getNeAll function', (done) => {
        try {
          assert.equal(true, typeof a.getNeAll === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.getNeAll(null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getNeAll', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNeByTenant - errors', () => {
      it('should have a getNeByTenant function', (done) => {
        try {
          assert.equal(true, typeof a.getNeByTenant === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.getNeByTenant(null, null, null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getNeByTenant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceType', (done) => {
        try {
          a.getNeByTenant('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'serviceType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getNeByTenant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantUuid', (done) => {
        try {
          a.getNeByTenant('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'tenantUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getNeByTenant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNe - errors', () => {
      it('should have a getNe function', (done) => {
        try {
          assert.equal(true, typeof a.getNe === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neUuid', (done) => {
        try {
          a.getNe(null, null, (data, error) => {
            try {
              const displayE = 'neUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getNe', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceType', (done) => {
        try {
          a.getNe('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getNe', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServiceCountOnPort - errors', () => {
      it('should have a getServiceCountOnPort function', (done) => {
        try {
          assert.equal(true, typeof a.getServiceCountOnPort === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing portUuid', (done) => {
        try {
          a.getServiceCountOnPort(null, (data, error) => {
            try {
              const displayE = 'portUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getServiceCountOnPort', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServicesOnPort - errors', () => {
      it('should have a getServicesOnPort function', (done) => {
        try {
          assert.equal(true, typeof a.getServicesOnPort === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing portUuid', (done) => {
        try {
          a.getServicesOnPort(null, (data, error) => {
            try {
              const displayE = 'portUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getServicesOnPort', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServicetype - errors', () => {
      it('should have a getServicetype function', (done) => {
        try {
          assert.equal(true, typeof a.getServicetype === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceType', (done) => {
        try {
          a.getServicetype(null, (data, error) => {
            try {
              const displayE = 'serviceType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getServicetype', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServicetypeByTenant - errors', () => {
      it('should have a getServicetypeByTenant function', (done) => {
        try {
          assert.equal(true, typeof a.getServicetypeByTenant === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceType', (done) => {
        try {
          a.getServicetypeByTenant(null, null, (data, error) => {
            try {
              const displayE = 'serviceType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getServicetypeByTenant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantUuid', (done) => {
        try {
          a.getServicetypeByTenant('fakeparam', null, (data, error) => {
            try {
              const displayE = 'tenantUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getServicetypeByTenant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTenant - errors', () => {
      it('should have a getTenant function', (done) => {
        try {
          assert.equal(true, typeof a.getTenant === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantUuid', (done) => {
        try {
          a.getTenant(null, (data, error) => {
            try {
              const displayE = 'tenantUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getTenant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4PortsId - errors', () => {
      it('should have a putV4PortsId function', (done) => {
        try {
          assert.equal(true, typeof a.putV4PortsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putV4PortsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-putV4PortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4PortsPortUuid - errors', () => {
      it('should have a getV4PortsPortUuid function', (done) => {
        try {
          assert.equal(true, typeof a.getV4PortsPortUuid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing portUuid', (done) => {
        try {
          a.getV4PortsPortUuid(null, (data, error) => {
            try {
              const displayE = 'portUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getV4PortsPortUuid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthentication - errors', () => {
      it('should have a getAuthentication function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthentication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createConstraintTest - errors', () => {
      it('should have a createConstraintTest function', (done) => {
        try {
          assert.equal(true, typeof a.createConstraintTest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.createConstraintTest(null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-createConstraintTest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDciRecompute - errors', () => {
      it('should have a createDciRecompute function', (done) => {
        try {
          assert.equal(true, typeof a.createDciRecompute === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.createDciRecompute(null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-createDciRecompute', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#isMaster - errors', () => {
      it('should have a isMaster function', (done) => {
        try {
          assert.equal(true, typeof a.isMaster === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setPluginConnect - errors', () => {
      it('should have a setPluginConnect function', (done) => {
        try {
          assert.equal(true, typeof a.setPluginConnect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pluginKey', (done) => {
        try {
          a.setPluginConnect(null, null, null, (data, error) => {
            try {
              const displayE = 'pluginKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-setPluginConnect', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pluginName', (done) => {
        try {
          a.setPluginConnect('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'pluginName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-setPluginConnect', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pluginVId', (done) => {
        try {
          a.setPluginConnect('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'pluginVId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-setPluginConnect', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#resyncNms - errors', () => {
      it('should have a resyncNms function', (done) => {
        try {
          assert.equal(true, typeof a.resyncNms === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#resyncObject - errors', () => {
      it('should have a resyncObject function', (done) => {
        try {
          assert.equal(true, typeof a.resyncObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uuid', (done) => {
        try {
          a.resyncObject(null, (data, error) => {
            try {
              const displayE = 'uuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-resyncObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getState - errors', () => {
      it('should have a getState function', (done) => {
        try {
          assert.equal(true, typeof a.getState === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVersion - errors', () => {
      it('should have a getVersion function', (done) => {
        try {
          assert.equal(true, typeof a.getVersion === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllClineServices - errors', () => {
      it('should have a getAllClineServices function', (done) => {
        try {
          assert.equal(true, typeof a.getAllClineServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createClineServices - errors', () => {
      it('should have a createClineServices function', (done) => {
        try {
          assert.equal(true, typeof a.createClineServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getClineServices - errors', () => {
      it('should have a getClineServices function', (done) => {
        try {
          assert.equal(true, typeof a.getClineServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.getClineServices(null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getClineServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateClineServices - errors', () => {
      it('should have a updateClineServices function', (done) => {
        try {
          assert.equal(true, typeof a.updateClineServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.updateClineServices('fakeparam', null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateClineServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteClineServices - errors', () => {
      it('should have a deleteClineServices function', (done) => {
        try {
          assert.equal(true, typeof a.deleteClineServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.deleteClineServices(null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-deleteClineServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllCustomAttributes - errors', () => {
      it('should have a getAllCustomAttributes function', (done) => {
        try {
          assert.equal(true, typeof a.getAllCustomAttributes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createCustomAttributes - errors', () => {
      it('should have a createCustomAttributes function', (done) => {
        try {
          assert.equal(true, typeof a.createCustomAttributes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCustomAttributes - errors', () => {
      it('should have a getCustomAttributes function', (done) => {
        try {
          assert.equal(true, typeof a.getCustomAttributes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.getCustomAttributes(null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getCustomAttributes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateCustomAttributes - errors', () => {
      it('should have a updateCustomAttributes function', (done) => {
        try {
          assert.equal(true, typeof a.updateCustomAttributes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.updateCustomAttributes('fakeparam', null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateCustomAttributes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCustomAttributes - errors', () => {
      it('should have a deleteCustomAttributes function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCustomAttributes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.deleteCustomAttributes(null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-deleteCustomAttributes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllElanServices - errors', () => {
      it('should have a getAllElanServices function', (done) => {
        try {
          assert.equal(true, typeof a.getAllElanServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createElanServices - errors', () => {
      it('should have a createElanServices function', (done) => {
        try {
          assert.equal(true, typeof a.createElanServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getElanServices - errors', () => {
      it('should have a getElanServices function', (done) => {
        try {
          assert.equal(true, typeof a.getElanServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.getElanServices(null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getElanServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateElanServices - errors', () => {
      it('should have a updateElanServices function', (done) => {
        try {
          assert.equal(true, typeof a.updateElanServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.updateElanServices('fakeparam', null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateElanServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteElanServices - errors', () => {
      it('should have a deleteElanServices function', (done) => {
        try {
          assert.equal(true, typeof a.deleteElanServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.deleteElanServices(null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-deleteElanServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllElineServices - errors', () => {
      it('should have a getAllElineServices function', (done) => {
        try {
          assert.equal(true, typeof a.getAllElineServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createElineServices - errors', () => {
      it('should have a createElineServices function', (done) => {
        try {
          assert.equal(true, typeof a.createElineServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getElineServices - errors', () => {
      it('should have a getElineServices function', (done) => {
        try {
          assert.equal(true, typeof a.getElineServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.getElineServices(null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getElineServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateElineServices - errors', () => {
      it('should have a updateElineServices function', (done) => {
        try {
          assert.equal(true, typeof a.updateElineServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.updateElineServices('fakeparam', null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateElineServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteElineServices - errors', () => {
      it('should have a deleteElineServices function', (done) => {
        try {
          assert.equal(true, typeof a.deleteElineServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.deleteElineServices(null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-deleteElineServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllGenericQos - errors', () => {
      it('should have a getAllGenericQos function', (done) => {
        try {
          assert.equal(true, typeof a.getAllGenericQos === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createGenericQos - errors', () => {
      it('should have a createGenericQos function', (done) => {
        try {
          assert.equal(true, typeof a.createGenericQos === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGenericQosProfiles - errors', () => {
      it('should have a getGenericQosProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.getGenericQosProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.getGenericQosProfiles(null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getGenericQosProfiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGenericQos - errors', () => {
      it('should have a getGenericQos function', (done) => {
        try {
          assert.equal(true, typeof a.getGenericQos === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing gqpId', (done) => {
        try {
          a.getGenericQos(null, (data, error) => {
            try {
              const displayE = 'gqpId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getGenericQos', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateGenericQos - errors', () => {
      it('should have a updateGenericQos function', (done) => {
        try {
          assert.equal(true, typeof a.updateGenericQos === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing gqpId', (done) => {
        try {
          a.updateGenericQos(null, null, (data, error) => {
            try {
              const displayE = 'gqpId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateGenericQos', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteGenericQos - errors', () => {
      it('should have a deleteGenericQos function', (done) => {
        try {
          assert.equal(true, typeof a.deleteGenericQos === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing gqpId', (done) => {
        try {
          a.deleteGenericQos(null, (data, error) => {
            try {
              const displayE = 'gqpId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-deleteGenericQos', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllIesServices - errors', () => {
      it('should have a getAllIesServices function', (done) => {
        try {
          assert.equal(true, typeof a.getAllIesServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIesServices - errors', () => {
      it('should have a createIesServices function', (done) => {
        try {
          assert.equal(true, typeof a.createIesServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIesServices - errors', () => {
      it('should have a getIesServices function', (done) => {
        try {
          assert.equal(true, typeof a.getIesServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.getIesServices(null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getIesServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIesServices - errors', () => {
      it('should have a updateIesServices function', (done) => {
        try {
          assert.equal(true, typeof a.updateIesServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.updateIesServices('fakeparam', null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateIesServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIesServices - errors', () => {
      it('should have a deleteIesServices function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIesServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.deleteIesServices(null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-deleteIesServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllL2DciVpnServices - errors', () => {
      it('should have a getAllL2DciVpnServices function', (done) => {
        try {
          assert.equal(true, typeof a.getAllL2DciVpnServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getL2DciVpnServices - errors', () => {
      it('should have a getL2DciVpnServices function', (done) => {
        try {
          assert.equal(true, typeof a.getL2DciVpnServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.getL2DciVpnServices(null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getL2DciVpnServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateL2DciVpnServices - errors', () => {
      it('should have a updateL2DciVpnServices function', (done) => {
        try {
          assert.equal(true, typeof a.updateL2DciVpnServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.updateL2DciVpnServices('fakeparam', null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateL2DciVpnServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllL2backhaulServices - errors', () => {
      it('should have a getAllL2backhaulServices function', (done) => {
        try {
          assert.equal(true, typeof a.getAllL2backhaulServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createL2backhaulServices - errors', () => {
      it('should have a createL2backhaulServices function', (done) => {
        try {
          assert.equal(true, typeof a.createL2backhaulServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getL2backhaulServices - errors', () => {
      it('should have a getL2backhaulServices function', (done) => {
        try {
          assert.equal(true, typeof a.getL2backhaulServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.getL2backhaulServices(null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getL2backhaulServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateL2backhaulServices - errors', () => {
      it('should have a updateL2backhaulServices function', (done) => {
        try {
          assert.equal(true, typeof a.updateL2backhaulServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.updateL2backhaulServices('fakeparam', null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateL2backhaulServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteL2backhaulServices - errors', () => {
      it('should have a deleteL2backhaulServices function', (done) => {
        try {
          assert.equal(true, typeof a.deleteL2backhaulServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.deleteL2backhaulServices(null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-deleteL2backhaulServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllL3DciVpnServices - errors', () => {
      it('should have a getAllL3DciVpnServices function', (done) => {
        try {
          assert.equal(true, typeof a.getAllL3DciVpnServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createL3DciVpnServices - errors', () => {
      it('should have a createL3DciVpnServices function', (done) => {
        try {
          assert.equal(true, typeof a.createL3DciVpnServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getL3DciVpnServices - errors', () => {
      it('should have a getL3DciVpnServices function', (done) => {
        try {
          assert.equal(true, typeof a.getL3DciVpnServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.getL3DciVpnServices(null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getL3DciVpnServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateL3DciVpnServices - errors', () => {
      it('should have a updateL3DciVpnServices function', (done) => {
        try {
          assert.equal(true, typeof a.updateL3DciVpnServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.updateL3DciVpnServices('fakeparam', null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateL3DciVpnServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteL3DciVpnServices - errors', () => {
      it('should have a deleteL3DciVpnServices function', (done) => {
        try {
          assert.equal(true, typeof a.deleteL3DciVpnServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.deleteL3DciVpnServices(null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-deleteL3DciVpnServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllL3VpnServices - errors', () => {
      it('should have a getAllL3VpnServices function', (done) => {
        try {
          assert.equal(true, typeof a.getAllL3VpnServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createL3VpnServices - errors', () => {
      it('should have a createL3VpnServices function', (done) => {
        try {
          assert.equal(true, typeof a.createL3VpnServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getL3VpnServices - errors', () => {
      it('should have a getL3VpnServices function', (done) => {
        try {
          assert.equal(true, typeof a.getL3VpnServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.getL3VpnServices(null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getL3VpnServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateL3VpnServices - errors', () => {
      it('should have a updateL3VpnServices function', (done) => {
        try {
          assert.equal(true, typeof a.updateL3VpnServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.updateL3VpnServices('fakeparam', null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateL3VpnServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteL3VpnServices - errors', () => {
      it('should have a deleteL3VpnServices function', (done) => {
        try {
          assert.equal(true, typeof a.deleteL3VpnServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.deleteL3VpnServices(null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-deleteL3VpnServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllLagServices - errors', () => {
      it('should have a getAllLagServices function', (done) => {
        try {
          assert.equal(true, typeof a.getAllLagServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createLagServices - errors', () => {
      it('should have a createLagServices function', (done) => {
        try {
          assert.equal(true, typeof a.createLagServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLagServices - errors', () => {
      it('should have a getLagServices function', (done) => {
        try {
          assert.equal(true, typeof a.getLagServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.getLagServices(null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getLagServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateLagServices - errors', () => {
      it('should have a updateLagServices function', (done) => {
        try {
          assert.equal(true, typeof a.updateLagServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.updateLagServices('fakeparam', null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateLagServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLagServices - errors', () => {
      it('should have a deleteLagServices function', (done) => {
        try {
          assert.equal(true, typeof a.deleteLagServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.deleteLagServices(null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-deleteLagServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createMediationProfileMapping - errors', () => {
      it('should have a createMediationProfileMapping function', (done) => {
        try {
          assert.equal(true, typeof a.createMediationProfileMapping === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMediationProfileMapping - errors', () => {
      it('should have a getMediationProfileMapping function', (done) => {
        try {
          assert.equal(true, typeof a.getMediationProfileMapping === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing profileId', (done) => {
        try {
          a.getMediationProfileMapping(null, (data, error) => {
            try {
              const displayE = 'profileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getMediationProfileMapping', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateMediationProfileMapping - errors', () => {
      it('should have a updateMediationProfileMapping function', (done) => {
        try {
          assert.equal(true, typeof a.updateMediationProfileMapping === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing profileId', (done) => {
        try {
          a.updateMediationProfileMapping('fakeparam', null, (data, error) => {
            try {
              const displayE = 'profileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateMediationProfileMapping', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteMediationProfileMapping - errors', () => {
      it('should have a deleteMediationProfileMapping function', (done) => {
        try {
          assert.equal(true, typeof a.deleteMediationProfileMapping === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing profileId', (done) => {
        try {
          a.deleteMediationProfileMapping(null, (data, error) => {
            try {
              const displayE = 'profileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-deleteMediationProfileMapping', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllMediationProfileMappings - errors', () => {
      it('should have a getAllMediationProfileMappings function', (done) => {
        try {
          assert.equal(true, typeof a.getAllMediationProfileMappings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNfmpTemplate - errors', () => {
      it('should have a getNfmpTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.getNfmpTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllOchServices - errors', () => {
      it('should have a getAllOchServices function', (done) => {
        try {
          assert.equal(true, typeof a.getAllOchServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createOchServices - errors', () => {
      it('should have a createOchServices function', (done) => {
        try {
          assert.equal(true, typeof a.createOchServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOchServices - errors', () => {
      it('should have a getOchServices function', (done) => {
        try {
          assert.equal(true, typeof a.getOchServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.getOchServices(null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getOchServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateOchServices - errors', () => {
      it('should have a updateOchServices function', (done) => {
        try {
          assert.equal(true, typeof a.updateOchServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.updateOchServices('fakeparam', null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateOchServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOchServices - errors', () => {
      it('should have a deleteOchServices function', (done) => {
        try {
          assert.equal(true, typeof a.deleteOchServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.deleteOchServices(null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-deleteOchServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllOduServices - errors', () => {
      it('should have a getAllOduServices function', (done) => {
        try {
          assert.equal(true, typeof a.getAllOduServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createOduServices - errors', () => {
      it('should have a createOduServices function', (done) => {
        try {
          assert.equal(true, typeof a.createOduServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOduServices - errors', () => {
      it('should have a getOduServices function', (done) => {
        try {
          assert.equal(true, typeof a.getOduServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.getOduServices(null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getOduServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateOduServices - errors', () => {
      it('should have a updateOduServices function', (done) => {
        try {
          assert.equal(true, typeof a.updateOduServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.updateOduServices('fakeparam', null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateOduServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOduServices - errors', () => {
      it('should have a deleteOduServices function', (done) => {
        try {
          assert.equal(true, typeof a.deleteOduServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.deleteOduServices(null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-deleteOduServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllOpticalConnectivityConstraint - errors', () => {
      it('should have a getAllOpticalConnectivityConstraint function', (done) => {
        try {
          assert.equal(true, typeof a.getAllOpticalConnectivityConstraint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createOpticalConnectivityConstraint - errors', () => {
      it('should have a createOpticalConnectivityConstraint function', (done) => {
        try {
          assert.equal(true, typeof a.createOpticalConnectivityConstraint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOpticalConnectivityConstraint - errors', () => {
      it('should have a getOpticalConnectivityConstraint function', (done) => {
        try {
          assert.equal(true, typeof a.getOpticalConnectivityConstraint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.getOpticalConnectivityConstraint(null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getOpticalConnectivityConstraint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateOpticalConnectivityConstraint - errors', () => {
      it('should have a updateOpticalConnectivityConstraint function', (done) => {
        try {
          assert.equal(true, typeof a.updateOpticalConnectivityConstraint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.updateOpticalConnectivityConstraint('fakeparam', null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateOpticalConnectivityConstraint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOpticalConnectivityConstraint - errors', () => {
      it('should have a deleteOpticalConnectivityConstraint function', (done) => {
        try {
          assert.equal(true, typeof a.deleteOpticalConnectivityConstraint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.deleteOpticalConnectivityConstraint(null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-deleteOpticalConnectivityConstraint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllOpticalConnectivityService - errors', () => {
      it('should have a getAllOpticalConnectivityService function', (done) => {
        try {
          assert.equal(true, typeof a.getAllOpticalConnectivityService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createOpticalConnectivityService - errors', () => {
      it('should have a createOpticalConnectivityService function', (done) => {
        try {
          assert.equal(true, typeof a.createOpticalConnectivityService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOpticalConnectivityService - errors', () => {
      it('should have a getOpticalConnectivityService function', (done) => {
        try {
          assert.equal(true, typeof a.getOpticalConnectivityService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.getOpticalConnectivityService(null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getOpticalConnectivityService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateOpticalConnectivityService - errors', () => {
      it('should have a updateOpticalConnectivityService function', (done) => {
        try {
          assert.equal(true, typeof a.updateOpticalConnectivityService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.updateOpticalConnectivityService('fakeparam', null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateOpticalConnectivityService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOpticalConnectivityService - errors', () => {
      it('should have a deleteOpticalConnectivityService function', (done) => {
        try {
          assert.equal(true, typeof a.deleteOpticalConnectivityService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.deleteOpticalConnectivityService(null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-deleteOpticalConnectivityService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllOpticalResilienceConstraint - errors', () => {
      it('should have a getAllOpticalResilienceConstraint function', (done) => {
        try {
          assert.equal(true, typeof a.getAllOpticalResilienceConstraint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createOpticalResilienceConstraint - errors', () => {
      it('should have a createOpticalResilienceConstraint function', (done) => {
        try {
          assert.equal(true, typeof a.createOpticalResilienceConstraint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOpticalResilienceConstraint - errors', () => {
      it('should have a getOpticalResilienceConstraint function', (done) => {
        try {
          assert.equal(true, typeof a.getOpticalResilienceConstraint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.getOpticalResilienceConstraint(null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getOpticalResilienceConstraint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateOpticalResilienceConstraint - errors', () => {
      it('should have a updateOpticalResilienceConstraint function', (done) => {
        try {
          assert.equal(true, typeof a.updateOpticalResilienceConstraint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.updateOpticalResilienceConstraint('fakeparam', null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateOpticalResilienceConstraint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOpticalResilienceConstraint - errors', () => {
      it('should have a deleteOpticalResilienceConstraint function', (done) => {
        try {
          assert.equal(true, typeof a.deleteOpticalResilienceConstraint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.deleteOpticalResilienceConstraint(null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-deleteOpticalResilienceConstraint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllOpticalRoutingConstraint - errors', () => {
      it('should have a getAllOpticalRoutingConstraint function', (done) => {
        try {
          assert.equal(true, typeof a.getAllOpticalRoutingConstraint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createOpticalRoutingConstraint - errors', () => {
      it('should have a createOpticalRoutingConstraint function', (done) => {
        try {
          assert.equal(true, typeof a.createOpticalRoutingConstraint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOpticalRoutingConstraint - errors', () => {
      it('should have a getOpticalRoutingConstraint function', (done) => {
        try {
          assert.equal(true, typeof a.getOpticalRoutingConstraint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.getOpticalRoutingConstraint(null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getOpticalRoutingConstraint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateOpticalRoutingConstraint - errors', () => {
      it('should have a updateOpticalRoutingConstraint function', (done) => {
        try {
          assert.equal(true, typeof a.updateOpticalRoutingConstraint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.updateOpticalRoutingConstraint('fakeparam', null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateOpticalRoutingConstraint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOpticalRoutingConstraint - errors', () => {
      it('should have a deleteOpticalRoutingConstraint function', (done) => {
        try {
          assert.equal(true, typeof a.deleteOpticalRoutingConstraint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.deleteOpticalRoutingConstraint(null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-deleteOpticalRoutingConstraint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllPathProfiles - errors', () => {
      it('should have a getAllPathProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.getAllPathProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createPathProfiles - errors', () => {
      it('should have a createPathProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.createPathProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPathProfiles - errors', () => {
      it('should have a getPathProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.getPathProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.getPathProfiles(null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getPathProfiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePathProfiles - errors', () => {
      it('should have a updatePathProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.updatePathProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.updatePathProfiles('fakeparam', null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updatePathProfiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePathProfiles - errors', () => {
      it('should have a deletePathProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.deletePathProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.deletePathProfiles(null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-deletePathProfiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllQos - errors', () => {
      it('should have a getAllQos function', (done) => {
        try {
          assert.equal(true, typeof a.getAllQos === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createQos - errors', () => {
      it('should have a createQos function', (done) => {
        try {
          assert.equal(true, typeof a.createQos === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllQosPolicies - errors', () => {
      it('should have a getAllQosPolicies function', (done) => {
        try {
          assert.equal(true, typeof a.getAllQosPolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getQosPolicies - errors', () => {
      it('should have a getQosPolicies function', (done) => {
        try {
          assert.equal(true, typeof a.getQosPolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.getQosPolicies(null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getQosPolicies', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getQos - errors', () => {
      it('should have a getQos function', (done) => {
        try {
          assert.equal(true, typeof a.getQos === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.getQos(null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getQos', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateQos - errors', () => {
      it('should have a updateQos function', (done) => {
        try {
          assert.equal(true, typeof a.updateQos === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.updateQos('fakeparam', null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateQos', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteQos - errors', () => {
      it('should have a deleteQos function', (done) => {
        try {
          assert.equal(true, typeof a.deleteQos === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.deleteQos(null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-deleteQos', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllRouterIdSystemIdMappings - errors', () => {
      it('should have a getAllRouterIdSystemIdMappings function', (done) => {
        try {
          assert.equal(true, typeof a.getAllRouterIdSystemIdMappings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createRouterIdSystemIdMapping - errors', () => {
      it('should have a createRouterIdSystemIdMapping function', (done) => {
        try {
          assert.equal(true, typeof a.createRouterIdSystemIdMapping === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRouterIdSystemIdMapping - errors', () => {
      it('should have a getRouterIdSystemIdMapping function', (done) => {
        try {
          assert.equal(true, typeof a.getRouterIdSystemIdMapping === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.getRouterIdSystemIdMapping(null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getRouterIdSystemIdMapping', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateRouterIdSystemIdMapping - errors', () => {
      it('should have a updateRouterIdSystemIdMapping function', (done) => {
        try {
          assert.equal(true, typeof a.updateRouterIdSystemIdMapping === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.updateRouterIdSystemIdMapping(null, null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateRouterIdSystemIdMapping', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRouterIdSystemIdMapping - errors', () => {
      it('should have a deleteRouterIdSystemIdMapping function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRouterIdSystemIdMapping === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyId', (done) => {
        try {
          a.deleteRouterIdSystemIdMapping(null, (data, error) => {
            try {
              const displayE = 'policyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-deleteRouterIdSystemIdMapping', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllSystemIpMplsConfig - errors', () => {
      it('should have a getAllSystemIpMplsConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getAllSystemIpMplsConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSystemIpMplsConfig - errors', () => {
      it('should have a updateSystemIpMplsConfig function', (done) => {
        try {
          assert.equal(true, typeof a.updateSystemIpMplsConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateSystemIpMplsConfig(null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateSystemIpMplsConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.updateSystemIpMplsConfig('fakeparam', null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateSystemIpMplsConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllTunnelCreations - errors', () => {
      it('should have a getAllTunnelCreations function', (done) => {
        try {
          assert.equal(true, typeof a.getAllTunnelCreations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTunnelCreations - errors', () => {
      it('should have a updateTunnelCreations function', (done) => {
        try {
          assert.equal(true, typeof a.updateTunnelCreations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.updateTunnelCreations(null, null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateTunnelCreations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllWorkflowProfileMappings - errors', () => {
      it('should have a getAllWorkflowProfileMappings function', (done) => {
        try {
          assert.equal(true, typeof a.getAllWorkflowProfileMappings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createWorkflowProfile - errors', () => {
      it('should have a createWorkflowProfile function', (done) => {
        try {
          assert.equal(true, typeof a.createWorkflowProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWorkflowProfile - errors', () => {
      it('should have a getWorkflowProfile function', (done) => {
        try {
          assert.equal(true, typeof a.getWorkflowProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing profileId', (done) => {
        try {
          a.getWorkflowProfile(null, (data, error) => {
            try {
              const displayE = 'profileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getWorkflowProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateWorkflowProfile - errors', () => {
      it('should have a updateWorkflowProfile function', (done) => {
        try {
          assert.equal(true, typeof a.updateWorkflowProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing profileId', (done) => {
        try {
          a.updateWorkflowProfile(null, null, (data, error) => {
            try {
              const displayE = 'profileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateWorkflowProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWorkflowProfile - errors', () => {
      it('should have a deleteWorkflowProfile function', (done) => {
        try {
          assert.equal(true, typeof a.deleteWorkflowProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing profileId', (done) => {
        try {
          a.deleteWorkflowProfile(null, (data, error) => {
            try {
              const displayE = 'profileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-deleteWorkflowProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4Tenants - errors', () => {
      it('should have a getV4Tenants function', (done) => {
        try {
          assert.equal(true, typeof a.getV4Tenants === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postV4Tenants - errors', () => {
      it('should have a postV4Tenants function', (done) => {
        try {
          assert.equal(true, typeof a.postV4Tenants === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCustomerAll - errors', () => {
      it('should have a getCustomerAll function', (done) => {
        try {
          assert.equal(true, typeof a.getCustomerAll === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCustomer - errors', () => {
      it('should have a getCustomer function', (done) => {
        try {
          assert.equal(true, typeof a.getCustomer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerId', (done) => {
        try {
          a.getCustomer(null, (data, error) => {
            try {
              const displayE = 'customerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getCustomer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#resync - errors', () => {
      it('should have a resync function', (done) => {
        try {
          assert.equal(true, typeof a.resync === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing provider', (done) => {
        try {
          a.resync(null, (data, error) => {
            try {
              const displayE = 'provider is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-resync', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4TenantsTenantUuid - errors', () => {
      it('should have a getV4TenantsTenantUuid function', (done) => {
        try {
          assert.equal(true, typeof a.getV4TenantsTenantUuid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantUuid', (done) => {
        try {
          a.getV4TenantsTenantUuid(null, (data, error) => {
            try {
              const displayE = 'tenantUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getV4TenantsTenantUuid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putV4TenantsTenantUuid - errors', () => {
      it('should have a putV4TenantsTenantUuid function', (done) => {
        try {
          assert.equal(true, typeof a.putV4TenantsTenantUuid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantUuid', (done) => {
        try {
          a.putV4TenantsTenantUuid('fakeparam', null, (data, error) => {
            try {
              const displayE = 'tenantUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-putV4TenantsTenantUuid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteV4TenantsTenantUuid - errors', () => {
      it('should have a deleteV4TenantsTenantUuid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteV4TenantsTenantUuid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantUuid', (done) => {
        try {
          a.deleteV4TenantsTenantUuid(null, (data, error) => {
            try {
              const displayE = 'tenantUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-deleteV4TenantsTenantUuid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getResources - errors', () => {
      it('should have a getResources function', (done) => {
        try {
          assert.equal(true, typeof a.getResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantUuid', (done) => {
        try {
          a.getResources(null, (data, error) => {
            try {
              const displayE = 'tenantUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getResources', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addResources - errors', () => {
      it('should have a addResources function', (done) => {
        try {
          assert.equal(true, typeof a.addResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantUuid', (done) => {
        try {
          a.addResources('fakeparam', null, (data, error) => {
            try {
              const displayE = 'tenantUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-addResources', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteResources - errors', () => {
      it('should have a deleteResources function', (done) => {
        try {
          assert.equal(true, typeof a.deleteResources === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantUuid', (done) => {
        try {
          a.deleteResources('fakeparam', null, (data, error) => {
            try {
              const displayE = 'tenantUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-deleteResources', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsergroup - errors', () => {
      it('should have a getUsergroup function', (done) => {
        try {
          assert.equal(true, typeof a.getUsergroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupName', (done) => {
        try {
          a.getUsergroup(null, null, (data, error) => {
            try {
              const displayE = 'groupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getUsergroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantUuid', (done) => {
        try {
          a.getUsergroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'tenantUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getUsergroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUsergroup - errors', () => {
      it('should have a deleteUsergroup function', (done) => {
        try {
          assert.equal(true, typeof a.deleteUsergroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupName', (done) => {
        try {
          a.deleteUsergroup(null, null, (data, error) => {
            try {
              const displayE = 'groupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-deleteUsergroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantUuid', (done) => {
        try {
          a.deleteUsergroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'tenantUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-deleteUsergroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createUsergroup - errors', () => {
      it('should have a createUsergroup function', (done) => {
        try {
          assert.equal(true, typeof a.createUsergroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupName', (done) => {
        try {
          a.createUsergroup(null, null, null, (data, error) => {
            try {
              const displayE = 'groupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-createUsergroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleType', (done) => {
        try {
          a.createUsergroup('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'roleType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-createUsergroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tenantUuid', (done) => {
        try {
          a.createUsergroup('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'tenantUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-createUsergroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4Usergroups - errors', () => {
      it('should have a getV4Usergroups function', (done) => {
        try {
          assert.equal(true, typeof a.getV4Usergroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4UsergroupsGroupName - errors', () => {
      it('should have a getV4UsergroupsGroupName function', (done) => {
        try {
          assert.equal(true, typeof a.getV4UsergroupsGroupName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupName', (done) => {
        try {
          a.getV4UsergroupsGroupName(null, (data, error) => {
            try {
              const displayE = 'groupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getV4UsergroupsGroupName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getV4UsergroupsGroupNameTenants - errors', () => {
      it('should have a getV4UsergroupsGroupNameTenants function', (done) => {
        try {
          assert.equal(true, typeof a.getV4UsergroupsGroupNameTenants === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupName', (done) => {
        try {
          a.getV4UsergroupsGroupNameTenants(null, (data, error) => {
            try {
              const displayE = 'groupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getV4UsergroupsGroupNameTenants', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNrcpHistorical - errors', () => {
      it('should have a getNrcpHistorical function', (done) => {
        try {
          assert.equal(true, typeof a.getNrcpHistorical === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchNrcpHistorical - errors', () => {
      it('should have a patchNrcpHistorical function', (done) => {
        try {
          assert.equal(true, typeof a.patchNrcpHistorical === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSrPolicyConfig - errors', () => {
      it('should have a getSrPolicyConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getSrPolicyConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchSrPolicyConfig - errors', () => {
      it('should have a patchSrPolicyConfig function', (done) => {
        try {
          assert.equal(true, typeof a.patchSrPolicyConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addElineEndpoint - errors', () => {
      it('should have a addElineEndpoint function', (done) => {
        try {
          assert.equal(true, typeof a.addElineEndpoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.addElineEndpoint(null, null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-addElineEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addElineEndpoint('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-addElineEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addL3DciL3Endpoint - errors', () => {
      it('should have a addL3DciL3Endpoint function', (done) => {
        try {
          assert.equal(true, typeof a.addL3DciL3Endpoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.addL3DciL3Endpoint(null, null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-addL3DciL3Endpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addL3DciL3Endpoint('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-addL3DciL3Endpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addL3DciL3LoopbackEndpoint - errors', () => {
      it('should have a addL3DciL3LoopbackEndpoint function', (done) => {
        try {
          assert.equal(true, typeof a.addL3DciL3LoopbackEndpoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.addL3DciL3LoopbackEndpoint(null, null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-addL3DciL3LoopbackEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchL3DciL3LoopbackEndpoint - errors', () => {
      it('should have a patchL3DciL3LoopbackEndpoint function', (done) => {
        try {
          assert.equal(true, typeof a.patchL3DciL3LoopbackEndpoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endpointUuid', (done) => {
        try {
          a.patchL3DciL3LoopbackEndpoint(null, null, null, (data, error) => {
            try {
              const displayE = 'endpointUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-patchL3DciL3LoopbackEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.patchL3DciL3LoopbackEndpoint('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-patchL3DciL3LoopbackEndpoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateStitchL2Ext - errors', () => {
      it('should have a updateStitchL2Ext function', (done) => {
        try {
          assert.equal(true, typeof a.updateStitchL2Ext === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing l2extServiceUuid', (done) => {
        try {
          a.updateStitchL2Ext(null, null, null, (data, error) => {
            try {
              const displayE = 'l2extServiceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateStitchL2Ext', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceEndpointUuid', (done) => {
        try {
          a.updateStitchL2Ext('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'serviceEndpointUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateStitchL2Ext', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceUuid', (done) => {
        try {
          a.updateStitchL2Ext('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceUuid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateStitchL2Ext', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTopologyReferencesByDomain - errors', () => {
      it('should have a deleteTopologyReferencesByDomain function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTopologyReferencesByDomain === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.deleteTopologyReferencesByDomain(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-deleteTopologyReferencesByDomain', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTopologyReferencesByLink - errors', () => {
      it('should have a deleteTopologyReferencesByLink function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTopologyReferencesByLink === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkId', (done) => {
        try {
          a.deleteTopologyReferencesByLink(null, (data, error) => {
            try {
              const displayE = 'linkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-deleteTopologyReferencesByLink', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#diagnoseLsp - errors', () => {
      it('should have a diagnoseLsp function', (done) => {
        try {
          assert.equal(true, typeof a.diagnoseLsp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing lspPathId', (done) => {
        try {
          a.diagnoseLsp(null, (data, error) => {
            try {
              const displayE = 'lspPathId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-diagnoseLsp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#diagnosePath - errors', () => {
      it('should have a diagnosePath function', (done) => {
        try {
          assert.equal(true, typeof a.diagnosePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#findSdnPath - errors', () => {
      it('should have a findSdnPath function', (done) => {
        try {
          assert.equal(true, typeof a.findSdnPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addMultipointPort - errors', () => {
      it('should have a addMultipointPort function', (done) => {
        try {
          assert.equal(true, typeof a.addMultipointPort === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing multipointLinkId', (done) => {
        try {
          a.addMultipointPort(null, null, (data, error) => {
            try {
              const displayE = 'multipointLinkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-addMultipointPort', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing portId', (done) => {
        try {
          a.addMultipointPort('fakeparam', null, (data, error) => {
            try {
              const displayE = 'portId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-addMultipointPort', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeMultipointPort - errors', () => {
      it('should have a removeMultipointPort function', (done) => {
        try {
          assert.equal(true, typeof a.removeMultipointPort === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing multipointLinkId', (done) => {
        try {
          a.removeMultipointPort(null, null, (data, error) => {
            try {
              const displayE = 'multipointLinkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-removeMultipointPort', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing portId', (done) => {
        try {
          a.removeMultipointPort('fakeparam', null, (data, error) => {
            try {
              const displayE = 'portId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-removeMultipointPort', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteMultipoint - errors', () => {
      it('should have a deleteMultipoint function', (done) => {
        try {
          assert.equal(true, typeof a.deleteMultipoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkId', (done) => {
        try {
          a.deleteMultipoint(null, (data, error) => {
            try {
              const displayE = 'linkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-deleteMultipoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createMultipoint - errors', () => {
      it('should have a createMultipoint function', (done) => {
        try {
          assert.equal(true, typeof a.createMultipoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkId', (done) => {
        try {
          a.createMultipoint(null, null, (data, error) => {
            try {
              const displayE = 'linkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-createMultipoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMultipoint - errors', () => {
      it('should have a getMultipoint function', (done) => {
        try {
          assert.equal(true, typeof a.getMultipoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkId', (done) => {
        try {
          a.getMultipoint(null, (data, error) => {
            try {
              const displayE = 'linkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getMultipoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMultipoints - errors', () => {
      it('should have a getMultipoints function', (done) => {
        try {
          assert.equal(true, typeof a.getMultipoints === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllConnectionProfile - errors', () => {
      it('should have a getAllConnectionProfile function', (done) => {
        try {
          assert.equal(true, typeof a.getAllConnectionProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.getAllConnectionProfile(null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getAllConnectionProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createTriggerTtzAlgorithm - errors', () => {
      it('should have a createTriggerTtzAlgorithm function', (done) => {
        try {
          assert.equal(true, typeof a.createTriggerTtzAlgorithm === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSRPolicies - errors', () => {
      it('should have a getSRPolicies function', (done) => {
        try {
          assert.equal(true, typeof a.getSRPolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSRPolicy - errors', () => {
      it('should have a createSRPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.createSRPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createSRPolicy(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-createSRPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createCandidatePathResignal - errors', () => {
      it('should have a createCandidatePathResignal function', (done) => {
        try {
          assert.equal(true, typeof a.createCandidatePathResignal === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.createCandidatePathResignal(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-createCandidatePathResignal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchCandidatePathAdmin - errors', () => {
      it('should have a patchCandidatePathAdmin function', (done) => {
        try {
          assert.equal(true, typeof a.patchCandidatePathAdmin === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing adminState', (done) => {
        try {
          a.patchCandidatePathAdmin(null, null, (data, error) => {
            try {
              const displayE = 'adminState is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-patchCandidatePathAdmin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing candidatePathId', (done) => {
        try {
          a.patchCandidatePathAdmin('fakeparam', null, (data, error) => {
            try {
              const displayE = 'candidatePathId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-patchCandidatePathAdmin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateCandidatePaths - errors', () => {
      it('should have a updateCandidatePaths function', (done) => {
        try {
          assert.equal(true, typeof a.updateCandidatePaths === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateCandidatePaths(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateCandidatePaths', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateCandidatePaths('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updateCandidatePaths', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSRPoliciesOnLink - errors', () => {
      it('should have a getSRPoliciesOnLink function', (done) => {
        try {
          assert.equal(true, typeof a.getSRPoliciesOnLink === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkId', (done) => {
        try {
          a.getSRPoliciesOnLink(null, null, (data, error) => {
            try {
              const displayE = 'linkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getSRPoliciesOnLink', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestType', (done) => {
        try {
          a.getSRPoliciesOnLink('fakeparam', null, (data, error) => {
            try {
              const displayE = 'requestType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getSRPoliciesOnLink', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePolicies - errors', () => {
      it('should have a deletePolicies function', (done) => {
        try {
          assert.equal(true, typeof a.deletePolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePolicies - errors', () => {
      it('should have a updatePolicies function', (done) => {
        try {
          assert.equal(true, typeof a.updatePolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updatePolicies(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updatePolicies', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePolicies('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-updatePolicies', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#fetchPolicyList - errors', () => {
      it('should have a fetchPolicyList function', (done) => {
        try {
          assert.equal(true, typeof a.fetchPolicyList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.fetchPolicyList(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-fetchPolicyList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePolicy - errors', () => {
      it('should have a deletePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.deletePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deletePolicy(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-deletePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPolicyAdmin - errors', () => {
      it('should have a patchPolicyAdmin function', (done) => {
        try {
          assert.equal(true, typeof a.patchPolicyAdmin === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing adminState', (done) => {
        try {
          a.patchPolicyAdmin(null, null, (data, error) => {
            try {
              const displayE = 'adminState is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-patchPolicyAdmin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing srPolicyId', (done) => {
        try {
          a.patchPolicyAdmin('fakeparam', null, (data, error) => {
            try {
              const displayE = 'srPolicyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-patchPolicyAdmin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSRPoliciesResignal - errors', () => {
      it('should have a createSRPoliciesResignal function', (done) => {
        try {
          assert.equal(true, typeof a.createSRPoliciesResignal === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createSRPoliciesResignal(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-createSRPoliciesResignal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAssocGroupConfig - errors', () => {
      it('should have a getAssocGroupConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getAssocGroupConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchAssocGroupConfig - errors', () => {
      it('should have a patchAssocGroupConfig function', (done) => {
        try {
          assert.equal(true, typeof a.patchAssocGroupConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBsidGenerationConfig - errors', () => {
      it('should have a getBsidGenerationConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getBsidGenerationConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchBsidGenerationConfig - errors', () => {
      it('should have a patchBsidGenerationConfig function', (done) => {
        try {
          assert.equal(true, typeof a.patchBsidGenerationConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchBsidGenerationConfig(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-patchBsidGenerationConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceNameIpService - errors', () => {
      it('should have a getDeviceNameIpService function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceNameIpService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDeviceNameIpService - errors', () => {
      it('should have a patchDeviceNameIpService function', (done) => {
        try {
          assert.equal(true, typeof a.patchDeviceNameIpService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLspBsidGenerationConfig - errors', () => {
      it('should have a getLspBsidGenerationConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getLspBsidGenerationConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchLspBsidGenerationConfig - errors', () => {
      it('should have a patchLspBsidGenerationConfig function', (done) => {
        try {
          assert.equal(true, typeof a.patchLspBsidGenerationConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAssocGroups - errors', () => {
      it('should have a getAssocGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getAssocGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAssocGroupsByType - errors', () => {
      it('should have a getAssocGroupsByType function', (done) => {
        try {
          assert.equal(true, typeof a.getAssocGroupsByType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing associationGroupType', (done) => {
        try {
          a.getAssocGroupsByType(null, (data, error) => {
            try {
              const displayE = 'associationGroupType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getAssocGroupsByType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#lspPathProfileOverride - errors', () => {
      it('should have a lspPathProfileOverride function', (done) => {
        try {
          assert.equal(true, typeof a.lspPathProfileOverride === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.lspPathProfileOverride(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-lspPathProfileOverride', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#fetchLspPathsByTunnelId - errors', () => {
      it('should have a fetchLspPathsByTunnelId function', (done) => {
        try {
          assert.equal(true, typeof a.fetchLspPathsByTunnelId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOnAssocGroup - errors', () => {
      it('should have a getOnAssocGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getOnAssocGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing associationGroupId', (done) => {
        try {
          a.getOnAssocGroup(null, null, null, (data, error) => {
            try {
              const displayE = 'associationGroupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getOnAssocGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing associationGroupSource', (done) => {
        try {
          a.getOnAssocGroup('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'associationGroupSource is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getOnAssocGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing associationGroupType', (done) => {
        try {
          a.getOnAssocGroup('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'associationGroupType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getOnAssocGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOnBsid - errors', () => {
      it('should have a getOnBsid function', (done) => {
        try {
          assert.equal(true, typeof a.getOnBsid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bsidId', (done) => {
        try {
          a.getOnBsid(null, (data, error) => {
            try {
              const displayE = 'bsidId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getOnBsid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getProfileGroup - errors', () => {
      it('should have a getProfileGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getProfileGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing extendedId', (done) => {
        try {
          a.getProfileGroup(null, null, (data, error) => {
            try {
              const displayE = 'extendedId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getProfileGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing profileId', (done) => {
        try {
          a.getProfileGroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'profileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getProfileGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#fetchLinks - errors', () => {
      it('should have a fetchLinks function', (done) => {
        try {
          assert.equal(true, typeof a.fetchLinks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#findPath - errors', () => {
      it('should have a findPath function', (done) => {
        try {
          assert.equal(true, typeof a.findPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getByKey - errors', () => {
      it('should have a getByKey function', (done) => {
        try {
          assert.equal(true, typeof a.getByKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing color', (done) => {
        try {
          a.getByKey(null, null, null, (data, error) => {
            try {
              const displayE = 'color is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getByKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endpoint', (done) => {
        try {
          a.getByKey('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'endpoint is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getByKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing headend', (done) => {
        try {
          a.getByKey('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'headend is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getByKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#fetchCandidatePathList - errors', () => {
      it('should have a fetchCandidatePathList function', (done) => {
        try {
          assert.equal(true, typeof a.fetchCandidatePathList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.fetchCandidatePathList(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-fetchCandidatePathList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConsumer - errors', () => {
      it('should have a getConsumer function', (done) => {
        try {
          assert.equal(true, typeof a.getConsumer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getConsumer(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getConsumer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createResignalAll - errors', () => {
      it('should have a createResignalAll function', (done) => {
        try {
          assert.equal(true, typeof a.createResignalAll === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createResignalSRPolicies - errors', () => {
      it('should have a createResignalSRPolicies function', (done) => {
        try {
          assert.equal(true, typeof a.createResignalSRPolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createResignalSRPolicies(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-createResignalSRPolicies', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSRPolicyByID - errors', () => {
      it('should have a getSRPolicyByID function', (done) => {
        try {
          assert.equal(true, typeof a.getSRPolicyByID === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getSRPolicyByID(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nokia_nsp_sdn-adapter-getSRPolicyByID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
